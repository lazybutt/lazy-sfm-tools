from character_library.common.enums import ANY_KEY


class ConfigFilter(object):
    def __init__(self, config_list):
        # type: (list) -> None
        self._items = config_list

    def _values(self, valueName):
        values = []
        for config in self._items:
            attr = getattr(config, valueName)
            if str(attr) != ANY_KEY and str(attr) not in values:
                values.extend(attr) if type(attr) == list else values.append(attr)
        return dict.fromkeys(values).keys()

    def _filter(self, by, value, fullConsistency):
        newList = []
        if value == ANY_KEY:
            return self.__class__(self._items)
        for modelConfig in self._items:
            if hasattr(modelConfig, by):
                attr = getattr(modelConfig, by)
                attr = [str(item) for item in attr] if isinstance(attr, (list, tuple)) else [str(attr)]
                value = [str(item) for item in value] if isinstance(value, (list, tuple)) else [str(value)]
                if ANY_KEY in attr:
                    newList.append(modelConfig)
                else:
                    founded = False
                    for attr_item in attr:
                        if founded:
                            break
                        for value_item in value:
                            if fullConsistency:
                                if value_item == attr_item:
                                    newList.append(modelConfig)
                                    founded = True
                                    break
                            else:
                                if value_item.lower().replace(' ', '') in str(attr_item).lower().replace(' ', ''):
                                    newList.append(modelConfig)
                                    founded = True
                                    break
        return self.__class__(newList)

    @property
    def items(self):
        return self._items

    @property
    def names(self):
        return self._values('name')

    def by_name(self, value, fullConsistency=True):
        return self._filter('name', value, fullConsistency)
