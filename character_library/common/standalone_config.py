import json

import yaml
from character_library.exceptions import *
from character_library.utils.file_utils import drop_extension
from character_library.common.fields import ConfigFields as Fields


class StandaloneConfig(object):
    extension = None
    searchFolders = None

    def __init__(self, name, path=None):
        self._name = name
        self._path = drop_extension(path) if path is not None else None

    @property
    def is_virtual(self):
        return self._path is None

    @property
    def name(self):
        return self._name

    @property
    def path_without_extension(self):
        if self.is_virtual:
            raise ConfigPathAccessError(self)
        return self._path

    @property
    def config_path(self):
        return "{}.{}".format(self.path_without_extension, self.__class__.extension)

    @property
    def dump(self):
        return {Fields.NAME: self._name}

    def save(self, path):
        self._path = path
        yaml.safe_dump(self.dump, open(path, 'w+'))

    def __str__(self):
        return self.dump.__str__()

    def __repr__(self):
        return "({}) Name: {}, Path: {}".format(self.__class__.__name__, self._name, self.config_path)
