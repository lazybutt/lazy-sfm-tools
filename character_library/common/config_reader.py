import os


class ConfigReader(object):
    @classmethod
    def read(cls, path_or_dict, check_needed=True):
        raise NotImplementedError()

    @classmethod
    def _get_data(cls, path):
        import yaml

        if isinstance(path, (str, unicode)):
            if os.path.exists(path):
                with open(path, "r") as config_file:
                    data = yaml.safe_load(config_file)
                    data["path"] = path
                    return data
            else:
                raise IOError('Path "{}" not found'.format(path))
        else:
            raise NotImplementedError()

    @classmethod
    def _get_error(cls, field_name, path):
        return 'Key "{}" not found in config "{}"'.format(field_name, path)

    @classmethod
    def log_errors(cls, config_name, path, errors_list):
        from character_library.logger import BaseLogger
        BaseLogger().error('{} in path "{}" skipped because:\n{}'.format(
            config_name, path, "\n".join(errors_list)
        ))
