import os.path


class ConfigWriter(object):
    @classmethod
    def write(cls, config, path, is_update):
        if is_update:
            if not os.path.exists(path):
                raise IOError('File "{}" not found'.format(path))
        raw_data = cls.dump(config)
        with open(path, "w+") as fp:
            import json
            json.dump(raw_data)

    @classmethod
    def dump(cls, config):
        raise NotImplementedError()
