import os
from character_library.common import StandaloneConfig


class PairBasedConfig(StandaloneConfig):
    pair_extension = None

    def __init__(self, name, path):
        super(PairBasedConfig, self).__init__(name, path)
        if self._path:
            self._pairingCheck()

    @property
    def pair_path(self):
        return "{}.{}".format(self.path_without_extension, self.pair_extension)

    def _pairingCheck(self):
        if not os.path.exists(self.config_path) or not os.path.exists(self.pair_path):
            from character_library.exceptions.config_exceptions import PairPathValidationError
            raise PairPathValidationError(self)
