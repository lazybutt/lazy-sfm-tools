from character_library.common import StandaloneConfig


class ConfigValidator(object):
    def __init__(self, library):
        from character_library.common.library import ConfigLibrary
        if not isinstance(library, ConfigLibrary):
            raise TypeError('Validator accept instance of Config Library, received: "{}"'.format(type(library)))
        self._library = library

    @property
    def _items(self):
        return self._library.items

    @_items.setter
    def _items(self, items):
        setattr(self._library, "_items", items)

    def validate(self):
        filtered_items = []
        for item in self._items:
            if self.validate_item(item):
                filtered_items.append(item)
            elif self._library.has_logger():
                self._library.get_logger().warning('file "{}" was ignored because'.format(item.config_path))
        raise NotImplementedError("Validation method not not implemented")

    def validate_item(self, item):
        """

        Args:
            item (StandaloneConfig):

        Returns:

        """
        raise NotImplementedError("Validation method not not implemented")

