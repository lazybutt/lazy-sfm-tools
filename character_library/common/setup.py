import os

import yaml

from character_library.common import PairBasedConfig, StandaloneConfig
from filesystem.valve import GameInfoFile

_SCAN_ALL_MOD_KEYWORD = "$scanAllMods\\"
_FIND_ONE_MOD_KEYWORD = "$findOneMod\\"
_CONFIG_PATH = os.path.join(os.path.dirname(__file__), "resources\\config.yml")


class Setup(object):
    def __init__(self, config_path, alias_to_library):
        """

        Args:

            alias_to_library (dict[str, out ConfigLibrary):
        """
        self._search_mods = GameInfoFile().getSearchMods()
        self._default_mod = "usermod"
        with open(config_path, 'r') as config_file:
            config = yaml.safe_load(config_file)["config"]
            for alias, library in alias_to_library.items():
                lib_config = library.configuration
                config_class = lib_config.config_class
                if alias not in config.keys():
                    raise KeyError('Missed config for "{}" ({})'
                                   .format(alias, config_class.__name__))
                params = config[alias]
                if issubclass(config_class, StandaloneConfig):
                    config_class.extension = params["extension"]
                if issubclass(config_class, PairBasedConfig):
                    config_class.pair_extension = params["pair_extension"]
                self._setup_library_configuration(lib_config, params)
                library.init_library()

    def _setup_library_configuration(self, lib_config, params):
        lib_config.dump_path = self._get_dump_path(params)
        lib_config.search_folders = self._get_search_folders(params)

    def _get_dump_path(self, params):
        dump_path = params["dump_path"]
        for search_mod in self._search_mods:
            guess_path = os.path.join(search_mod, dump_path)
            if os.path.exists(guess_path):
                return guess_path
        new_dump_path = os.path.join(self._default_mod, dump_path)
        dir_with_dump = os.path.dirname(new_dump_path)
        if not os.path.exists(dir_with_dump):
            os.makedirs(dir_with_dump)
        with open(new_dump_path, "w+") as dump_file:
            dump_file.write("[]")
        return new_dump_path

    def _get_search_folders(self, params):
        def resolve_path(path_to_resolve):
            if _SCAN_ALL_MOD_KEYWORD in path_to_resolve:
                path_without_keyword = path_to_resolve.replace(_SCAN_ALL_MOD_KEYWORD, "")
                return [os.path.join(search_mod, path_without_keyword)
                        for search_mod in self._search_mods]
            elif _FIND_ONE_MOD_KEYWORD in path_to_resolve:
                path_without_keyword = path_to_resolve.replace(_FIND_ONE_MOD_KEYWORD, "")
                for search_mod in self._search_mods:
                    guess_path = os.path.join(search_mod, path_without_keyword)
                    if os.path.exists(guess_path):
                        return [guess_path]
                new_dir_path = os.path.join(self._default_mod, path_without_keyword)
                if not os.path.exists(new_dir_path):
                    os.makedirs(new_dir_path)
                return [new_dir_path]
            return [path_to_resolve]

        search_paths = params["search_folders"]
        if not isinstance(search_paths, list):
            search_paths = [search_paths]
        resolved_search_paths = []
        for path in search_paths:
            resolved_search_paths.extend(resolve_path(path))
        return resolved_search_paths
