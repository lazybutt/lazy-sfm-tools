import json
import os

import scandir
from character_library.logger import BaseLogger
from character_library.utils.common import log_and_raise
from character_library.utils.file_utils import get_extension, isfile_or_raise, exists_or_raise, \
    check_extension_or_raise, check_access_or_raise


class ConfigLibrary(object):
    _instance = None
    _logger = None
    _status_listeners = []

    # region SETUP CONFIG LIBRARY FUNCTION
    @classmethod
    def _send_status_to_listeners(cls, text):
        listeners = [listener for listener in cls._status_listeners]
        for listener in listeners:
            try:
                listener.status_changed(text)
            except (AttributeError, TypeError):
                cls._status_listeners.remove(listener)

    @classmethod
    def add_status_listener(cls, status_listener):
        if not hasattr(status_listener, "status_changed"):
            raise TypeError("Expected class with method status_changed(text: str)"
                            .format(BaseLogger.__class__.__name__))
        cls._status_listeners.append(status_listener)

    @classmethod
    def remove_status_listener(cls, status_listener):
        if status_listener in cls._status_listeners:
            cls._status_listeners.remove(status_listener)

    @classmethod
    def has_logger(cls):
        return cls._logger is not None

    @classmethod
    def get_logger(cls):
        if cls.has_logger():
            raise ReferenceError("Attempting to get a logger before initialization")
        return cls._logger

    @classmethod
    def set_logger(cls, logger):
        if not isinstance(logger, BaseLogger):
            raise TypeError("Expected class must be instance of {}".format(BaseLogger.__class__.__name__))
        cls._logger = logger
    # endregion

    def __init__(self):
        from character_library.common.library_configuration import LibraryConfiguration
        self._configuration = LibraryConfiguration()
        self._items = []
        self._filter = None
        self._initialized = False

    @property
    def configuration(self):
        return self._configuration

    @property
    def is_initialized(self):
        return self._initialized

    @property
    def items(self):
        if not self.is_initialized:
            self.init_library()
        return self._items

    def init_library(self, force_update=False):
        filter_class = self._configuration.filter_class

        self._send_status_to_listeners('Initialization "{}" library...'.format(filter_class.__name__))
        self._items = []
        if not force_update:
            self._load_dump()
        if not self._items:
            self._update_library()
            self.dump()
        self._initialized = True
        self._filter = filter_class(self._items)

    def _update_library(self):
        config_class = self._configuration.config_class
        config_reader = self._configuration.config_reader
        config_name = config_class.__name__

        self._send_status_to_listeners('Updating "{}" library...'.format(config_class.__name__))
        paths = []
        for path in self._configuration.search_folders:
            for root, _, files in scandir.walk(path):
                for file_item in files:
                    if file_item.lower().endswith("."+config_class.extension.lower()):
                        paths.append(os.path.join(root, file_item))
        self._items = []
        for path in paths:
            config = self._try_read_config_data(path, config_name, config_reader)
            if config is not None:
                self._items.append(config)

    def _load_dump(self):
        from lazy_sfm_tools.files.json_loader import JsonLoader
        config_class = self._configuration.config_class
        config_reader = self._configuration.config_reader
        config_name = config_class.__name__

        self._send_status_to_listeners('Loading "{}" library from dump file...'.format(config_class.__name__))
        with open(self._configuration.dump_path, 'r') as dump_file:
            cfg_dumps = JsonLoader.load(dump_file)
            items = []
            for cfg_dump in cfg_dumps:
                if self._is_config_exists(cfg_dump):
                    config = self._try_read_config_data(cfg_dump, config_name, config_reader)
                    if config is not None:
                        items.append(config)
            self._items = items

    @staticmethod
    def _try_read_config_data(cfg_data_or_path, config_name, config_reader):
        try:
            return config_reader.read(cfg_data_or_path, check_needed=True)
        except Exception, error:
            path = "unknown"
            if isinstance(cfg_data_or_path, dict) and "path" in cfg_data_or_path:
                path = cfg_data_or_path["path"]
            elif isinstance(cfg_data_or_path, (str, unicode)):
                path = cfg_data_or_path
            config_reader.log_errors(config_name, path, [
                "- file corrupted or unexpected error while proceed data from config. "
                "Error text: {}".format(config_name, path, error.message)
            ])

    def validation(self):
        self._send_status_to_listeners('Validate "{}" library...'.format(self._configuration.config_class_name))
        for validator in self.configuration.validators:
            validator.validate()

    def dump(self):
        from character_library.common import StandaloneConfig
        config_writer = self._configuration.config_writer
        config_dumps = []
        for config in self._items:  # type: StandaloneConfig
            dump = config_writer.dump(config)
            config_dumps.append(dump)
        with open(self._configuration.dump_path, 'w') as dump_file:
            json.dump(config_dumps, dump_file)

    def create(self, name, path=None):
        self._configuration.config_class(name, path)

    @property
    def filter(self):
        if not self.is_initialized:
            self.init_library()
        return self._filter

    def save(self, config):
        existed_config = self._find_by_path(config.config_path)
        if existed_config:
            self._items.pop(existed_config)
        config.save(config.config_path)
        self._items.append(config)
        self.dump()

    def _find_by_path(self, path):
        for config in self._items:
            if config.config_path.lower() == path.lower():
                return config

    def delete(self, config):
        from character_library.common import StandaloneConfig
        idx = self._items.index(config)
        if idx != -1:
            log_and_raise(KeyError, "The config to be deleted "
                                    "is not part of the {}".format(self.__class__.__name__), self._logger)
        config = self._items.pop(idx)  # type: StandaloneConfig
        if not config.is_virtual:
            self._is_config_path_deletable_or_throw(config)
            os.remove(config.config_path)

    @staticmethod
    def _is_config_exists(config_dump):
        config_path = config_dump["path"]
        if config_path is not None:
            if not os.path.exists(config_path):
                return False
        return True

    def _is_config_path_deletable_or_throw(self, config):
        config_path = config.config_path
        isfile_or_raise(config_path, 'Path "{}" is not file'.format(config_path), self._logger)
        exists_or_raise(config_path, 'File "{}" not found'.format(config_path), self._logger)
        check_extension_or_raise(
            path=config_path,
            allowed_ext="." + self._configuration.config_class.extension,
            logger=self._logger,
            raise_message="{} has not rights to remove files with {} extension".format(
                self.__class__.__name__, get_extension(config_path)
            )
        )
        check_access_or_raise(config_path, 'File "{}" is occupied by another process, terminate all '
                                           'programs in which the file is open, then repeat the procedure, '
                                           'or delete file manually and run a complete library update', self._logger)
