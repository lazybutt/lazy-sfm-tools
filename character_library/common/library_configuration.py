from character_library.common.config_reader import ConfigReader


class LibraryConfiguration(object):
    def __init__(self):
        self._validators = []
        self._config_class = None
        self._filter_class = None
        self._search_folders = None
        self._dump_path = None
        self._config_reader = None
        self._config_writer = None

    @property
    def config_class_name(self):
        return self.config_class.__name__

    @property
    def filter_class_name(self):
        return self.filter_class.__name__

    def has_config_class(self):
        return self._config_class is not None

    @property
    def config_class(self):
        if self._config_class is None:
            raise ReferenceError("Attempting to get a config class before initialization")
        return self._config_class

    @config_class.setter
    def config_class(self, config_class):
        from character_library.common import StandaloneConfig
        if not issubclass(config_class, StandaloneConfig):
            raise TypeError("Expected class must be subclass of {}".format(StandaloneConfig.__name__))
        self._config_class = config_class

    def has_filter_class(self):
        return self._filter_class is not None

    @property
    def filter_class(self):
        if self._filter_class is None:
            raise ReferenceError("Attempting to get a filter class before initialization")
        return self._filter_class

    @filter_class.setter
    def filter_class(self, filter_class):
        from character_library.common.config_filter import ConfigFilter
        if not issubclass(filter_class, ConfigFilter):
            raise TypeError("Expected class must be subclass of {}".format(ConfigFilter.__name__))
        self._filter_class = filter_class

    def has_search_folders(self):
        return self._search_folders is not None

    @property
    def search_folders(self):
        if self._search_folders is None:
            raise ReferenceError("Attempting to get a search folders before initialization")
        return self._search_folders

    @search_folders.setter
    def search_folders(self, search_folders):
        if isinstance(search_folders, (list, tuple)):
            if not all([isinstance(search_folder, (str, unicode)) for search_folder in search_folders]):
                raise TypeError("Excepted instance of List[str, unicode]")
            self._search_folders = search_folders
        elif isinstance(search_folders, (str, unicode)):
            self._search_folders = [search_folders]
        else:
            raise TypeError("Excepted instance of str | unicode | List[str, unicode]")

    def has_dump_path(self):
        return self._dump_path is not None

    @property
    def dump_path(self):
        if self._dump_path is None:
            raise ReferenceError("Attempting to get a dump path before initialization")
        return self._dump_path

    @dump_path.setter
    def dump_path(self, dump_path):
        if not isinstance(dump_path, (str, unicode)):
            raise TypeError("Excepted instance of str | unicode")
        self._dump_path = dump_path

    @property
    def validators(self):
        return self._validators

    @validators.setter
    def validators(self, validators):
        self._validators = validators

    def has_config_reader(self):
        return self._config_reader is not None

    @property
    def config_reader(self):
        """Receiver of config reader

        Returns:
            ConfigReader: config data reader.
        """
        if self._config_reader is None:
            raise ReferenceError("Attempting to get a config reader before initialization")
        return self._config_reader

    @config_reader.setter
    def config_reader(self, config_reader):
        from character_library.common.config_reader import ConfigReader
        if not issubclass(config_reader, ConfigReader):
            raise TypeError("Excepted subclass of {}".format(ConfigReader.__name__))
        self._config_reader = config_reader

    def has_config_writer(self):
        return self._config_writer is not None

    @property
    def config_writer(self):
        if self._config_writer is None:
            raise ReferenceError("Attempting to get a config reader before initialization")
        return self._config_writer

    @config_writer.setter
    def config_writer(self, config_writer):
        from character_library.common.config_writer import ConfigWriter
        if not issubclass(config_writer, ConfigWriter):
            raise TypeError("Excepted subclass of {}".format(ConfigWriter.__name__))
        self._config_writer = config_writer
