class ConfigFields(object):
    ANY = "*"
    DEFAULT = "default"
    NAME = "name"
    PATH = "path"

    @classmethod
    def validate(cls, obj):
        """Validate config object for missing fields.

        Args:
            obj (dict): config to validate.
        """
        missed_fields = []
        for field_name in cls.__dict__.values():
            if not callable(field_name) and field_name not in [cls.ANY, cls.DEFAULT]:
                if field_name not in obj.keys():
                    missed_fields.append(field_name)
        if missed_fields:
            raise KeyError('Config validation error, {} config missed fields: {}. Config data: {}'
                           .format(cls.__name__.replace('Fields', ''), ', '.join(missed_fields), obj))
