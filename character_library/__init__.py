import os

from character_library.common import PairBasedConfig as _PairBasedConfig
from character_library.common.setup import Setup as _Setup

from config.model_line_config import ModelLineConfig as _ModelLineConfig
from config import reader as _readers
from config import writer as _writers
from config.character_override_config import CharacterOverrideConfig as _CharacterOverrideConfig
from config.character_override_template_config import \
    CharacterOverrideTemplateConfig as _CharacterOverrideTemplateConfig
from config.character_part_config import \
    CharacterPartConfig as _CharacterPartConfig
from library import ModelLineLibrary as _ModelLineLibrary
from library import CharacterOverrideLibrary as _CharacterOverrideLibrary
from library import CharacterOverrideTemplateLibrary as _CharacterOverrideTemplateLibrary
from library import CharacterPartLibrary as _CharacterPartLibrary
from filter import ModelLineFilter as _ModelLineFilter
from filter import CharacterOverrideFilter as _CharacterOverrideFilter
from filter import CharacterOverrideTemplateFilter as _CharacterOverrideTemplateFilter
from filter import CharacterPartFilter as _CharacterPartFilter

# region Setup Library
class _Libraries(object):
    _model_line = None
    _character_part = None
    _character_override = None
    _character_override_template = None

    @property
    def model_line(self):
        """Receive model line library.

        Returns:
            _ModelLineLibrary: model line library.
        """
        if not self._model_line:
            self._model_line = _ModelLineLibrary()
        return self._model_line

    @property
    def character_part(self):
        """Receive character part library.

        Returns:
            _CharacterPartLibrary: character part library.
        """
        if not self._character_part:
            self._character_part = _CharacterPartLibrary()
        return self._character_part

    @property
    def character_override(self):
        """Receive character override library.

        Returns:
            _CharacterOverrideLibrary: character override library.
        """
        if not self._character_override:
            self._character_override = _CharacterOverrideLibrary()
        return self._character_override

    @property
    def character_override_template(self):
        """Receive character override template library.

        Returns:
            _CharacterOverrideTemplateLibrary: character override template library.
        """
        if not self._character_override_template:
            self._character_override_template = _CharacterOverrideTemplateLibrary()
        return self._character_override_template

    def update(self, model_line=False, character_part=False, character_override=False, character_override_template=False,
               all_libraries=False):
        if model_line or all_libraries:
            self.model_line.init_library(True)
        if character_part or all_libraries:
            self.character_part.init_library(True)
        if character_override_template or all_libraries:
            self.character_override_template.init_library(True)
        if character_override or all_libraries:
            self.character_override.init_library(True)

    def add_status_listener(self, listener):
        self.model_line.add_status_listener(listener)
        self.character_part.add_status_listener(listener)
        self.character_override.add_status_listener(listener)
        self.character_override_template.add_status_listener(listener)

    def remove_status_listener(self, listener):
        self.model_line.remove_status_listener(listener)
        self.character_part.remove_status_listener(listener)
        self.character_override.remove_status_listener(listener)
        self.character_override_template.remove_status_listener(listener)


Libraries = _Libraries()

# region Setup Filters
class _Filters(object):
    @property
    def model_line(self):
        """Receive model line filter.

        Returns:
            _ModelLineFilter: model line filter.
        """
        return Libraries.model_line.filter

    @property
    def character_part(self):
        """Receive character part filter.

        Returns:
            _CharacterPartFilter: character part filter.
        """
        return Libraries.character_part.filter

    @property
    def character_override(self):
        """Receive character override filter.

        Returns:
            _CharacterOverrideFilter: character override filter.
        """
        return Libraries.character_override.filter

    @property
    def character_override_template(self):
        """Receive character override template filter.

        Returns:
            _CharacterOverrideTemplateFilter: character override template filter.
        """
        return Libraries.character_override_template.filter


Filters = _Filters()

_CONFIG_PATH = os.path.join(os.path.dirname(__file__), "resources\\config.yml")
_ALIASES_TO_LIBRARY = {
    "model_line": Libraries.model_line,
    "character_part": Libraries.character_part,
    "character_override_template": Libraries.character_override_template,
    "character_override": Libraries.character_override,
}

_Setup(_CONFIG_PATH, _ALIASES_TO_LIBRARY)
