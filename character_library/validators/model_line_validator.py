from character_library.common.fields import ConfigFields
from character_library.common.library import ConfigLibrary
from character_library.common.validator import ConfigValidator


class ModelLineValidator(ConfigValidator):
    def __init__(self, library, model_line_selector, body_part_selector):
        """

        Args:
            library (ConfigLibrary):
            model_line_selector (function):
        """
        super(ModelLineValidator, self).__init__(library)
        self._model_lines_to_parts = {}
        self._model_line_selector = model_line_selector
        self._body_part_selector = body_part_selector
        self._model_lines_loaded = False

    def validate_item(self, item):
        if not self._model_lines_loaded:
            self._init_model_lines_to_parts()
            self._model_lines_loaded = True

        model_line = self._model_line_selector(item).lower()
        available_model_lines = self._model_lines_to_parts.keys()
        is_any_model_line = model_line == ConfigFields.ANY
        if model_line not in available_model_lines and not is_any_model_line:
            return False
        if self._body_part_selector:
            body_part = self._body_part_selector(item).lower()
            available_body_parts = self._model_lines_to_parts[model_line]
            is_any_body_part = body_part == ConfigFields.ANY
            if body_part not in available_body_parts and not is_any_body_part:
                return False
        return True

    def _init_model_lines_to_parts(self):
        import character_library
        all_parts = []
        for item in character_library.Libraries.model_line.items:
            name = item.name.lower()
            parts = [part.lower() for part in item.parts]
            all_parts.append(parts)
            self._model_lines_to_parts[name] = parts
        self._model_lines_to_parts.keys()
