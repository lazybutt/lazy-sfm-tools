import sfmApp

from PySide import QtGui
from PySide import QtCore

from lazy_sfm_tools.ui.label import ClickableLabel
from lazy_sfm_tools.ui.list import BufferedListWidget
from lazy_sfm_tools.ui.icons import AnimsetIcons
from lazy_sfm_tools.commons.enums.animset_type import AnimationSetType
from lazy_sfm_tools.selectors import AnimationSetsSelector
from lazy_sfm_tools.selectors import AnimSetSelectContext
from lazy_sfm_tools.materials.overrides import OverrideMaterialsApplier

from character_library import Filters
from character_library.config.reader import CharacterPartConfigReader
from character_library.config.fields import CharacterPartFields
from character_library.config.fields import CharacterPartFields as fields
from character_library.systems.material_override_system.orida_subsystem import OridaOverridesAdapter
from character_library.utils.characters_receiver import CharactersReceiver
from character_library.systems.material_override_system.restore_subsystem.restore_materials import RestoreMaterials

_doc_root = sfmApp.GetDocumentRoot()
_backup_attribute_name = "overrideMaterialsBackup"
_folder = "usermod\\scripts\\backups\\override materials"


class OverrideMaterialsEngine(QtGui.QDialog):
    def __init__(self, *args, **kwargs):
        super(OverrideMaterialsEngine, self).__init__(*args, **kwargs)
        if self._fix_materials_if_needed():
            self.close()
        else:
            self._developer_mode = kwargs["dev"] if "dev" in kwargs else False

            self._current_character = None
            self._current_variant = None
            self._current_body_part = None
            self._current_model_line = None
            self._is_character_selected = False

            self._filter = Filters.character_override
            font_creator = lambda size: QtGui.QFont("Arial", size, QtGui.QFont.Bold)

            self._header = QtGui.QLabel(
                "<strong style='font-size: 26pt'> Override Materials </strong><strong style='font-size: 18pt'><font color=\"red\">" +
                str(2.0) + "</font></strong><br/><strong>created <strong>by</strong> artist for artists</strong>")
            self._header.setAlignment(QtCore.Qt.AlignCenter)

            link = "https://gitlab.com/lazybutt/lazy-sfm-tools"
            self._footer = QtGui.QLabel(
                "<strong style='font-size: 12px'>Powered by "
                "<a style='text-decoration:none' href=\"{0}\"><font color=\"green\">Lazy SFM Tools</font></a> and "
                "<a style='text-decoration:none' href=\"{0}\"><font color=\"green\">Character Library</font></a>. "
                "<a style='text-decoration:none' href=\"{0}\"><font color=\"yellow\">Support me.</font></a></strong>"
                    .format(link)
            )
            self._footer.setOpenExternalLinks(True)
            self._footer.setAlignment(QtCore.Qt.AlignCenter)
            self._footer.setFixedHeight(20)

            self._name_field = QtGui.QLineEdit()
            self._name_field.setFixedHeight(40)
            self._name_field.setFont(font_creator(12))
            self._name_field.setDisabled(True)
            self._name_field.textChanged.connect(self._update_character_list)
            self._name_field.setPlaceholderText("Enter character...")

            self._orida_mode = QtGui.QCheckBox()
            self._orida_mode.setText("enable orida mode")
            self._orida_mode.setFont(font_creator(10))
            self._orida_mode.setChecked(False)
            self._orida_mode.stateChanged.connect(self._anim_set_list_changed)

            self._anim_set_list_widget = BufferedListWidget()
            self._anim_set_list_widget.setFont(font_creator(12))
            self._anim_set_list_widget.itemSelectionChanged.connect(self._anim_set_list_changed)

            self._select_character_list = BufferedListWidget()
            self._select_character_list.setFont(font_creator(12))
            self._select_character_list.setDisabled(True)
            self._select_character_list.itemDoubleClicked.connect(self._character_list_item_double_clicked)

            self._select_override_list_widget = BufferedListWidget()
            self._select_override_list_widget.hide()
            self._select_override_list_widget.setFont(font_creator(12))
            self._select_override_list_widget.setDisabled(True)
            self._select_override_list_widget.itemDoubleClicked.connect(self._apply_override)
            self._select_override_list_widget.itemSelectionChanged.connect(lambda: self._apply_button.setText("Apply"))

            self._back_button = self._setup_back_button(font_creator(12))
            self._apply_button = self._setup_apply_button()

            self._main_layout = QtGui.QGridLayout()
            self._main_layout.setAlignment(QtCore.Qt.AlignCenter)

            self._main_layout.addWidget(self._anim_set_list_widget, 0, 0, 4, 1)
            self._main_layout.addWidget(self._header, 0, 1)
            self._main_layout.addWidget(self._back_button, 1, 1)
            self._main_layout.addWidget(self._name_field, 1, 1)
            self._main_layout.addWidget(self._select_character_list, 2, 1, 1, 1)
            self._main_layout.addWidget(self._select_override_list_widget, 2, 1, 1, 1)
            self._main_layout.addWidget(self._apply_button, 3, 1)
            self._main_layout.addWidget(self._orida_mode, 4, 0)
            self._main_layout.addWidget(self._footer, 4, 1)

            self._fill_anim_sets()
            self.setStyleSheet('font-family: Arial; font-size: 16; font-weight: bold')
            self.setLayout(self._main_layout)
            self.setFixedHeight(400)
            self.adjustSize()
            self.setFixedSize(self.size())
            self.setWindowTitle('Override Materials Engine 2.0 (OME 2.0) by LazyButt')
            self.exec_()

    def _setup_back_button(self, font):
        back_button = ClickableLabel()
        back_button.setFont(font)
        back_button.setAlignment(QtCore.Qt.AlignCenter)
        back_button.setFixedHeight(40)
        back_button.setStyleSheet(
            'font-family: Arial; font-size: 16pt; font-weight: bold; '
            'background-color: #282828; border: 1px solid #777777; padding: 4px')
        back_button.clicked.connect(self._deselect_character)
        return back_button

    def _setup_apply_button(self):
        apply_button = ClickableLabel()
        apply_button.setText("Apply")
        apply_button.setAlignment(QtCore.Qt.AlignCenter)
        apply_button.setFixedHeight(40)
        apply_button.setStyleSheet(
            'font-family: Arial; font-size: 11pt; font-weight: bold; '
            'background-color: #282828; border: 1px solid #777777; padding: 4px')
        apply_button.clicked.connect(self._apply_override)
        apply_button.setVisible(False)
        return apply_button

    def _fix_materials_if_needed(self):
        need_to_close = False
        if RestoreMaterials.session_has_broken_materials() and RestoreMaterials.have_backup():
            question = QtGui.QMessageBox(self)
            question.setText("Session have broken materials.")
            question.setInformativeText("There is broken materials,\ndo you open Override Materials just to fix it?")
            just_fix_btn = question.addButton("Yes, just fix it", QtGui.QMessageBox.YesRole)
            fix_and_open_btn = question.addButton("Fix it and open script", QtGui.QMessageBox.YesRole)
            question.setDefaultButton(just_fix_btn)
            question.addButton("No, just open script", QtGui.QMessageBox.NoRole)
            ret = question.exec_()
            if ret == 0:
                need_to_close = True
            if ret in [0, 1]:
                RestoreMaterials.restore(only_broken=True)
                if RestoreMaterials.session_has_broken_materials():
                    QtGui.QMessageBox.warning(self, "Not all materials could be recovered")

        return need_to_close

    def _anim_set_list_changed(self):
        self._name_field.setDisabled(False)
        self._back_button.setDisabled(False)
        self._select_override_list_widget.setDisabled(False)
        self._select_character_list.setDisabled(False)

        animset = self._anim_set_list_widget.get_current_buffered_data()
        self._update_current_character_part(animset)

    def _update_current_character_part(self, animSet):
        characterPart = CharacterPartConfigReader.read_from_animset(animSet)
        if characterPart is not None:
            if characterPart.character != fields.ANY:
                variant = self._current_variant \
                    if characterPart.variant is None \
                    else characterPart.variant
                self._select_character(characterPart.character, variant)
            self._current_body_part = characterPart.body_part
            self._current_model_line = characterPart.model_line
        self._clear_and_fill_overrides()

    def _select_character(self, character, variant='*'):
        self._apply_button.setVisible(True)
        self._is_character_selected = True
        self._current_character = character
        self._current_variant = variant
        self._back_button.setText(
            "<strong style='font-size: 12pt'><font color=\"green\">{} ({})</font> [Back]</strong>"
                .format(character, variant)
        )
        self._select_override_list_widget.show()
        self._back_button.show()
        self._select_character_list.hide()
        self._name_field.hide()

    def _apply_override(self):
        animSet = self._anim_set_list_widget.get_current_buffered_data()
        override = self._select_override_list_widget.get_current_buffered_data()
        if animSet and override:
            raw_override_data = override.build_raw_override()
            OverrideMaterialsApplier.for_animset(animSet).add_overrides(raw_override_data)
        current_override_name = self._select_override_list_widget.currentItem().text()
        if len(current_override_name) > 27:
            current_override_name = current_override_name[:27] + "..."
        self._apply_button.setText('<font color="green">"{}" applied</font>'.format(current_override_name))
        RestoreMaterials.backup()

    def _deselect_character(self):
        self._apply_button.setVisible(False)
        self._is_character_selected = False
        self._current_character = CharacterPartFields.ANY
        self._current_variant = CharacterPartFields.ANY
        self._select_character_list.show()
        self._name_field.show()
        self._select_override_list_widget.hide()
        self._back_button.hide()
        self._name_field.setFocus()
        self._name_field.selectAll()

    def _fill_anim_sets(self):
        anim_sets = AnimationSetsSelector \
            .animation_sets_in_shot(AnimSetSelectContext.IN_CURRENT_SHOT, AnimationSetType.MODEL)
        current_anim_set = AnimationSetsSelector.current_animation_set
        set_current_item_func = lambda: None
        for anim_set in anim_sets:
            character_part = CharacterPartConfigReader.read_from_animset(anim_set)
            if character_part:
                name = anim_set.GetName().replace('_', ' ')
                item = self._anim_set_list_widget.add_buffered_data_item(
                    text=name, data=anim_set, icon=AnimsetIcons.GAMEMODEL.value
                )
                if str(anim_set.GetId()) == str(current_anim_set.GetId()):
                    set_current_item_func = \
                        lambda: self._anim_set_list_widget.setCurrentItem(item)
        set_current_item_func()

    def _clear_and_fill_overrides(self, character=None, variant=None):
        self._select_override_list_widget.clear()
        if not self._orida_mode.isChecked():
            self._fill_character_system_overrides(character, variant)
        else:
            self._fill_orida_overrides(character)

    def _fill_orida_overrides(self, character):
        animset = self._anim_set_list_widget.get_current_buffered_data()
        overrides_with_compatibility = OridaOverridesAdapter.overrides_by_character_with_compatibility(
            animset.gameModel,
            self._current_character if character is None else character
        )
        for item in overrides_with_compatibility:
            compatibility_rate = int(round(item.score, 2) * 100)
            if self._developer_mode:
                item_text = "{} ({}%)".format(item.override.name, compatibility_rate)
            else:
                item_text = "{}".format(item.override.name)
            self._select_override_list_widget.add_buffered_data_item(item_text, item.override)

    def _fill_character_system_overrides(self, character, variant):
        ch_filter = self._filter \
            .by_model_line(self._current_model_line) \
            .by_character(self._current_character if character is None else character) \
            .by_body_part(self._current_body_part) \
            .by_variant(self._current_variant if variant is None else variant)
        multiple_model_lines = len(ch_filter.model_lines) > 1
        for item in ch_filter.items:
            item_text = '{} ({})'.format(item.name, item.model_line) if multiple_model_lines else item.name
            self._select_override_list_widget.add_buffered_data_item(item_text, item)

    def _character_list_item_double_clicked(self):
        if self._is_not_orida_mode():
            data = self._select_character_list.get_current_data()
            character, variant = data.character, data.variant
        else:
            character, variant = self._select_character_list.currentItem().text(), "*"
        self._select_character(character, variant)
        self._clear_and_fill_overrides(character, variant)

    def _update_character_list(self):
        self._select_character_list.clear()
        search_query = self._name_field.text()
        if search_query.strip():
            if self._is_not_orida_mode():
                character_to_variant_list = CharactersReceiver.find_by_name_and_model_line(
                    search_query, self._current_model_line, find_in_variants=True
                )

                for character_to_variant in character_to_variant_list:
                    display_name = str(character_to_variant)
                    self._select_character_list.add_data_item(display_name, character_to_variant)
            else:
                overrides = OridaOverridesAdapter.overrides_by_character(search_query, False)
                character_names = list(set([override.character for override in overrides]))
                self._select_character_list.addItems(character_names)

    def _is_not_orida_mode(self):
        return not self._orida_mode.isChecked()
