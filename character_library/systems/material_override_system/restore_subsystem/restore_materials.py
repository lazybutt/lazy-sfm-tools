import json
import os
from datetime import datetime

import sfmApp
import vsUtils
from vs.movieobjects import CDmeAnimationSet

from lazy_sfm_tools.materials.overrides import OverrideMaterialElement, OverrideMaterialsApplier
from character_library.systems.material_override_system.restore_subsystem.restore_materials_item import BackupOverridesItem


class RestoreMaterials(object):
    _doc_root = sfmApp.GetDocumentRoot()
    _backup_attribute_name = "overrideMaterialsBackup"
    _folder = "usermod\\scripts\\backups\\override materials"

    @classmethod
    def backup(cls):
        """Backup all override materials in session and create attribute with backup filename."""
        shot_to_animsets = [
            filter(cls._filter_anim_sets_key, shot.animationSets)
            for shot in sfmApp.GetShots()
        ]

        backup_overrides = []

        for animsets in shot_to_animsets:
            for anim_set in animsets:
                if any([mat is None for mat in anim_set.gameModel.materials]):
                    continue
                anim_set_id = str(anim_set.GetId())
                overrides = []

                for material in anim_set.gameModel.materials:
                    if material is not None:
                        for attr_name in vsUtils.getDynamicAttributeNames(material):
                            overrides.append({
                                "mat_name": material.GetName(),
                                "attr_name": attr_name,
                                "value": material.GetAttribute(attr_name).GetValueAsString("", 100000)
                            })
                if overrides:
                    backup_overrides.append(
                        BackupOverridesItem(anim_set_id, overrides).serialize()
                    )

        filename = cls._generate_backup_file(backup_overrides)

        cls._set_backup_attribute(filename)

    @classmethod
    def restore(cls, only_broken=True):
        """Restore all override materials in session from backup or only broken (empty or null filled).

        Args:
            only_broken: if true, only broken (empty or null filled) overrides
                will be restored else all overrides will be restored.
        """
        data = cls._get_backup_data()
        if data:
            for shot in sfmApp.GetShots():
                ids_to_animsets = {str(anim_set.GetId()): anim_set for anim_set in shot.animationSets}
                for backup_overrides_item in data:
                    if backup_overrides_item.anim_set_id in ids_to_animsets.keys():
                        anim_set = ids_to_animsets[backup_overrides_item.anim_set_id]
                        if only_broken:
                            if anim_set.gameModel.HasAttribute("materials"):
                                materials = anim_set.gameModel.materials
                                all_materials_ok = all([material is not None for material in materials])
                                materials_not_empty = materials.Count() != 0
                                if all_materials_ok and materials_not_empty:
                                    continue
                        overrides = []
                        for override in backup_overrides_item.overrides:
                            overrides.append(
                                OverrideMaterialElement(
                                    [override["mat_name"]],
                                    override["attr_name"],
                                    override["value"],
                                )
                            )
                        applier = OverrideMaterialsApplier.for_animset(anim_set)
                        applier.make_empty_overrides()
                        applier.add_overrides(overrides)

    @classmethod
    def session_has_broken_materials(cls):
        """Check if session have animation sets with broken materials.

        Returns:
            bool: true if there are animation sets with broken materials.
        """
        return len(cls.get_anim_sets_with_broken_materials()) > 0

    @classmethod
    def get_anim_sets_with_broken_materials(cls):
        """Receiver of animation sets with broken materials.

        Returns:
            list[CDmeAnimationSet]: animation sets with broken materials.
        """
        found_anim_sets = []
        is_materials_empty = lambda animset: animset.gameModel.materials.Count() == 0
        have_null_filled_materials = lambda animset: any([mat is None for mat in animset.gameModel.materials])
        for shot in sfmApp.GetShots():
            anim_sets = filter(cls._filter_anim_sets_key, shot.animationSets)
            for anim_set in anim_sets:
                if have_null_filled_materials(anim_set) or is_materials_empty(anim_set):
                    found_anim_sets.append(anim_sets)
        return found_anim_sets

    @classmethod
    def _get_backup_data(cls):
        if cls._doc_root.HasAttribute(cls._backup_attribute_name):
            filename = cls._doc_root.GetValue(cls._backup_attribute_name)
            path = os.path.join(cls._folder, filename)
            if os.path.exists(path):
                backup_items = json.load(open(path, "r"))
                return [BackupOverridesItem.deserialize(item)
                        for item in backup_items]

    @classmethod
    def _set_backup_attribute(cls, filename):
        if not cls._doc_root.HasAttribute(cls._backup_attribute_name):
            cls._doc_root.AddAttributeAsString(cls._backup_attribute_name)
        cls._doc_root.SetValue(cls._backup_attribute_name, filename)

    @classmethod
    def _generate_backup_file(cls, data):
        filename = cls._get_or_generate_filename()
        if not os.path.exists(cls._folder):
            os.makedirs(cls._folder)
        path = os.path.join(cls._folder, filename)
        with open(path, "w") as f:
            json.dump(data, f)
        return filename

    @classmethod
    def have_backup(cls):
        return cls._doc_root.HasAttribute(cls._backup_attribute_name)

    @classmethod
    def _get_or_generate_filename(cls):
        if cls.have_backup():
            return cls._doc_root.GetValue(cls._backup_attribute_name)

        new_file_name = str(datetime.now())
        for char_to_replace in [" ", ":", "-", "."]:
            new_file_name = new_file_name.replace(char_to_replace, "_")

        return new_file_name

    @staticmethod
    def _filter_anim_sets_key(anim_set):
        return anim_set.HasAttribute("gameModel") \
               and anim_set.gameModel.HasAttribute("materials")

