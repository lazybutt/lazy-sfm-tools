class BackupOverridesItem(object):
    def __init__(self, anim_set_id, overrides):
        """DTO object for storing backup material overrides.

        Args:
            anim_set_id: animation set id.
            overrides: overrides data.
        """
        self.anim_set_id = anim_set_id
        self.overrides = overrides

    def serialize(self):
        """BackupOverridesItem to dict.

        Returns:

        """
        return {
            "anim_set_id": self.anim_set_id,
            "overrides": self.overrides,
        }

    @classmethod
    def deserialize(cls, obj):
        """Dict to BackupOverridesItem.

        Args:
            obj (dict): dict object to deserialize.

        Returns:
            BackupOverridesItem: DTO object for storing backup material overrides.
        """
        return cls(
            obj["anim_set_id"],
            obj["overrides"]
        )
