from lazy_sfm_tools.materials.overrides import OverrideMaterialElement

class OridaOverride(object):
    def __init__(self, name=None, character=None, attributes=None, file_path=None):
        """

        Args:
            name (str|unicode): material overrides name.
            character (str|unicode): character name.
            attributes (list[OverrideMaterialElement]): list of elements with material overrides info.
            file_path (str): path to orida format file.
        """
        self._name = str(name)
        self._attributes = attributes
        self._character = str(character)
        self._file_path = str(file_path)

    @property
    def name(self):
        """Receive material overrides name.

        Returns:
            str: material overrides name.
        """
        return self._name

    @property
    def character(self):
        """Receive character name.

        Returns:
            str: character name.
        """
        return self._character

    @property
    def attributes(self):
        """Receive list of elements with material overrides info.

        Returns:
            list[OverrideMaterialElement]: list of elements with material overrides info.
        """
        return self._attributes

    @property
    def file_path(self):
        """Receive path to orida format file.

        Returns:
            str: path to orida format file.
        """
        return self._file_path

    def build_raw_override(self):
        """Adapter method for character system format.

        Returns:
            list[OverrideMaterialElement]: list of elements with material overrides info.
        """
        return self.attributes
