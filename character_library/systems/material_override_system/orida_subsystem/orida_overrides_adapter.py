from lazy_sfm_tools.files.file_structure import GamePath


class _OridaOverrideWithCompatibility(object):
    def __init__(self, override):
        self._override = override
        self._score = 0.0

    @property
    def override(self):
        return self._override

    @property
    def score(self):
        return self._score

    @score.setter
    def score(self, value):
        self._score = value

class _OridaOverridesAdapter(object):
    def __init__(self):
        import os
        from lazy_sfm_tools.files.json_loader import JsonLoader
        from lazy_sfm_tools.materials.overrides import OverrideMaterialElement
        from character_library.systems.material_override_system.orida_subsystem.orida_override import OridaOverride

        self._items = []
        path = GamePath.script(r"sfm\animset\Tools\oridafiles\RedDaz")
        if not path.is_valid:
            pass
        for root, _, files in os.walk(path.absolute_path):
            for file_name in files:
                if ".json" in file_name.lower():
                    path = os.path.join(root, file_name)
                    try:
                        obj = JsonLoader.load(open(path, 'r'))
                        name = os.path.basename(path).split('.')[0].replace('_', ' ')
                        character = os.path.dirname(path).replace('/', '\\').split('\\')[-1]
                        attributes = [
                            OverrideMaterialElement(override["applyTo"], override["name"], override["value"])
                            for override in obj["allOverrides"]
                        ]
                        self._items.append(OridaOverride(name, character, attributes))
                    except:
                        import sfm
                        sfm.Msg('broken orida override: "{}"\n'.format(path))

    @property
    def characters(self):
        return [item.character for item in self._items]

    def overrides_by_character(self, character, exact=True):
        if exact:
            return [item for item in self._items if item.character.lower() == character.lower()]
        return [item for item in self._items if character.lower() in item.character.lower()]

    def overrides_by_character_with_compatibility(self, gamemodel, character, exact=True):
        from lazy_sfm_tools.materials.common import MaterialsData
        mat_names_in_gm = MaterialsData.from_gamemodel(gamemodel).material_names

        def sort_overrides_by_compatibility(override_with_compatibility):
            counter = 0
            mat_names_in_overrides = []
            for attr in override_with_compatibility.override.attributes:
                mat_names_in_overrides.extend(attr.for_materials)
            mat_names_in_overrides = dict.fromkeys(mat_names_in_overrides).keys()

            for mat_name_in_overrides in mat_names_in_overrides:
                if mat_name_in_overrides in mat_names_in_gm:
                    counter += 1
            override_with_compatibility.score = (float(counter) / float(len(mat_names_in_gm)))

            return -override_with_compatibility.score

        overrides_with_compatibility = map(
            _OridaOverrideWithCompatibility,
            self.overrides_by_character(character, exact)
        )
        sorted_overrides = sorted(
            overrides_with_compatibility,
            key=sort_overrides_by_compatibility
        )

        filtered_overrides = filter(
            lambda sorted_override: sorted_override.score,
            sorted_overrides
        )

        return filtered_overrides


OridaOverridesAdapter = _OridaOverridesAdapter()
