from lazy_sfm_tools.ui.label import ClickableLabel
from character_library import Libraries

from PySide import QtGui
from PySide import QtCore

from character_library.systems.material_override_system.override_materials_engine import OverrideMaterialsEngine
from character_library.systems import CharacterSpawnEngine


class CharacterLibraryLauncher(QtGui.QDialog):
    def __init__(self, *args, **kwargs):
        super(CharacterLibraryLauncher, self).__init__(*args, **kwargs)
        layout = QtGui.QVBoxLayout()
        header = QtGui.QLabel(
            "<strong style='font-size: 34pt'> Character Library </strong><br/>"
            "<strong style='font-size: 14pt'>created by artist for artists</strong>")
        header.setAlignment(QtCore.Qt.AlignCenter)
        buttonLayout = QtGui.QHBoxLayout()
        self._spawnButton = ClickableLabel(text="Spawn")
        self._overridesButton = ClickableLabel(text="Materials")
        buttonLayout.addWidget(self._spawnButton)
        buttonLayout.addWidget(self._overridesButton)
        self._updateButton = ClickableLabel(text="Update Library")
        self._status = QtGui.QLabel()
        self._spawnButton.clicked.connect(lambda: self._run(CharacterSpawnEngine))
        self._overridesButton.clicked.connect(lambda: self._run(OverrideMaterialsEngine))
        self._updateButton.clicked.connect(self._update)
        self._status.setFont(QtGui.QFont("Arial", 18, QtGui.QFont.Bold))
        self._spawnButton.setFixedWidth(200)
        self._overridesButton.setFixedWidth(200)
        self._spawnButton.setFixedHeight(150)
        self._overridesButton.setFixedHeight(150)
        frame = QtGui.QFrame()
        frame.setFrameShape(QtGui.QFrame.HLine)
        layout.addWidget(header)
        layout.addWidget(frame)
        layout.addLayout(buttonLayout)
        layout.addWidget(self._updateButton)
        layout.addWidget(self._status)
        self._status.hide()
        style = 'font-family: Arial; font-size: {}pt; font-weight: bold; ' \
                'background-color: #282828; border: 1px solid #777777; padding: 4px'
        self._spawnButton.setAlignment(QtCore.Qt.AlignCenter)
        self._overridesButton.setAlignment(QtCore.Qt.AlignCenter)
        self._updateButton.setAlignment(QtCore.Qt.AlignCenter)
        self._spawnButton.setStyleSheet(style.format(28))
        self._overridesButton.setStyleSheet(style.format(28))
        self._updateButton.setStyleSheet(style.format(22))
        self.setLayout(layout)
        self.setWindowTitle("Character Library Launcher by LazyButt")
        self.setWindowFlags(QtCore.Qt.WindowContextHelpButtonHint)
        self.exec_()

    def _run(self, classToRun):
        self.close()
        classToRun()

    def _update(self):
        from lazy_sfm_tools.ui.pyqt_utils import WidgetBatchActions
        WidgetBatchActions.hide(
            self._spawnButton,
            self._overridesButton,
            self._updateButton
        )
        self._status.show()
        Libraries.add_status_listener(self)
        Libraries.update(all_libraries=True)
        Libraries.remove_status_listener(self)
        self._status.hide()
        WidgetBatchActions.show(
            self._spawnButton,
            self._overridesButton,
            self._updateButton
        )
        self.adjustSize()

    def status_changed(self, text):
        self._status.setText(str(text))
        self.adjustSize()
        self.repaint()
