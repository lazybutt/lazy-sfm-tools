from PySide import QtGui


class CharacterSetupTabs(QtGui.QTabWidget):
    def __init__(self, model_line, character, variant, *args, **kwargs):
        super(CharacterSetupTabs, self).__init__(*args, **kwargs)

        self._init_tabs(model_line, character, variant)
        self._add_tabs()

    def _init_tabs(self, model_line, character, variant):
        from character_library.systems.spawn_system.character_parts.main_widget import CharacterPartsMainWidget

        self._select_parts = CharacterPartsMainWidget(model_line, character, variant)

    def _add_tabs(self):
        self.addTab(self._select_parts, 'Select parts')

    def spawn(self):
        from lazy_sfm_tools.selectors.shot_selector import ShotSelector
        from lazy_sfm_tools.utils.elements import create_element
        from lazy_sfm_tools.factories.animset_factory import AnimationSetFactory
        from lazy_sfm_tools.rigging.animset_constrain_tool import AnimsetConstrainTool
        from lazy_sfm_tools.materials.overrides import OverrideMaterialsApplier
        from character_library.systems.spawn_system.character_parts.spawn_data import SpawnItem

        spawn_group = self._select_parts.spawn_data
        current_shot = ShotSelector.current_shot
        parent_character_group = create_element("DmeDag", spawn_group.name)
        current_shot.scene.children.AddToTail(parent_character_group)

        parts = {}
        for spawn_item in spawn_group.items:  # type: SpawnItem
            animset = AnimationSetFactory.create_model_animset(
                spawn_item.name,
                spawn_item.model_path,
                current_shot,
                parent_character_group
            )
            if spawn_item.overrides:
                OverrideMaterialsApplier\
                    .for_animset(animset)\
                    .add_overrides(spawn_item.overrides)
            parts[spawn_item.body_part] = animset

        for spawn_item in spawn_group.items:
            if spawn_item.constrain_to:
                if spawn_item.constrain_to in parts.keys():
                    master = parts[spawn_item.constrain_to]
                    slave = parts[spawn_item.body_part]
                    AnimsetConstrainTool.constrain_animsets(master, slave)