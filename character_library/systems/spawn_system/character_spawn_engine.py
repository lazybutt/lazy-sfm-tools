from PySide import QtGui
from PySide import QtCore

from character_library.systems.material_override_system.restore_subsystem.restore_materials import RestoreMaterials

_style = """
    QTabWidget
    {
        font-size: 14px;
        font-weight: bold;
    }

    QTabWidget::pane:top
    {
        border: 0.1ex groove #555555;
    }

    QTabWidget::pane:bottom
    {
        border: 0.1ex groove #555555;
    }

    QTabWidget::pane:left
    {
        border: 0.1ex groove #555555;
    }

    QTabWidget::pane:right
    {
        border: 0.1ex groove #555555;
    }
"""

class CharacterSpawnEngine(QtGui.QDialog):
    def __init__(self, *args, **kwargs):
        super(CharacterSpawnEngine, self).__init__(*args, **kwargs)
        from lazy_sfm_tools.ui.label import ClickableLabel
        from character_library.ui.selector import CharacterSelectionWidget

        self._header = QtGui.QLabel(
            "<strong style='font-size: 24pt'> Spawn Engine </strong><br/><strong>created <strong>by</strong> artist for artists</strong>")
        self._header.setAlignment(QtCore.Qt.AlignCenter)
        link = "https://gitlab.com/lazybutt/lazy-sfm-tools"
        self._footer = QtGui.QLabel(
            "<strong style='font-size: 11px'>Powered by "
            "<a style='text-decoration:none' href=\"{0}\"><font color=\"green\">Lazy SFM Tools</font></a> and "
            "<a style='text-decoration:none' href=\"{0}\"><font color=\"green\">Character Library.</font></a> "
            "<a style='text-decoration:none' href=\"{0}\"><font color=\"yellow\">Support me.</font></a></strong>"
                .format(link)
        )
        self._footer.setOpenExternalLinks(True)
        self._footer.setAlignment(QtCore.Qt.AlignRight)

        self._layout = QtGui.QGridLayout()
        self._characterSearch = CharacterSelectionWidget(cancelButton=False)
        self._characterSearch.itemSelected.connect(self.add_character_tab)
        self._tabWidget = QtGui.QTabWidget()
        self._tabWidget.setTabsClosable(True)
        self._tabWidget.tabCloseRequested.connect(self._tab_closed)
        self._btnLay = QtGui.QVBoxLayout()
        self._spawnButton = ClickableLabel(text="<strong style='font-size: 40px'>D<br/>O<br/>N<br/>E<br/></strong>")
        self._btnLay.addWidget(self._spawnButton)
        self._btnGroupBox = QtGui.QGroupBox()
        self._btnGroupBox.setLayout(self._btnLay)
        self._spawnButton.clicked.connect(self._spawn)
        self._layout.addWidget(self._header, 0, 0)
        self._layout.addWidget(self._characterSearch, 1, 0)
        self._layout.addWidget(self._tabWidget, 0, 1, 2, 1)
        self._layout.addWidget(self._btnGroupBox, 0, 2, 2, 1)
        self._layout.addWidget(self._footer, 2, 0, 1, 3)
        self._spawnButton.setEnabled(False)
        self._tabWidget.setVisible(False)
        self.setStyleSheet('font-size:14px; font-weight:bold')
        self.setWindowTitle('Character Spawn Engine (CSE 2.0) by LazyButt''')
        self.setLayout(self._layout)
        self.setStyleSheet(_style)
        self.exec_()

    def _tab_closed(self, index):
        self._tabWidget.removeTab(index)
        if self._tabWidget.count() == 0:
            self._spawnButton.setEnabled(False)
            self._tabWidget.setVisible(False)
            self.adjustSize()

    def add_character_tab(self, character):
        from lazy_sfm_tools.utils.value_buffer import BufferByID
        from character_library.systems.spawn_system.character_setup_tabs import CharacterSetupTabs

        character = BufferByID[character]
        self._spawnButton.setEnabled(True)
        self._tabWidget.setVisible(True)
        self._tabWidget.addTab(CharacterSetupTabs(self._characterSearch.filter.model_line, character[0], character[1]), character[0])

    def _spawn(self):
        for idx in range(self._tabWidget.count()):
            self._tabWidget.widget(idx).spawn()
        RestoreMaterials.backup()
        self.close()
