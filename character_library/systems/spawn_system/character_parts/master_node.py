from PySide import QtGui


class CharacterPartMasterNode(QtGui.QGroupBox):
    def __init__(self, modelLine, character, variant='*', title=None, *args, **kwargs):
        super(CharacterPartMasterNode, self).__init__(*args, **kwargs)
        from character_library.systems.spawn_system.character_parts.title_node import CharacterPartTitleNode

        self._title = title  # type: CharacterPartTitleNode
        self._model_line = modelLine
        self._character = character
        self._variant = variant
        self._layout = QtGui.QVBoxLayout()
        self._to_adjust = []
        self._character_parts = {}
        self._layout.setSpacing(0)
        self._init_character_parts()
        self._layout.setSpacing(17)
        self._layout.setContentsMargins(10, 12, 10, 12)
        # self._masterCharacterPart.layout().setSpacing()
        self.setLayout(self._layout)

    def _init_character_parts(self):
        from character_library.systems.spawn_system.character_parts.node import CharacterPartNode
        from character_library import Libraries

        filter = Libraries.model_line.filter.by_name(self._model_line)
        for body_part in filter.parts:
            node = CharacterPartNode(self._model_line, body_part, self._character, self._variant)
            self._character_parts[body_part] = node
            self._layout.addWidget(node)
        for constrain in filter.constrains:
            self._character_parts[constrain.master].add_dependent_body_parts(self._character_parts[constrain.slave])
            # if child in self._characterParts and parent in self._characterParts:
        if self._title is not None:
            self._title.add_node_dependencies(self.character_parts)

    @property
    def character_parts(self):
        return self._character_parts.values()
