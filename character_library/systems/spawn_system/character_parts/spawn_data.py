class SpawnGroup(object):
    def __init__(self, name, items):
        self._name = name
        self._items = items

    @property
    def name(self):
        return self._name

    @property
    def items(self):
        return self._items

class SpawnItem(object):
    def __init__(self, name, body_part, model_path, constrain_to, overrides):
        self._name = name
        self._body_part = body_part
        self._model_path = model_path
        self._constrain_to = constrain_to
        self._overrides = overrides

    @property
    def name(self):
        return self._name

    @property
    def body_part(self):
        return self._body_part

    @property
    def model_path(self):
        return self._model_path

    @property
    def constrain_to(self):
        return self._constrain_to

    @property
    def overrides(self):
        return self._overrides
