from PySide import QtGui
from PySide import QtCore

class CharacterPartTitleNode(QtGui.QGroupBox):
    modelEnabledChanged = QtCore.Signal()
    overrideEnabledChanged = QtCore.Signal()
    constrainEnabledChanged = QtCore.Signal()

    def __init__(self, *args, **kwargs):
        super(CharacterPartTitleNode, self).__init__(*args, **kwargs)
        self._filler = QtGui.QWidget()
        self._filler.setFixedSize(1, 1)
        self._model_master_check_box = QtGui.QCheckBox()
        self._model_label = QtGui.QLabel('Model')
        self._override_master_check_box = QtGui.QCheckBox()
        self._override_label = QtGui.QLabel('Override')
        self._constrain_master_check_box = QtGui.QCheckBox()
        self._constrain_label = QtGui.QLabel('Constrain')
        self._main_layout = QtGui.QHBoxLayout()
        self._model_master_check_box.setChecked(True)
        self._override_master_check_box.setChecked(True)
        self._constrain_master_check_box.setChecked(True)
        # self._modelMasterCheckBox.setEnabled(False)
        # self._overrideMasterCheckBox.setEnabled(False)
        # self._constrainMasterCheckBox.setEnabled(False)
        self._model_master_check_box.stateChanged.connect(self._model_master_check_box_changed)
        self._override_master_check_box.stateChanged.connect(self._override_master_check_box_changed)
        self._constrain_master_check_box.stateChanged.connect(self._constrain_master_check_box_changed)
        self._main_layout.addWidget(self._filler)
        self._main_layout.addWidget(self._model_master_check_box)
        self._main_layout.addWidget(self._model_label)
        self._main_layout.addWidget(self._override_master_check_box)
        self._main_layout.addWidget(self._override_label)
        self._main_layout.addWidget(self._constrain_master_check_box)
        self._main_layout.addWidget(self._constrain_label)
        self._main_layout.setContentsMargins(10, 10, 10, 10)
        self._dependent_character_parts = []
        self.setStyleSheet('font-size:14px; font-weight:bold; color:#98fb98; background-color:#444444')
        self.setLayout(self._main_layout)

    @property
    def widgets_to_adjust(self):
        return [self._filler, self._model_master_check_box, self._model_label,
                self._override_master_check_box, self._override_label,
                self._constrain_master_check_box, self._constrain_label]

    def add_node_dependency(self, depend):
        depend.check_box_state_changed.connect(self._character_part_check_boxes_changes)
        self._dependent_character_parts.append(depend)

    def add_node_dependencies(self, partList):
        for item in partList:
            self.add_node_dependency(item)

    def _model_master_check_box_changed(self):
        if self._model_master_check_box.checkState() == QtCore.Qt.PartiallyChecked:
            self._model_master_check_box.setCheckState(QtCore.Qt.Checked)
        for characterPart in self._dependent_character_parts:
            characterPart.check_box_state_changed.disconnect(self._character_part_check_boxes_changes)
            characterPart.model_check_box.setCheckState(self._model_master_check_box.checkState())
            characterPart.check_box_state_changed.connect(self._character_part_check_boxes_changes)

    def _override_master_check_box_changed(self):
        if self._override_master_check_box.checkState() == QtCore.Qt.PartiallyChecked:
            self._override_master_check_box.setCheckState(QtCore.Qt.Checked)
        for characterPart in self._dependent_character_parts:
            characterPart.check_box_state_changed.disconnect(self._character_part_check_boxes_changes)
            characterPart.override_check_box.setCheckState(self._override_master_check_box.checkState())
            characterPart.check_box_state_changed.connect(self._character_part_check_boxes_changes)

    def _constrain_master_check_box_changed(self):
        if self._constrain_master_check_box.checkState() == QtCore.Qt.PartiallyChecked:
            self._constrain_master_check_box.setCheckState(QtCore.Qt.Checked)
        for characterPart in self._dependent_character_parts:
            characterPart.check_box_state_changed.disconnect(self._character_part_check_boxes_changes)
            characterPart.constrain_check_box.setCheckState(self._constrain_master_check_box.checkState())
            characterPart.check_box_state_changed.connect(self._character_part_check_boxes_changes)

    # def _updateStates(self, state):
    #     def a(senderF, checkBoxList, f):
    #         if senderF.checkState() == Qt.PartiallyChecked:
    #             senderF.setCheckState(Qt.Checked)
    #         for checkBox in checkBoxList:
    #             checkBox.stateChanged.disconnect(f)
    #             checkBox.setCheckState(senderF.checkState())
    #             checkBox.stateChanged.connect(f)
    #             checkBox.setTristate(False)
    #
    #     def b(mainCheckBox, checkBoxList, f):
    #         mainCheckBox.stateChanged.disconnect(f)
    #         if all([checkBox.checkState() == Qt.Unchecked for checkBox in checkBoxList]):
    #             mainCheckBox.setCheckState(Qt.Unchecked)
    #         elif all([checkBox.checkState() == Qt.Checked for checkBox in checkBoxList]):
    #             mainCheckBox.setCheckState(Qt.Checked)
    #         else:
    #             mainCheckBox.setCheckState(Qt.PartiallyChecked)
    #         mainCheckBox.stateChanged.connect(f)
    #
    #     sender = self.sender()
    #     if sender is self._modelMasterCheckBox:
    #         a(sender, [item.ModelCheckBox for item in self._dependentCharacterParts], self._updateStates)
    #     elif sender is self._overrideMasterCheckBox:
    #         a(sender, [item.OverrideCheckBox for item in self._dependentCharacterParts], self._updateStates)
    #     elif sender is self._constrainMasterCheckBox:
    #         a(sender, [item.ConstrainCheckBox for item in self._dependentCharacterParts], self._updateStates)
    #     if sender in self._modelCheckBoxes:
    #         b(self._modelsCheckBox, self._modelCheckBoxes, self._updateStates)
    #     if sender in self._overrideCheckBoxes:
    #         b(self._overrideCheckBox, self._overrideCheckBoxes, self._updateStates)
    #     if sender in self._lockToCheckBoxes:
    #         b(self._constrainsCheckBox, self._lockToCheckBoxes, self._updateStates)
    #     if self._overrideCheckBox.checkState() == Qt.Checked:
    #         self._matOverrideGroup.setEnabled(True)
    #     elif self._overrideCheckBox.checkState() == Qt.Unchecked:
    #         self._matOverrideGroup.setEnabled(False)
    #     if (self._constrainsCheckBox.checkState() == Qt.Checked or self._constrainsCheckBox.checkState() == Qt.PartiallyChecked) and \
    #         (self._modelsCheckBox.checkState() == Qt.Checked or self._modelsCheckBox.checkState() == Qt.PartiallyChecked):
    #         self._constrainGroup.setEnabled(True)
    #     elif self._constrainsCheckBox.checkState() == Qt.Unchecked or self._modelsCheckBox.checkState() == Qt.Unchecked:
    #         self._constrainGroup.setEnabled(False)

    def _character_part_check_boxes_changes(self):
        def checkBoxChecker(dependentCharacterParts, index, masterCheckBox):
            something = [characterPart.check_box_states[index] for characterPart in dependentCharacterParts]
            if all([state == QtCore.Qt.Checked for state in something]):
                masterCheckBox.setCheckState(QtCore.Qt.Checked)
            elif all([state == QtCore.Qt.Unchecked for state in something]):
                masterCheckBox.setCheckState(QtCore.Qt.Unchecked)
            else:
                masterCheckBox.setCheckState(QtCore.Qt.PartiallyChecked)

        self._model_master_check_box.stateChanged.disconnect(self._model_master_check_box_changed)
        self._override_master_check_box.stateChanged.disconnect(self._override_master_check_box_changed)
        self._constrain_master_check_box.stateChanged.disconnect(self._constrain_master_check_box_changed)
        checkBoxChecker(self._dependent_character_parts, 0, self._model_master_check_box)
        checkBoxChecker(self._dependent_character_parts, 1, self._override_master_check_box)
        checkBoxChecker(self._dependent_character_parts, 2, self._constrain_master_check_box)
        self._model_master_check_box.stateChanged.connect(self._model_master_check_box_changed)
        self._override_master_check_box.stateChanged.connect(self._override_master_check_box_changed)
        self._constrain_master_check_box.stateChanged.connect(self._constrain_master_check_box_changed)
