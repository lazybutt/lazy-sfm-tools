from PySide import QtGui
from PySide import QtCore

from character_library.common.fields import ConfigFields


class CharacterPartsMainWidget(QtGui.QGroupBox):
    def __init__(self, modelLine, character, variant='*', *args, **kwargs):
        super(CharacterPartsMainWidget, self).__init__(*args, **kwargs)

        self._model_line = modelLine
        self._character = character
        self._variant = variant
        self._init_ui()

    def _init_ui(self):
        from character_library.systems.spawn_system.character_parts.title_node import CharacterPartTitleNode
        from character_library.systems.spawn_system.character_parts.master_node import CharacterPartMasterNode
        from character_library import Filters

        self._layout = QtGui.QVBoxLayout()
        self._headerGroup = QtGui.QGroupBox()
        self._title = CharacterPartTitleNode()
        variants = Filters.character_part\
            .by_model_line(self._model_line)\
            .by_character(self._character).variants
        if self._variant == ConfigFields.ANY:
            if "default" in variants:
                self._variant = "default"
            elif "Default" in variants:
                self._variant = "Default"

        self._list = CharacterPartMasterNode(self._model_line, self._character, self._variant, self._title)
        self._init_header_ui(variants)
        self._layout.addWidget(self._headerGroup)
        self._layout.addWidget(self._title)
        self._layout.addWidget(self._list)
        self._layout.setContentsMargins(0, 0, 0, 0)
        self._layout.setSpacing(0)
        self.setStyleSheet('font-size:14px; font-weight:bold')
        self.adjust_widgets()
        self._headerGroup.adjustSize()
        self.setLayout(self._layout)

    def _init_header_ui(self, variants):

        header_layout = QtGui.QHBoxLayout()
        model_line_label = QtGui.QLabel('ModelLine: <font color="green">{}</font>'.format(self._model_line))
        character_label = QtGui.QLabel('Character: <font color="green">{}</font>'.format(self._character))
        model_line_label.setAlignment(QtCore.Qt.AlignCenter)
        character_label.setAlignment(QtCore.Qt.AlignCenter)

        self.init_variants_selector(variants)

        self._set_variant(self._variant)

        # headerLayout.setAlignment(Qt.AlignLeading)
        def create_separator():
            label = QtGui.QLabel('|')
            label.setAlignment(QtCore.Qt.AlignCenter)
            return label
        header_layout.addWidget(model_line_label)
        header_layout.addWidget(create_separator())
        header_layout.addWidget(character_label)
        header_layout.addWidget(create_separator())
        header_layout.addWidget(self._variant_selector)
        header_layout.setContentsMargins(10, 10, 10, 10)
        self._variant_selector.currentIndexChanged.connect(self._variant_changed)
        self._headerGroup.setStyleSheet('background-color: #303030')
        self._headerGroup.setLayout(header_layout)

    def init_variants_selector(self, variants):
        from lazy_sfm_tools.ui.combo_box import ComboBoxAutoCompletion

        self._variant_selector = ComboBoxAutoCompletion()
        self._variant_selector.setDisabled(True)
        is_single_variant = len(variants) == 1
        if not is_single_variant:
            variants.append("*")
            self._variant_selector.setEnabled(True)
        self._variant_selector.addItems(variants)

    def _set_variant(self, variant):
        idx = self._variant_selector.findText(variant)
        is_found = idx != -1
        if is_found:
            self._variant_selector.setCurrentIndex(idx)
        return is_found

    def _variant_changed(self):
        for characterPart in self._list.character_parts:
            characterPart.init(self._model_line, characterPart.body_part, self._character,
                               self._variant_selector.currentText())
            characterPart.set_defaults()

    @property
    def spawn_data(self):
        from character_library.systems.spawn_system.character_parts.spawn_data import SpawnItem
        from character_library.systems.spawn_system.character_parts.spawn_data import SpawnGroup

        items = []
        for characterPart in self._list.character_parts:
            if characterPart.is_spawn_needed:
                overrides = \
                    characterPart.selected_override_materials.build_raw_override() \
                        if characterPart.is_override_materials_needed \
                        else []
                constrain_to = characterPart.lock_to if characterPart.is_constrain_to_parent_needed else None
                items.append(
                    SpawnItem(
                        name=characterPart.character + ' ' + characterPart.body_part,
                        body_part=characterPart.body_part,
                        model_path=characterPart.selected_model_path,
                        constrain_to=constrain_to,
                        overrides=overrides
                    )
                )
        return SpawnGroup(self._character, items)

    def adjust_widgets(self):
        toAdjust = [self._title]
        toAdjust.extend(self._list.character_parts)
        count = 0
        for idx in range(len(toAdjust)):
            newLen = len(toAdjust[idx].widgets_to_adjust)
            if newLen > count:
                count = newLen
        for idx in range(count):
            width = 0
            height = 0
            for widget in toAdjust:
                subwidget = widget.widgets_to_adjust[idx]
                if subwidget is not None:
                    subwidget.adjustSize()
                    newWidth = subwidget.width()
                    newHeight = subwidget.height()
                    if newWidth > width:
                        width = newWidth
                    if newHeight > height:
                        height = newHeight
            for widget in toAdjust:
                subwidget = widget.widgets_to_adjust[idx]
                if subwidget is not None:
                    subwidget.setFixedWidth(width)