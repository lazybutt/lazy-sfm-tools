from PySide import QtGui
from PySide import QtCore
from lazy_sfm_tools.files.file_structure import GamePath

class CharacterPartNode(QtGui.QWidget):
    check_box_state_changed = QtCore.Signal()

    def __init__(self, modelLine, bodyPart, character, variant='*', *args, **kwargs):
        super(CharacterPartNode, self).__init__(*args, **kwargs)
        from lazy_sfm_tools.ui.combo_box import ComboBoxAutoCompletion

        self._character = None
        self._dependentList = []
        self._allowLock = False
        self._lockTo = None
        # region Init Widgets
        self._model_check_box = QtGui.QCheckBox()
        self._body_part_label = QtGui.QLabel()
        self._model_selection = ComboBoxAutoCompletion()
        self._override_check_box = QtGui.QCheckBox()
        self._override_selection = ComboBoxAutoCompletion()
        self._lock_to_check_box = QtGui.QCheckBox()
        self._lock_to_label = QtGui.QLabel('to: -')
        # endregion
        # region Setup Widgets
        self._model_check_box.setChecked(True)
        self._override_check_box.setChecked(True)
        self._lock_to_check_box.setChecked(True)
        self._lock_to_check_box.setEnabled(False)
        self._body_part_label.setStyleSheet('font-size:16px; font-weight:bold')
        self._model_selection.setStyleSheet('font-size:13px; font-weight:bold')
        self._override_selection.setStyleSheet('font-size:13px; font-weight:bold')
        self._lock_to_label.setStyleSheet('font-size:14px; font-weight:bold')
        self._lock_to_label.setAlignment(QtCore.Qt.AlignLeft)
        self._model_check_box.stateChanged.connect(self._model_check_box_changed)
        self._override_check_box.stateChanged.connect(self._override_check_box_changed)
        self._lock_to_check_box.stateChanged.connect(self._lock_to_check_box_changed)
        self._model_selection.currentIndexChanged.connect(self._model_changed)
        # endregion
        self.init(modelLine, bodyPart, character, variant)
        self._main_layout = QtGui.QHBoxLayout()
        self._main_layout.addWidget(self._body_part_label)
        self._main_layout.addWidget(self._model_check_box)
        self._main_layout.addWidget(self._model_selection)
        self._main_layout.addWidget(self._override_check_box)
        self._main_layout.addWidget(self._override_selection)
        self._main_layout.addWidget(self._lock_to_check_box)
        self._main_layout.addWidget(self._lock_to_label)
        self._main_layout.setContentsMargins(0, 0, 0, 0)
        self._masterCharacterItem = None
        self.set_defaults()
        self.setLayout(self._main_layout)

    def set_defaults(self):
        override_idx = self._override_selection.findText('Default')
        if override_idx == -1:
            override_idx = self._override_selection.findText('default')
        if override_idx == -1:
            override_idx = self._override_selection.findText('BodyColor')
        if override_idx != -1:
            self._override_selection.setCurrentIndex(override_idx)

        default_models = [
            self._model_selection.itemText(idx)
            for idx in range(self._model_selection.count())
            if "default" in self._model_selection.itemText(idx).lower()
        ]
        import sfm
        if default_models:
            model_idx = self._model_selection.findText(default_models[0])
            self._model_selection.setCurrentIndex(model_idx)

    @property
    def widgets_to_adjust(self):
        return [self._body_part_label, self._model_check_box, self._model_selection,
                self._override_check_box, self._override_selection,
                self._lock_to_check_box, self._lock_to_label]

    @property
    def check_box_states(self):
        return [self._model_check_box.checkState(), self._override_check_box.checkState(),
                self._lock_to_check_box.checkState()]

    @property
    def model_check_box(self):
        return self._model_check_box

    @property
    def override_check_box(self):
        return self._override_check_box

    @property
    def constrain_check_box(self):
        return self._lock_to_check_box

    @property
    def is_spawn_needed(self):
        return self._model_check_box.isChecked() and self._model_check_box.isEnabled()

    @property
    def is_override_materials_needed(self):
        return self._override_check_box.isChecked() and self._override_check_box.isEnabled()

    @property
    def is_constrain_to_parent_needed(self):
        return self._lock_to_check_box.isChecked() and self._lock_to_check_box.isEnabled()

    @property
    def current_model_item(self):
        from character_library.config import CharacterPartConfig
        return self._model_selection.itemData(self._model_selection.currentIndex())  # type: CharacterPartConfig

    @property
    def current_override_materials(self):
        from character_library.config import CharacterOverrideConfig
        return self._override_selection.itemData(self._override_selection.currentIndex())  # type: CharacterOverrideConfig

    @property
    def selected_model_path(self):
        if self.is_spawn_needed:
            return GamePath.model(self.current_model_item.pair_path)
        else:
            return None

    @property
    def selected_override_materials(self):
        if self.is_override_materials_needed:
            return self.current_override_materials
        else:
            return []

    @property
    def character(self):
        return self._character

    @property
    def body_part(self):
        return self._body_part_label.text()

    @property
    def lock_to(self):
        return self._lockTo

    def init(self, modelLine, bodyPart, character, variant='*'):
        self._character = character
        self._body_part_label.setText(bodyPart)

        self._init_model_selection(modelLine, bodyPart, character, variant)
        self._init_override_selection(modelLine, bodyPart, character, variant)

    def _init_model_selection(self, modelLine, bodyPart, character, variant):
        from character_library import Libraries

        self._model_selection.currentIndexChanged.disconnect(self._model_changed)

        current_model = self._model_selection.currentText() if self._model_selection.count() > 0 else None

        self._model_selection.clear()

        self._fill_items(self._model_selection,
                         Libraries.character_part.filter.by_model_line(modelLine).by_body_part(bodyPart).
                         by_character(character).by_variant(variant).items)
        if current_model is not None:
            idx = self._model_selection.findText(current_model)
            if idx != -1:
                self._model_selection.setCurrentIndex(idx)

        if self._model_selection.count() == 0:
            self._model_check_box.setChecked(False)
            self._model_check_box.setEnabled(False)
        else:
            self._model_check_box.setEnabled(True)

        self._model_selection.currentIndexChanged.connect(self._model_changed)

    def _init_override_selection(self, modelLine, bodyPart, character, variant):
        from character_library import Libraries

        current_override = self._override_selection.currentText() if self._override_selection.count() > 0 else None

        self._override_selection.clear()

        self._fill_items(self._override_selection,
                         Libraries.character_override.filter.by_model_line(modelLine).by_body_part(bodyPart).
                         by_character(character).by_variant(variant).items)
        if current_override is not None:
            idx = self._override_selection.findText(current_override)
            if idx != -1:
                self._override_selection.setCurrentIndex(idx)

        if self._override_selection.count() == 0:
            self._override_check_box.setChecked(False)
            self._override_check_box.setEnabled(False)
        else:
            self._override_check_box.setEnabled(True)

    def set_enabled_locks(self, enabled):
        self._allowLock = enabled
        self._lock_to_check_box.setEnabled(enabled and self._model_check_box.isChecked())
        self._lock_to_label.setEnabled(enabled and self._model_check_box.isChecked())

    def set_lock_to(self, bodyPart):
        self._lockTo = bodyPart
        self._lock_to_label.setText('to: ' + str(bodyPart))

    def add_dependent_body_parts(self, *widgets):
        for widget in widgets:
            widget.set_enabled_locks(True)
            widget.set_lock_to(self.body_part)
            self._dependentList.append(widget)

    def _model_check_box_changed(self):
        enabled = self._model_check_box.isChecked()
        self._body_part_label.setEnabled(enabled)
        self._model_selection.setEnabled(enabled)
        self._override_check_box.setEnabled(enabled)
        self._override_selection.setEnabled(enabled)
        self._lock_to_check_box.setEnabled(enabled and self._allowLock)
        self._lock_to_label.setEnabled(enabled and self._allowLock)
        for item in self._dependentList:
            item.set_enabled_locks(enabled)
        if self._override_selection.count() == 0:
            self._override_check_box.setChecked(False)
            self._override_check_box.setEnabled(False)
        elif self.is_override_materials_needed:
            self._override_check_box.setEnabled(True)
        self.check_box_state_changed.emit()

    def _model_changed(self):
        from character_library import Libraries

        item = self.current_model_item
        current_override = self._override_selection.currentText()
        self._fill_items(self._override_selection,
                         Libraries.character_override.filter.by_model_line(item.model_line).by_body_part(item.body_part).
                         by_character(self._character).by_variant(item.variant).items)
        if self._override_selection.count() == 0:
            self._override_check_box.setChecked(False)
            self._override_check_box.setEnabled(False)
        else:
            self._override_check_box.setEnabled(True)
            idx = self._override_selection.findText(current_override)
            if idx != -1:
                self._override_selection.setCurrentIndex(idx)

    def _override_check_box_changed(self):
        if self._override_selection.count() > 0 and self.is_override_materials_needed:
            self._override_selection.setEnabled(self._override_check_box.isChecked())
        self.check_box_state_changed.emit()

    def _lock_to_check_box_changed(self):
        self._lock_to_label.setEnabled(self._lock_to_check_box.isChecked())
        self.check_box_state_changed.emit()

    @staticmethod
    def _fill_items(combobox, items):
        combobox.clear()
        if len(items) > 0:
            for item in items:
                combobox.addItem(item.name, item)
            combobox.setCurrentIndex(0)