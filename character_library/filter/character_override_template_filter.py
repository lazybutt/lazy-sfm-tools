from character_library.common.config_filter import ConfigFilter as _ConfigFilter
from character_library.config.fields import CharacterOverrideTemplateFields as _fields

class CharacterOverrideTemplateFilter(_ConfigFilter):
    def __init__(self, config_list):
        super(CharacterOverrideTemplateFilter, self).__init__(config_list)

    @property
    def model_lines(self):
        return self._values(_fields.MODEL_LINE)

    @property
    def body_parts(self):
        return self._values(_fields.BODY_PART)

    def by_model_line(self, model_line_name, fullConsistency=True):
        return self._filter(_fields.MODEL_LINE, model_line_name, fullConsistency)

    def by_body_part(self, body_part, fullConsistency=True):
        return self._filter(_fields.BODY_PART, body_part, fullConsistency)
