from character_library.common.config_filter import ConfigFilter as _ConfigFilter
from character_library.config.fields import CharacterPartFields as _fields

class CharacterPartFilter(_ConfigFilter):
    def __init__(self, config_list):
        super(CharacterPartFilter, self).__init__(config_list)

    @property
    def characters(self):
        return self._values(_fields.CHARACTER)

    @property
    def body_parts(self):
        return self._values(_fields.BODY_PART)

    @property
    def variants(self):
        return self._values(_fields.VARIANT)

    @property
    def model_lines(self):
        return self._values(_fields.MODEL_LINE)

    def by_character(self, part_name, fullConsistency=True):
        return self._filter(_fields.CHARACTER, part_name, fullConsistency)

    def by_body_part(self, part_name, fullConsistency=True):
        return self._filter(_fields.BODY_PART, part_name, fullConsistency)

    def by_variant(self, part_name, fullConsistency=True):
        return self._filter(_fields.VARIANT, part_name, fullConsistency)

    def by_model_line(self, part_name, fullConsistency=True):
        return self._filter(_fields.MODEL_LINE, part_name, fullConsistency)
