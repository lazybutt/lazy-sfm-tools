from character_library.common.config_filter import ConfigFilter as _ConfigFilter
from character_library.config.fields import ModelLineFields as _fields

class ModelLineFilter(_ConfigFilter):
    def __init__(self, config_list):
        super(ModelLineFilter, self).__init__(config_list)

    @property
    def parts(self):
        return self._values(_fields.PARTS)

    @property
    def constrains(self):
        return self._values(_fields.CONSTRAINS)
