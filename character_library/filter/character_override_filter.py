from character_library.common.config_filter import ConfigFilter as _ConfigFilter
from character_library.config.fields import CharacterOverrideFields as _fields

class CharacterOverrideFilter(_ConfigFilter):
    def __init__(self, config_list):
        super(CharacterOverrideFilter, self).__init__(config_list)

    @property
    def model_lines(self):
        return self._values(_fields.MODEL_LINE)

    @property
    def body_parts(self):
        return self._values(_fields.BODY_PART)

    @property
    def characters(self):
        return self._values(_fields.CHARACTER)

    @property
    def variants(self):
        return self._values(_fields.VARIANT)

    def by_model_line(self, character_name, fullConsistency=True):
        return self._filter(_fields.MODEL_LINE, character_name, fullConsistency)

    def by_body_part(self, body_part, fullConsistency=True):
        return self._filter(_fields.BODY_PART, body_part, fullConsistency)

    def by_character(self, character_name, fullConsistency=True):
        return self._filter(_fields.CHARACTER, character_name, fullConsistency)

    def by_variant(self, variant, fullConsistency=True):
        return self._filter(_fields.VARIANT, variant, fullConsistency)
