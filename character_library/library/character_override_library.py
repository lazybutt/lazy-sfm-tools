from character_library.common.library import ConfigLibrary as _ConfigLibrary


class CharacterOverrideLibrary(_ConfigLibrary):

    def __init__(self):
        super(CharacterOverrideLibrary, self).__init__()
        from character_library.config import CharacterOverrideConfig
        from character_library.filter import CharacterOverrideFilter
        from character_library.config.reader import CharacterOverrideConfigReader
        from character_library.config.writer import CharacterOverrideConfigWriter
        from character_library.validators import ModelLineValidator

        self.configuration.config_class = CharacterOverrideConfig
        self.configuration.filter_class = CharacterOverrideFilter
        self.configuration.config_reader = CharacterOverrideConfigReader
        self.configuration.config_writer = CharacterOverrideConfigWriter

        model_line_selector = lambda item: item.model_line
        body_part_selector = lambda item: item.body_part
        model_line_validator = ModelLineValidator(self, model_line_selector, body_part_selector)
        self.configuration.validators = [model_line_validator]
