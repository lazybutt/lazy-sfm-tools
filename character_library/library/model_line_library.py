from character_library.common.library import ConfigLibrary as _ConfigLibrary


class ModelLineLibrary(_ConfigLibrary):

    def __init__(self):
        super(ModelLineLibrary, self).__init__()
        from character_library.config import ModelLineConfig as ModelLineConfig
        from character_library.filter import ModelLineFilter as ModelLineFilter
        from character_library.config.reader import ModelLineConfigReader
        from character_library.config.writer import ModelLineConfigWriter

        self.configuration.config_class = ModelLineConfig
        self.configuration.filter_class = ModelLineFilter
        self.configuration.config_reader = ModelLineConfigReader
        self.configuration.config_writer = ModelLineConfigWriter
