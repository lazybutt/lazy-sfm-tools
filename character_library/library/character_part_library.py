from character_library.common.library import ConfigLibrary as _ConfigLibrary


class CharacterPartLibrary(_ConfigLibrary):

    def __init__(self):
        super(CharacterPartLibrary, self).__init__()
        from character_library.config import CharacterPartConfig as _CharacterPartConfig
        from character_library.filter import CharacterPartFilter as _CharacterPartFilter
        from character_library.config.reader import CharacterPartConfigReader
        from character_library.config.writer import CharacterPartConfigWriter
        from character_library.validators import ModelLineValidator

        self.configuration.config_class = _CharacterPartConfig
        self.configuration.filter_class = _CharacterPartFilter
        self.configuration.config_reader = CharacterPartConfigReader
        self.configuration.config_writer = CharacterPartConfigWriter

        model_line_selector = lambda item: item.model_line
        body_part_selector = lambda item: item.body_part
        model_line_validator = ModelLineValidator(self, model_line_selector, body_part_selector)
        self.configuration.validators = [model_line_validator]
