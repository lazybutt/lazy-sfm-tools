from character_library.common.library import ConfigLibrary as _ConfigLibrary


class CharacterOverrideTemplateLibrary(_ConfigLibrary):

    def __init__(self):
        super(CharacterOverrideTemplateLibrary, self).__init__()
        from character_library.config import CharacterOverrideTemplateConfig
        from character_library.filter import CharacterOverrideTemplateFilter
        from character_library.config.reader import CharacterOverrideTemplateConfigReader
        from character_library.config.writer import CharacterOverrideTemplateConfigWriter
        from character_library.validators import ModelLineValidator

        self.configuration.config_class = CharacterOverrideTemplateConfig
        self.configuration.filter_class = CharacterOverrideTemplateFilter
        self.configuration.config_reader = CharacterOverrideTemplateConfigReader
        self.configuration.config_writer = CharacterOverrideTemplateConfigWriter

        model_line_selector = lambda item: item.model_line
        body_part_selector = lambda item: item.body_part
        model_line_validator = ModelLineValidator(self, model_line_selector, body_part_selector)
        self.configuration.validators = [model_line_validator]
