def log_and_raise(exception_class, message, logger=None):
    if logger is not None:
        logger.message(message)
    exception_class(message)
