from character_library import Filters
from character_library.config.fields import CharacterPartFields as fields


class CharactersReceiver(object):
    @classmethod
    def find_by_name_and_model_line(cls, query, model_line, find_in_variants):
        """Find character and variant info via character name/variant and model line.

        Args:
            query (str|unicode): character name/variant name query.
            model_line (str|unicode): name of model line.
            find_in_variants (bool): find by variants too if true else false.

        Returns:
            list[CharacterToVariant]: list of character and variant info.
        """
        character_filter = Filters.character_part.by_model_line(model_line)
        found_character_parts = filter(
            lambda item: cls._have_character_name(item)
                         and cls._is_variant_default(item),
            character_filter.by_character(str(query), False).items
        )
        if find_in_variants:
            found_character_parts.extend(filter(
                lambda item: cls._have_character_name(item)
                             and not cls._is_variant_default(item),
                character_filter.by_variant(str(query), False).items
            ))
        return set([
            CharacterToVariant(
                character_part.character,
                character_part.variant
            ) for character_part in found_character_parts
        ])

    @staticmethod
    def _have_character_name(character_part):
        return character_part.character != fields.ANY

    @staticmethod
    def _is_variant_default(character_part):
        return character_part.variant.lower() in [fields.DEFAULT, fields.ANY]


class CharacterToVariant(object):
    """DTO object for storing character name and variant."""

    def __init__(self, character, variant):
        """CharacterToVariant constructor.

        Args:
            character (str|unicode): character name.
            variant (str|unicode): character variant.
        """
        self.character = character
        self.variant = variant

    def __str__(self):
        return "{} ({})".format(self.variant, self.character) \
            if self.variant.lower() not in [fields.DEFAULT, fields.ANY] \
            else self.character

    def __eq__(self, other):
        return self.variant == other.variant and self.character == other.character

    def __hash__(self):
        return hash(str(self.character + self.variant))

    def __repr__(self):
        class_name = self.__class__.__name__
        args_and_values = ", ".join([
            '{}="{}"'.format(name, value)
            for name, value in self.__dict__.items()
            if not callable(value)
        ])
        return '{}({})'.format(class_name, args_and_values)

