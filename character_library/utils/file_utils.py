import os


def safe_config_delete(library, config):
    if config.is_virtual:
        return
    ext = library.get_config_class().extension
    if config.config_path.endswith("." + ext):
        os.remove(config)
    else:
        raise IOError("{} has not rights to remove files with {} extension".format(library.__name__,
                                                                                   get_extension(config.config_path)))


def drop_extension(path):
    return os.path.splitext(path)[0]


def get_extension(path):
    return os.path.splitext(path)[1]


def isfile_or_raise(path, raise_message, logger=None):
    from character_library.utils.common import log_and_raise
    if not os.path.isfile(path):
        log_and_raise(OSError, raise_message, logger)


def exists_or_raise(path, raise_message, logger=None):
    from character_library.utils.common import log_and_raise
    if not os.path.exists(path):
        log_and_raise(OSError, raise_message, logger)

def check_extension_or_raise(path, allowed_ext, raise_message, logger=None):
    from character_library.utils.common import log_and_raise
    fact_ext = get_extension(path)
    if allowed_ext.lower() != fact_ext.lower():
        log_and_raise(OSError, raise_message, logger)

def check_access_or_raise(path, raise_message, logger):
    from character_library.utils.common import log_and_raise
    if not os.access(path, os.W_OK | os.X_OK):
        log_and_raise(OSError, raise_message, logger)
