
def item_field_in_filter(item_field, allowed_list):
    if isinstance(item_field, (list, tuple)):
        for item in item_field:
            if item.lower() in allowed_list:
                return True
    else:
        if item_field.lower() in allowed_list:
            return True
    return False

