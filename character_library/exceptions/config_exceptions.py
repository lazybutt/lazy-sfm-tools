
class BaseError(Exception):
    def __init__(self, cause=None):
        message = cause if cause is not None else "Unknown Error send log to dev"
        super(BaseError, self).__init__(message)

class ValidationError(BaseError):
    def __init__(self, validator_class, cause=None):
        message = cause if cause is not None else '{}: Validation Error'.format(type(validator_class))
        super(BaseError, self).__init__(message)

class ModelLineValidationError(ValidationError):
    def __init__(self, validator_class, model_line):
        message = 'Model line "{}" not found'.format(model_line)
        super(ModelLineValidationError).__init__(validator_class, message)

class BodyPartValidationError(ValidationError):
    def __init__(self, validator_class, model_line, body_part):
        message = 'Body part "{}" not found in "{}" model line'.format(body_part, model_line)
        super(ModelLineValidationError).__init__(validator_class, message)

class ConfigPathAccessError(Exception):
    def __init__(self, config):
        message = "You are trying to access a path, but the configuration ({}) is virtual and cannot have a path".format(
            config.__class__)
        super(ConfigPathAccessError, self).__init__(message)


class PairPathValidationError(Exception):
    def __init__(self, pair_config):
        message = "The pairing check did not pass. " \
                  "Check for files in the following path: {}.{} (and *.{})".format(pair_config.path_without_extension,
                                                                                   pair_config.extension,
                                                                                   pair_config.pair_extension)
        super(PairPathValidationError, self).__init__(message)


class PartNameConstrainError(Exception):
    def __init__(self, wrong_parts, model_line_config):
        message = 'There are non-existent parts in the list of constraints: {}. ' \
                  'The ModelLine "{}" ({}) contains the following parts: {}'.format(join(wrong_parts),
                                                                                    model_line_config.name,
                                                                                    get_path(model_line_config),
                                                                                    join(model_line_config.parts))
        super(PartNameConstrainError, self).__init__(message)


class RecursiveConstrainError(Exception):
    def __init__(self, model_line_config):
        message = 'Recursive holding is unacceptable. Situation A>B and B>A not allowed please check' \
                  'modelLine "{}" ({})'.format(model_line_config.name, get_path(model_line_config))
        super(RecursiveConstrainError, self).__init__(message)


class UncertaintyOverrideTemplateBinding(Exception):
    def __init__(self, override, duplicated_templates):
        message = 'Unable to attach a template to a material override "{}" ({}). ' \
                  'There are duplicates in the list of templates belonging to the "{}" model line:\n{}' \
            .format(override.name, get_path(override), override.model_line,
                    ['{}. {} ({})\n'.format(idx + 1, template.name, template.config_path)
                     for idx, template in enumerate(duplicated_templates)])
        super(UncertaintyOverrideTemplateBinding, self).__init__(message)


class OverrideTemplateNotFound(Exception):
    def __init__(self, override, template_name):
        message = 'Unable to attach a template to a material override "{}" ({}). ' \
                  'There are no templates with name "{}" belonging to the "{}" model line' \
            .format(override.name, get_path(override), template_name, override.model_line)
        super(OverrideTemplateNotFound, self).__init__(message)


def get_path(config):
    return "VIRTUAL" if config.is_virtual else config.config_path


def join(list_to_join):
    return ', '.join(list_to_join)
