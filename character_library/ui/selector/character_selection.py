from character_library.ui.selector.base_selector import BaseSelectionWidget


class CharacterSelectionWidget(BaseSelectionWidget):
    def __init__(self, modelLine=None, searchQuery='', searchEnabled=True, *args, **kwargs):
        from character_library.ui.filter import CharacterFilterWidget
        super(CharacterSelectionWidget, self).__init__(CharacterFilterWidget, [modelLine, searchQuery, searchEnabled],
                                                       *args, **kwargs)

    def _fill_items(self):
        from PySide import QtGui
        from PySide import QtCore
        from lazy_sfm_tools.utils.value_buffer import BufferByID

        self.items_list.clear()
        if self.filter.search_query.replace(' ', '') != '':
            for item in self.filter.filtered_items:
                witem = QtGui.QListWidgetItem(
                    item[0] if item[1] == '*' or item[1] == 'Default' else "{} ({})".format(item[1], item[0]))
                witem.setData(QtCore.Qt.UserRole, BufferByID.add(item))
                self.items_list.addItem(witem)
