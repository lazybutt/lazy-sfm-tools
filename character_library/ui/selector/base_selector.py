from PySide import QtGui
from PySide import QtCore


class BaseSelectionWidget(QtGui.QGroupBox):
    itemsListChanged = QtCore.Signal()
    itemSelected = QtCore.Signal(object)
    itemsSelected = QtCore.Signal(list)
    cancelClicked = QtCore.Signal()
    itemRightClick = QtCore.Signal()
    customContextMenuRequested = QtCore.Signal(object)

    def __init__(self, filterWidget, defaults, cancelButton=False, withCheckBox=False, *args, **kwargs):
        super(BaseSelectionWidget, self).__init__(*args, **kwargs)
        self._with_check_box = withCheckBox
        self._filter = filterWidget(*defaults)
        self._items_list = QtGui.QListWidget()
        self._cancel_button = QtGui.QPushButton('Cancel')
        self._main_layout = QtGui.QVBoxLayout()

        self._items_list.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self._filter.layout().setContentsMargins(0, 0, 0, 0)
        self._main_layout.addWidget(self._filter)
        self._main_layout.addWidget(self._items_list)
        self._setup_signals_connections()
        if cancelButton:
            self._main_layout.addWidget(self._cancel_button)
        self._fill_items()
        self.setStyleSheet('font-size: 14px; font-weight: bold;')
        self.setLayout(self._main_layout)

    def _setup_signals_connections(self):
        self._filter.filter_changed.connect(self._fill_items)
        self._items_list.itemDoubleClicked.connect(
            lambda: self.itemSelected.emit(
                self.items_list.currentItem().data(QtCore.Qt.UserRole)
            )
        )
        self._items_list.customContextMenuRequested.connect(
            lambda pos: self.customContextMenuRequested.emit(
                self._items_list.mapToGlobal(QtCore.QPoint(0, 0)) + pos
            )
        )
        self._cancel_button.clicked.connect(self.cancelClicked.emit)

    @property
    def filter(self):
        return self._filter

    @property
    def items_list(self):
        return self._items_list

    @property
    def cancel_button(self):
        return self._cancel_button

    def set_multi_selection(self, enabled):
        self.items_list.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection
                                        if enabled else
                                        QtGui.QAbstractItemView.SingleSelection)

    def _fill_items(self):
        from lazy_sfm_tools.utils.value_buffer import BufferByID

        self.items_list.clear()
        for item in self.filter.filtered_items:
            widget_item = QtGui.QListWidgetItem('{} ({})'.format(item.name, item.body_part))
            widget_item.setData(QtCore.Qt.UserRole, BufferByID.add(item))
            self.items_list.addItem(widget_item)

    @property
    def current_item(self):
        return self.items_list.currentItem()

    @property
    def selected_items(self):
        return [item for item in self.items_list.selectedItems()]

    @property
    def current_value(self):
        return self.items_list.currentItem().data(QtCore.Qt.UserRole) \
            if self.items_list.currentRow() != -1 \
            else None

    @property
    def selected_values(self):
        return [item.data(QtCore.Qt.UserRole)
                for item in self.selected_items]





# class CharacterPartSelectionWidget(_SelectionWidget):
#     def __init__(self, modelLine=None, bodyPart=None, character=None, variant=None, searchQuery='', searchEnabled=True,
#                  *args, **kwargs):
#         super(CharacterPartSelectionWidget, self).__init__(CharacterPartFilterWidget,
#                                                            [modelLine, bodyPart, character, variant, searchQuery,
#                                                             searchEnabled], *args, **kwargs)
# #
#
# class CharacterOverrideSelectionWidget(_SelectionWidget):
#     def __init__(self, modelLine=None, bodyPart=None, character=None, variant=None, searchQuery='', searchEnabled=True,
#                  *args, **kwargs):
#         super(CharacterOverrideSelectionWidget, self).__init__(CharacterOverrideFilterWidget,
#                                                                [modelLine, bodyPart, character, variant, searchQuery,
#                                                                 searchEnabled], *args, **kwargs)
#
#
# class ClothesSelectionWidget(_SelectionWidget):
#     def __init__(self, modelLine=None, bodyPart=None, category=None, variant=None, searchQuery='', searchEnabled=True,
#                  *args, **kwargs):
#         super(ClothesSelectionWidget, self).__init__(ClothesFilterWidget,
#                                                      [modelLine, bodyPart, category, variant, searchQuery,
#                                                       searchEnabled], *args, **kwargs)
#
#
# class CharacterOverrideTemplateSelectionWidget(_SelectionWidget):
#     def __init__(self, modelLine=None, bodyPart=None, searchQuery='', searchEnabled=True, *args, **kwargs):
#         super(CharacterOverrideTemplateSelectionWidget, self).__init__(CharacterOverrideTemplateFilterWidget,
#                                                                        [modelLine, bodyPart, searchQuery,
#                                                                         searchEnabled], *args, **kwargs)