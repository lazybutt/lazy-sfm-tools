from character_library.common.fields import ConfigFields
from character_library.ui.utils.memory_filter_mode import MemoryFilterMode


class CharacterOverridesMemoryFilter(object):
    def __init__(self, default_model_line=MemoryFilterMode.ANY, default_character=MemoryFilterMode.ANY,
                 default_body_part=MemoryFilterMode.ANY, default_variant=MemoryFilterMode.ANY):
        from character_library import Filters

        self._model_line_filter = Filters.model_line
        self._character_filter = Filters.character_part
        self._override_filter = Filters.character_override

        self._last_model_line = ConfigFields.ANY
        if default_model_line != MemoryFilterMode.ANY and len(self._model_line_filter.Names) > 0:
            self._last_model_line = self._model_line_filter.Names[default_model_line]

        self._last_character = ConfigFields.ANY
        if default_character != MemoryFilterMode.ANY and len(self._character_filter.Characters) > 0:
            self._last_character = sorted(self._character_filter.Characters)[default_character]

        self._last_body_part = ConfigFields.ANY
        if default_body_part != MemoryFilterMode.ANY and len(self._model_line_filter.Parts) > 0:
            self._last_body_part = self._model_line_filter.Parts[default_body_part]

        self._last_variant = ConfigFields.ANY
        if default_variant != MemoryFilterMode.ANY and len(self._character_filter.Variants) > 0:
            self._last_variant = self._character_filter.Variants[default_variant]

    def filtered_items(self, model_line=ConfigFields.ANY, character=ConfigFields.ANY,
                       body_part=ConfigFields.ANY, variant=ConfigFields.ANY, name=ConfigFields.ANY):
        return self._override_filter\
            .by_model_line(model_line)\
            .by_character(character)\
            .by_body_part(body_part)\
            .by_variant(variant)\
            .by_name(name, False)\
            .items

    @property
    def last_model_line(self):
        model_line_list = self.model_line_list
        if model_line_list:
            return self._value_in_list_or_any(self._last_model_line, model_line_list)

    @property
    def last_character(self):
        character_list = self.character_list
        if character_list:
            return self._value_in_list_or_any(self._last_character, character_list)

    @property
    def last_body_part(self):
        body_part_list = self.body_part_list
        if body_part_list:
            return self._value_in_list_or_any(self._last_body_part, body_part_list)

    @property
    def last_variant(self):
        variant_list = self.variants_list
        if variant_list:
            return self._value_in_list_or_any(self._last_variant, variant_list)

    @last_model_line.setter
    def last_model_line(self, model_line):
        self._last_model_line = model_line

    @last_character.setter
    def last_character(self, character):
        self._last_character = character

    @last_body_part.setter
    def last_body_part(self, body_part):
        self._last_body_part = body_part

    @last_variant.setter
    def last_variant(self, variant):
        self._last_variant = variant

    @property
    def model_line_list(self):
        model_line_list = self._model_line_filter.names
        return self._add_any_value(model_line_list)

    @property
    def character_list(self):
        character_list = self._character_filter.by_model_line(self._last_model_line).characters
        return self._add_any_value(character_list)

    @property
    def body_part_list(self):
        body_part_list = self._model_line_filter.by_name(self._last_model_line).parts
        return self._add_any_value(body_part_list)

    @property
    def variants_list(self):
        filtered = self._character_filter.by_model_line(self._last_model_line)
        if filtered.characters:
            if self._last_character in filtered.characters:
                filtered = filtered.by_character(self.last_character)
        return self._add_any_value(filtered.variants)

    @staticmethod
    def _add_any_value(item_list):
        return item_list if len(item_list) == 1 else item_list + [ConfigFields.ANY]

    @staticmethod
    def _value_in_list_or_any(item, item_list):
        return item if item in item_list else ConfigFields.ANY
