from PySide import QtGui
from PySide import QtCore


class CharacterPartFilterWidget(QtGui.QWidget):
    filterChanged = QtCore.Signal()

    def __init__(self, model_line=None, body_part=None, character=None, variant=None, search_query='', search_enabled=True,
                 *args, **kwargs):
        super(CharacterPartFilterWidget, self).__init__(*args, **kwargs)
        from lazy_sfm_tools.ui.combo_box import ComboBoxAutoCompletion
        from character_library.ui.utils.memory_filter_mode import MemoryFilterMode
        from character_library.ui.utils.character_part_memory_filter import CharacterPartMemoryFilter

        self._filter = CharacterPartMemoryFilter(MemoryFilterMode.FIRST, MemoryFilterMode.FIRST,
                                                 MemoryFilterMode.ANY, MemoryFilterMode.ANY)

        self._main_layout = QtGui.QGridLayout()
        self._model_line_selector = ComboBoxAutoCompletion()
        self._body_part_selector = ComboBoxAutoCompletion()
        self._character_selector = ComboBoxAutoCompletion()
        self._variant_selector = ComboBoxAutoCompletion()
        self._search_filter = QtGui.QLineEdit()
        self._setup_signals()
        self._fill_all()
        self._set_default_values(model_line, body_part, character, variant, search_query)
        self.set_enabled_search(search_enabled)
        self._main_layout.addWidget(QtGui.QLabel('ModelLine:'), 0, 0)
        self._main_layout.addWidget(QtGui.QLabel('BodyPart:'), 1, 0)
        self._main_layout.addWidget(QtGui.QLabel('Character:'), 2, 0)
        self._main_layout.addWidget(QtGui.QLabel('Variant:'), 3, 0)
        self._main_layout.addWidget(self._model_line_selector, 0, 1)
        self._main_layout.addWidget(self._body_part_selector, 1, 1)
        self._main_layout.addWidget(self._character_selector, 2, 1)
        self._main_layout.addWidget(self._variant_selector, 3, 1)
        self._main_layout.addWidget(self._search_filter, 4, 0, 1, 2)
        self._main_layout.setAlignment(QtCore.Qt.AlignCenter)
        self.setStyleSheet('font-size: 14px; font-weight: bold;')
        self.setLayout(self._main_layout)

    def _setup_signals(self):
        self._model_line_selector.currentIndexChanged.connect(self._model_line_changed)
        self._body_part_selector.currentIndexChanged.connect(self._body_part_changed)
        self._character_selector.currentIndexChanged.connect(self._character_changed)
        self._variant_selector.currentIndexChanged.connect(self._variant_changed)
        self._search_filter.textChanged.connect(self._searchFieldChanged)

    def update_filter(self):
        from character_library.ui.utils.memory_filter_mode import MemoryFilterMode
        from character_library.ui.utils.character_part_memory_filter import CharacterPartMemoryFilter

        self._filter = CharacterPartMemoryFilter(MemoryFilterMode.FIRST, MemoryFilterMode.FIRST,
                                                 MemoryFilterMode.ANY, MemoryFilterMode.ANY)
        self.filterChanged.emit()

    @property
    def filtered_items(self):
        return self._filter.filtered_items(model_line=self.model_line, body_part=self.body_part,
                                           character=self.character, variant=self.variant,
                                           name='*' if self.search_query == '' else self.search_query)

    # region Current Filter Values
    @property
    def model_line(self):
        return self._model_line_selector.currentText()

    @property
    def body_part(self):
        return self._body_part_selector.currentText()

    @property
    def character(self):
        return self._character_selector.currentText()

    @property
    def variant(self):
        return self._variant_selector.currentText()

    @property
    def search_query(self):
        return self._search_filter.text()

    # endregion

    # region Filter Widgets
    @property
    def model_line_selector(self):
        return self._model_line_selector

    @property
    def body_part_selector(self):
        return self._body_part_selector

    @property
    def character_selector(self):
        return self._character_selector

    @property
    def variant_selector(self):
        return self._variant_selector

    @property
    def search_field(self):
        return self._search_filter

    # endregion

    # region Set Enabled
    def set_enabled_search(self, enabled):
        self._search_filter.setEnabled(enabled)

    def set_enabled_model_line(self, enabled):
        self._model_line_selector.setEnabled(enabled)

    def set_enabled_body_part(self, enabled):
        self._body_part_selector.setEnabled(enabled)

    def set_enabled_character(self, enabled):
        self._character_selector.setEnabled(enabled)

    def set_enabled_variant(self, enabled):
        self._variant_selector.setEnabled(enabled)

    # endregion

    # region Set Visible
    def set_visible_search(self, visible):
        self._search_filter.setVisible(visible)

    def set_visible_model_line(self, visible):
        self._main_layout.itemAtPosition(0, 0).widget().setVisible(visible)
        self._model_line_selector.setVisible(visible)

    def set_visible_body_part(self, visible):
        self._main_layout.itemAtPosition(1, 0).widget().setVisible(visible)
        self._body_part_selector.setVisible(visible)

    def set_visible_character(self, visible):
        self._main_layout.itemAtPosition(2, 0).widget().setVisible(visible)
        self._character_selector.setVisible(visible)

    def set_visible_variant(self, visible):
        self._main_layout.itemAtPosition(3, 0).widget().setVisible(visible)
        self._variant_selector.setVisible(visible)

    # endregion

    # region Set Fixed
    def set_fixed_model_line(self, modelLine):
        if modelLine is None:
            self.set_visible_model_line(True)
            return
        if self.set_current_model_line(modelLine):
            self.set_visible_model_line(False)

    def set_fixed_body_part(self, bodyPart):
        if bodyPart is None:
            self.set_visible_body_part(True)
            return
        if self.set_current_body_part(bodyPart):
            self.set_visible_body_part(False)

    def set_fixed_character(self, character):
        if character is None:
            self.set_visible_character(True)
            return
        if self.set_current_character(character):
            self.set_visible_character(False)

    def set_fixed_variant(self, variant):
        if variant is None:
            self.set_visible_variant(True)
            return
        if self.set_current_variant(variant):
            self.set_visible_variant(False)

    # endregion

    # region Set Current
    def _set_default_values(self, model_line, body_part, character, variant, search_query):
        self.set_current_model_line(model_line)
        self.set_current_body_part(body_part)
        self.set_current_character(character)
        self.set_current_variant(variant)
        self.set_current_search_query(search_query)

    def set_current_search_query(self, query):
        self._search_filter.setText(query)
        return True

    def set_current_model_line(self, modelLine):
        index = self._model_line_selector.findText(modelLine)
        if index != -1:
            self._model_line_selector.setCurrentIndex(index)
            return True
        return False

    def set_current_body_part(self, bodyPart):
        index = self._body_part_selector.findText(bodyPart)
        if index != -1:
            self._body_part_selector.setCurrentIndex(index)
            return True
        return False

    def set_current_character(self, character):
        index = self._character_selector.findText(character)
        if index != -1:
            self._character_selector.setCurrentIndex(index)
            return True
        return False

    def set_current_variant(self, variant):
        index = self._variant_selector.findText(variant)
        if index != -1:
            self._variant_selector.setCurrentIndex(index)
            return True
        return False

    # endregion

    # region Fill Items
    def _model_line_changed(self, index):
        self._filter.last_model_line = self.model_line
        self._fill_body_part()
        self._fill_character()
        self._fill_variant()
        self.filterChanged.emit()

    def _body_part_changed(self, index):
        self._filter.last_body_part = self.body_part
        self.filterChanged.emit()

    def _character_changed(self, index):
        self._filter.last_character = self.character
        self._fill_variant()
        self.filterChanged.emit()

    def _variant_changed(self, index):
        self._filter.last_variant = self.variant
        self.filterChanged.emit()

    def _searchFieldChanged(self, index):
        self.filterChanged.emit()

    def _fill_all(self):
        self._fill_model_lines()
        self._fill_body_part()
        self._fill_character()
        self._fill_variant()

    def _fill_model_lines(self):
        self.model_line_selector.clear()
        self.model_line_selector.addItems(self._filter.model_line_list)
        self.set_current_model_line(self._filter.last_model_line)

    def _fill_body_part(self):
        self.body_part_selector.disconnect(QtCore.SIGNAL('currentIndexChanged(int)'), self._body_part_changed)
        self.body_part_selector.clear()
        self.body_part_selector.addItems(self._filter.body_part_list)
        self.set_current_body_part(self._filter.last_body_part)
        self.body_part_selector.currentIndexChanged.connect(self._body_part_changed)

    def _fill_character(self):
        self.character_selector.disconnect(QtCore.SIGNAL('currentIndexChanged(int)'), self._character_changed)
        self.character_selector.clear()
        self.character_selector.addItems(self._filter.character_list)
        self.set_current_character(self._filter.last_character)
        self.character_selector.currentIndexChanged.connect(self._character_changed)

    def _fill_variant(self):
        self.variant_selector.disconnect(QtCore.SIGNAL('currentIndexChanged(int)'), self._variant_changed)
        self.variant_selector.clear()
        self.variant_selector.addItems(self._filter.variants_list)
        self.set_current_variant(self._filter.last_variant)
        self.variant_selector.currentIndexChanged.connect(self._variant_changed)
    # endregion
