from PySide import QtGui
from PySide import QtCore

class CharacterFilterWidget(QtGui.QWidget):
    filter_changed = QtCore.Signal()

    def __init__(self, model_line=None, search_query='', search_enabled=True, *args, **kwargs):
        super(CharacterFilterWidget, self).__init__(*args, **kwargs)
        from character_library import Filters
        from lazy_sfm_tools.ui.combo_box import ComboBoxAutoCompletion

        self._last_selected_model_line = None

        self._main_layout = QtGui.QGridLayout()
        self._filter = Filters.character_part
        self._model_line_selector = ComboBoxAutoCompletion()
        self._bodyPartSelector = ComboBoxAutoCompletion()
        self._search_filter = QtGui.QLineEdit()
        self._model_line_selector.addItems(self._filter.model_lines)
        self.search_field.setText(search_query)
        self._search_filter.textChanged.connect(lambda: self.filter_changed.emit())
        self.set_current_model_line(model_line)
        self.set_enabled_search(search_enabled)
        self._main_layout.addWidget(QtGui.QLabel('ModelLine:'), 0, 0)
        self._main_layout.addWidget(self._model_line_selector, 0, 1)
        self._main_layout.addWidget(self._search_filter, 1, 0, 1, 2)
        self._main_layout.setAlignment(QtCore.Qt.AlignCenter)
        self.setStyleSheet('font-size: 14px; font-weight: bold;')
        self.setLayout(self._main_layout)

    def update_filter(self):
        from character_library import Filters
        self._filter = Filters.character_part

    @property
    def filtered_items(self):
        modelLine = self._model_line_selector.currentText()
        search_query = self._search_filter.text()
        filter = self._filter.by_model_line(modelLine)
        characters = []
        items = []
        for character_item in filter.by_character(search_query, False).items:
            if character_item.character != '*' and (character_item.variant == 'Default' or character_item.variant == '*') and character_item.character not in characters:
                characters.append(character_item.character)
                items.append([character_item.character, '*'])
        for character_item in filter.by_variant(search_query, False).items:
            if character_item.character != '*' and character_item.variant != '*' and character_item.variant != 'Default':
                if [character_item.character, character_item.variant] not in items:
                    items.append([character_item.character, character_item.variant])
        return items

    # region Current Filter Values
    @property
    def model_line(self):
        return self._model_line_selector.currentText()

    @property
    def search_query(self):
        return self._search_filter.text()
    # endregion

    # region Filter Widgets
    @property
    def model_line_selector(self):
        return self._model_line_selector

    @property
    def search_field(self):
        return self._search_filter
    # endregion

    # region Set Enabled
    def set_enabled_search(self, enabled):
        self._search_filter.setEnabled(enabled)

    def set_enabled_model_line(self, enabled):
        self._model_line_selector.setEnabled(enabled)
    # endregion

    # region Set Visible
    def set_visible_search(self, visible):
        self._search_filter.setVisible(visible)

    def set_visible_model_line(self, visible):
        self._main_layout.itemAtPosition(0, 0).widget().setVisible(visible)
        self._model_line_selector.setVisible(visible)
    # endregion

    # region Set Fixed
    def set_fixed_model_line(self, model_line):
        if model_line is None:
            self.set_visible_model_line(True)
            return
        if self.set_current_model_line(model_line):
            self.set_visible_model_line(False)
    # endregion

    # region Set Current
    def set_current_search_query(self, search_query):
        self._search_filter.setText(search_query)
        return True

    def set_current_model_line(self, model_line):
        index = self._model_line_selector.findText(model_line)
        if index != -1:
            self._model_line_selector.setCurrentIndex(index)
            return True
        return False
    # endregion

    # region Fill Items
    def _model_line_changed(self, index):
        self._last_selected_model_line = self.model_line
        self._fillBodyPart()
        self.filter_changed.emit()
    # endregion
