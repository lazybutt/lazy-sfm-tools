import sfm

class BaseLogger(object):
    def __init__(self, log_path=None, in_console=True):
        self._in_console = in_console
        self._log_path = open(log_path, "w") if log_path else None

    def get_log_file(self):
        return self._log_path

    @property
    def has_log_path(self):
        return self._log_path is not None

    @property
    def console_logging_enabled(self):
        return self._in_console is not None

    def message(self, text, in_class=None):
        message = "MESSAGE: {}".format(text)
        self._send_message(message, in_class)

    def warning(self, text, in_class=None):
        message = "WARNING: {}".format(text)
        self._send_message(message, in_class)

    def error(self, text, in_class=None):
        message = "ERROR: {}".format(text)
        self._send_message(message, in_class)

    def message_list(self, text, ls, in_class=None):
        message = "MESSAGE: {}\n{}".format(text, "\n".join(ls))
        self._send_message(message, in_class)

    def warning_list(self, text, ls, in_class=None):
        message = "WARNING: {}\n{}".format(text, "\n".join(ls))
        self._send_message(message, in_class)

    def error_list(self, text, ls, in_class=None):
        message = "ERROR: {}\n{}".format(text, "\n".join(ls))
        self._send_message(message, in_class)

    def _send_message(self, message, in_class=None):
        if in_class:
            message += " in {} class".format(in_class.__name__)
        message += "\n"
        if self.console_logging_enabled:
            sfm.Msg(message)
        if self.has_log_path:
            self.get_log_file().write(message)
