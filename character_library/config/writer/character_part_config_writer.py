from character_library.common.config_writer import ConfigWriter


class CharacterPartConfigWriter(ConfigWriter):
    @classmethod
    def dump(cls, config):
        from character_library.config.fields import CharacterPartFields as fields
        return {
            fields.NAME: config.name,
            fields.MODEL_LINE: config.model_line,
            fields.BODY_PART: config.body_part,
            fields.CHARACTER: config.character,
            fields.VARIANT: config.variant,
            fields.PATH: "virtual" if config.is_virtual else config.config_path
        }
