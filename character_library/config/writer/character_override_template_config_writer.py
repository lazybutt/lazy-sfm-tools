from character_library.common.config_writer import ConfigWriter


class CharacterOverrideTemplateConfigWriter(ConfigWriter):
    @classmethod
    def dump(cls, config):
        from character_library.config.fields import CharacterOverrideTemplateFields as fields
        return {
            fields.NAME: config.name,
            fields.MODEL_LINE: config.model_line,
            fields.BODY_PART: config.body_part,
            fields.VALUES: cls._get_values(config),
            fields.PATH: "virtual" if config.is_virtual else config.config_path
        }

    @classmethod
    def _get_values(cls, config):
        from lazy_sfm_tools.materials.overrides import OverrideMaterialElement
        from character_library.config.fields import CharacterOverrideTemplateFields as fields

        overrides = []
        for value in config.values:  # type: OverrideMaterialElement
            overrides.append({
                fields.FOR_MATERIALS: value.for_materials,
                fields.ATTRIBUTE_NAME: value.attr_name,
                fields.ATTRIBUTE_VALUE: value.attr_value
            })
        return overrides
