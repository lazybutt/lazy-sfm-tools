from character_library.common.config_writer import ConfigWriter


class ModelLineConfigWriter(ConfigWriter):
    @classmethod
    def dump(cls, config):
        from character_library.config.fields import ModelLineFields as fields
        return {
            fields.NAME: config.name,
            fields.PARTS: config.parts,
            fields.CONSTRAINS: ["{}>{}".format(constrain.slave, constrain.master) for constrain in config.constrains],
            fields.PATH: "virtual" if config.is_virtual else config.config_path
        }
