from character_library.common.config_writer import ConfigWriter
from character_library.config import CharacterOverrideConfig


class CharacterOverrideConfigWriter(ConfigWriter):
    @classmethod
    def dump(cls, config):
        # type: (CharacterOverrideConfig) -> dict
        from character_library.config.fields import CharacterOverrideFields as fields
        return {
            fields.NAME: config.name,
            fields.MODEL_LINE: config.model_line,
            fields.BODY_PART: config.body_part,
            fields.CHARACTER: config.character,
            fields.VARIANT: config.variant,
            fields.TEMPLATE: cls._get_template_name(config),
            fields.TEMPLATE_VALUES: config.template_values,
            fields.OVERRIDE_VALUES: cls._get_override_values(config),
            fields.PATH: "virtual" if config.is_virtual else config.config_path
        }

    @classmethod
    def _get_template_name(cls, config):
        return config.template.name if config.has_template else None,

    @classmethod
    def _get_override_values(cls, config):
        from character_library.config.fields import CharacterOverrideFields as fields
        overrides = []
        for override in config.overrides:
            overrides.append({
                fields.FOR_MATERIALS: override.for_materials,
                fields.ATTRIBUTE_NAME: override.attribute_name,
                fields.ATTRIBUTE_VALUE: override.attribute_value
            })
        return overrides
