from character_library.common import StandaloneConfig as _StandaloneConfig
from character_library.config.fields import CharacterOverrideFields as _Fields

class CharacterOverrideConfig(_StandaloneConfig):
    def __init__(self, name, model_line, body_part, character, template_name=None, template_values=None, override_values=None, variant="Default", path=None):
        # type: (str, str, str, str, str, dict, list, str, str) -> None
        super(CharacterOverrideConfig, self).__init__(name, path)
        self._model_line = model_line
        self._body_part = body_part
        self._character = character

        self._template = None
        if template_name is not None:
            self._bind_template(template_name)

        self._template_values = template_values
        self._override_values = override_values
        self._variant = variant

    @property
    def model_line(self):
        return self._model_line

    @property
    def body_part(self):
        return self._body_part

    @property
    def character(self):
        return self._character

    @property
    def variant(self):
        return self._variant

    @property
    def has_template(self):
        return self._template is not None

    @property
    def template(self):
        return self._template

    @property
    def template_values(self):
        return self._template_values

    @property
    def overrides(self):
        return self._override_values
    
    def build_raw_override(self):
        from lazy_sfm_tools.materials.overrides import OverrideMaterialElement
        raw = []
        if self.has_template:
            for value in self.template.values:
                attribute_value = value.attr_value
                if self._is_template_key(value):
                    template_key = self._template_value_key(value)
                    template_default = self._template_value_default(value)
                    if template_key in self.template_values.keys():
                        attribute_value = self.template_values[template_key]
                    elif template_default is not None:
                        attribute_value = template_default
                    else:
                        continue
                if attribute_value is not None:
                    raw.append(OverrideMaterialElement(value.for_materials, value.attr_name, attribute_value))
        raw.extend(self._override_values)
        return raw

    @staticmethod
    def _is_template_key(value):
        return value.attr_value.startswith("$") and value.attr_value.endswith("$")

    @staticmethod
    def _template_value_key(value):
        raw = value.attr_value.strip("$")
        return raw.split(":")[0] if ":" in raw else raw

    @staticmethod
    def _template_value_default(value):
        raw = value.attr_value.strip("$")
        return raw.split(":")[1] if ":" in raw else None

    def _bind_template(self, template_name):
        from character_library.exceptions import OverrideTemplateNotFound
        from character_library.exceptions import UncertaintyOverrideTemplateBinding
        from character_library import Filters

        filter = Filters.character_override_template.by_model_line(self._model_line).by_name(template_name)
        items_count = len(filter.items)
        if items_count == 0:
            raise OverrideTemplateNotFound(self, template_name)
        elif len(filter.items) > 1:
            raise UncertaintyOverrideTemplateBinding(self, filter.items)
        self._template = filter.items[0]
