from character_library.common.config_reader import ConfigReader


class CharacterOverrideTemplateConfigReader(ConfigReader):

    @classmethod
    def read(cls, path_or_dict, check_needed=True):
        data = path_or_dict if isinstance(path_or_dict, dict) else cls._get_data(path_or_dict)
        from character_library.config.fields import CharacterOverrideTemplateFields as fields
        from lazy_sfm_tools.utils.kwargs_tools import KwargsReader
        from character_library.config import CharacterOverrideTemplateConfig

        kw_reader = KwargsReader(data)

        errors_list = []

        path = kw_reader.get_or_default("path", None)
        name = kw_reader.get_or_default(fields.NAME, None)
        model_line = kw_reader.get_or_default(fields.MODEL_LINE, fields.ANY)
        body_part = kw_reader.get_or_default(fields.BODY_PART, fields.ANY)
        raw_values = kw_reader.get_or_default(fields.VALUES, fields.ANY)
        values = cls._wrap_override_values(raw_values, errors_list)

        if not errors_list:
            return CharacterOverrideTemplateConfig(name, model_line, body_part, values, path)

        cls.log_errors("Character Override Template", path, errors_list)

    @classmethod
    def _wrap_override_values(cls, raw_values, errors_list):
        from character_library.config.fields import CharacterOverrideFields as fields
        from lazy_sfm_tools.materials.overrides import OverrideMaterialElement
        from lazy_sfm_tools.utils.kwargs_tools import KwargsReader

        values = []
        for item in raw_values:
            kw_reader = KwargsReader(item)
            for_materials = kw_reader.get_or_default(fields.FOR_MATERIALS, fields.ANY)
            attribute_name = kw_reader.get_or_default(fields.ATTRIBUTE_NAME, None)
            attribute_value = kw_reader.get_or_default(fields.ATTRIBUTE_VALUE, None)
            if attribute_name is None or attribute_value is None or \
                    attribute_name == "" or attribute_value == "" or \
                    attribute_name.isspace() or attribute_value.isspace():
                errors_list.append(' - "{}" and "{}" can not be null or empty for materials "{}" in field "{}"'.format(
                    fields.ATTRIBUTE_NAME, fields.ATTRIBUTE_VALUE, ", ".join(for_materials), fields.OVERRIDE_VALUES
                ))
            override_attribute = OverrideMaterialElement(for_materials, attribute_name, attribute_value)
            values.append(override_attribute)
        return values
