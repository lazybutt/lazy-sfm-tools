from character_library.common.config_reader import ConfigReader


class CharacterOverrideConfigReader(ConfigReader):
    _template_buff = {}

    @classmethod
    def read(cls, path_or_dict, check_needed=True):
        data = path_or_dict if isinstance(path_or_dict, dict) else cls._get_data(path_or_dict)
        from character_library.config.fields import CharacterOverrideFields as fields
        from lazy_sfm_tools.utils.kwargs_tools import KwargsReader
        from character_library.config import CharacterOverrideConfig

        errors_list = []

        kw_reader = KwargsReader(data)

        path = kw_reader.get_or_default("path", None)
        name = kw_reader.get_or_default(fields.NAME, None)
        model_line = kw_reader.get_or_default(fields.MODEL_LINE, fields.ANY)
        body_part = kw_reader.get_or_default(fields.BODY_PART, fields.ANY)
        character = kw_reader.get_or_default(fields.CHARACTER, fields.ANY)
        variant = kw_reader.get_or_default(fields.VARIANT, fields.ANY)
        template_name = kw_reader.get_or_default(fields.TEMPLATE, None)
        if isinstance(template_name, list) and template_name:
            template_name = template_name[0]
        template_values = kw_reader.get_or_default(fields.TEMPLATE_VALUES, None)
        raw_override_values = kw_reader.get_or_default(fields.OVERRIDE_VALUES, None)
        override_values = cls._wrap_override_values(raw_override_values, errors_list)

        if check_needed:
            if template_name is not None:
                cls._template_exists(model_line, body_part, template_name, errors_list)
            if not template_values and not raw_override_values:
                errors_list.append(' - fields "{}", "{}" or "{}" must be filled'.format(
                    fields.TEMPLATE, fields.TEMPLATE_VALUES, fields.OVERRIDE_VALUES
                ))
            if (template_values is not None and template_values != []) and \
                    (template_name is None or template_name == "" or template_name.isspace()):
                errors_list.append(' - "{}" can not be null or empty if "{}" not empty'.format(
                    fields.TEMPLATE, fields.TEMPLATE_VALUES
                ))

        if not errors_list or not check_needed:
            return CharacterOverrideConfig(name, model_line, body_part, character, template_name,
                                           template_values, override_values, variant, path)

        cls.log_errors("Character Overrides", path, errors_list)

    @classmethod
    def _wrap_override_values(cls, raw_override_values, errors_list):
        from character_library.config.fields import CharacterOverrideFields as fields
        from lazy_sfm_tools.utils.kwargs_tools import KwargsReader
        from lazy_sfm_tools.materials.overrides import OverrideMaterialElement

        values = []
        for item in raw_override_values:
            kw_reader = KwargsReader(item)
            for_materials = kw_reader.get_or_default(fields.FOR_MATERIALS, fields.ANY)
            attribute_name = kw_reader.get_or_default(fields.ATTRIBUTE_NAME, None)
            attribute_value = kw_reader.get_or_default(fields.ATTRIBUTE_VALUE, None)
            if attribute_name is None or attribute_value is None or \
                    attribute_name == "" or attribute_value == "" or \
                    attribute_name.isspace() or attribute_value.isspace():
                errors_list.append(' - "{}" and "{}" can not be null or empty for materials "{}" in field "{}"'.format(
                    fields.ATTRIBUTE_NAME, fields.ATTRIBUTE_VALUE, ", ".join(for_materials), fields.OVERRIDE_VALUES
                ))
            override_attribute = OverrideMaterialElement(for_materials, attribute_name, attribute_value)
            values.append(override_attribute)
        return values

    @classmethod
    def _template_exists(cls, model_line, body_part, template_name, errors_list):
        if template_name in cls._template_buff.keys():
            found_model_line, found_body_part = cls._template_buff[template_name]
            if model_line.lower() == found_model_line.lower() and body_part.lower() == found_body_part.lower():
                return True
        from character_library import Filters
        template_names = Filters.character_override_template\
            .by_model_line(model_line).by_body_part(body_part).by_name(template_name).names
        if len(template_names) == 1:
            cls._template_buff[template_names[0]] = tuple([model_line, body_part])
            return True
        elif len(template_names) > 1:
            errors_list.append(' - there are several templates with name "{}", '
                               'it is impossible to choose which one to use'.format(template_name))
        else:
            errors_list.append(' - there are not templates with name "{}"'.format(template_name))
