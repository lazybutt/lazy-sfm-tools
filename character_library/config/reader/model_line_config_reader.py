from character_library.common.config_reader import ConfigReader


class ModelLineConfigReader(ConfigReader):

    @classmethod
    def read(cls, path_or_dict, check_needed=True):
        data = path_or_dict if isinstance(path_or_dict, dict) else cls._get_data(path_or_dict)
        from character_library.config.fields import ModelLineFields as fields
        from lazy_sfm_tools.utils.kwargs_tools import KwargsReader
        from character_library.config import ModelLineConfig

        kw_reader = KwargsReader(data)

        errors_list = []

        path = kw_reader.get_or_default(fields.PATH, None)
        name = kw_reader.get_or_default(fields.NAME, None)
        parts = kw_reader.get_or_default(fields.PARTS, [])
        raw_constrains = kw_reader.get_or_default(fields.CONSTRAINS, [])
        constrains = cls._wrap_constrains(raw_constrains)

        if check_needed:
            if name is None or name == "" or name.isspace():
                errors_list.append(' - field "{}" can not be null or empty'.format(fields.NAME))
                return None
            cls._check_constrains(
                parts,
                {constrain.split('>')[0]: constrain.split('>')[1] for constrain in raw_constrains},
                errors_list
            )

        if not errors_list or not check_needed:
            return ModelLineConfig(name, parts, constrains, path)

        cls.log_errors("Model Line", path, errors_list)

    @classmethod
    def _wrap_constrains(cls, raw_constrains):
        from character_library.config import ModelLineConfig
        constrains = []
        for raw_constrain in raw_constrains:
            constrains.append(ModelLineConfig.ModelConstrain.from_string(raw_constrain))
        return constrains

    @classmethod
    def _check_constrains(cls, parts, constrains, errors_list):
        from character_library.config.fields import ModelLineFields as fields

        for master, slave in constrains.items():
            if slave in constrains.keys() and constrains[slave] == master:
                errors_list.append(' - recursive constrain of parts "{}" and "{}" is not allowed'.format(master, slave))

            if master not in parts:
                errors_list.append(' - field "{}" must contain items from "{}" field ({})'.format(
                    fields.CONSTRAINS, fields.PARTS, master
                ))
            elif slave not in parts:
                errors_list.append(' - field "{}" must contain items from "{}" field ({})'.format(
                    fields.CONSTRAINS, fields.PARTS, slave
                ))
