import os.path

from character_library.common.config_reader import ConfigReader


class CharacterPartConfigReader(ConfigReader):

    @classmethod
    def read_from_animset(cls, animset):
        from lazy_sfm_tools.files.file_structure import GamePath
        if animset.HasAttribute("gameModel"):
            path = animset.gameModel.modelName.GetValue().replace(".mdl", ".clcp")
            game_path = GamePath.model(path)
            if game_path.is_valid:
                return cls.read(game_path.absolute_path)

    @classmethod
    def read(cls, path_or_dict, check_needed=True):
        data = path_or_dict if isinstance(path_or_dict, dict) else cls._get_data(path_or_dict)
        from character_library.config.fields import CharacterPartFields as fields
        from lazy_sfm_tools.utils.kwargs_tools import KwargsReader
        from character_library.config import CharacterPartConfig

        kw_reader = KwargsReader(data)

        path = kw_reader.get_or_default(fields.PATH, None)
        name = kw_reader.get_or_default(fields.NAME, None)
        model_line = kw_reader.get_or_default(fields.MODEL_LINE, fields.ANY)
        body_part = kw_reader.get_or_default(fields.BODY_PART, fields.ANY)
        character = kw_reader.get_or_default(fields.CHARACTER, fields.ANY)
        variant = kw_reader.get_or_default(fields.VARIANT, fields.ANY)

        errors_list = []

        if check_needed:
            if name is None or name == "" or name.isspace():
                errors_list.append(' - field "{}" can not be null or empty'.format(fields.NAME))
                return None
            if not os.path.exists(path.replace(".clcp", ".mdl")):
                errors_list.append(' - mdl file not found near config')
        if not errors_list:
            return CharacterPartConfig(name, body_part, character, model_line, variant, path)

        cls.log_errors("Character Part", path, errors_list)
