from character_library.common.standalone_config import StandaloneConfig as _StandaloneConfig
from character_library.config.fields import ModelLineFields as _Fields

class ModelLineConfig(_StandaloneConfig):

    class ModelConstrain(object):

        def __init__(self, from_model, to_model):
            self.slave = from_model
            self.master = to_model

        @staticmethod
        def from_string(constrain_string):
            return ModelLineConfig.ModelConstrain(*constrain_string.split(">"))

        def __str__(self):
            return self.slave + ">" + self.master

    def __init__(self, name, parts, constrains, path=None):
        # type: (str, list, list, str) -> None
        super(ModelLineConfig, self).__init__(name, path)
        self._parts = parts
        self._constrains = constrains

    @property
    def parts(self):
        return self._parts

    @property
    def constrains(self):
        return self._constrains

    @property
    def dump(self):
        return {
            _Fields.NAME: self._name,
            _Fields.PARTS: self._parts,
            _Fields.CONSTRAINS: [str(constrain) for constrain in self._constrains],
        }