from character_library.common.standalone_config import StandaloneConfig as _StandaloneConfig
from character_library.config.fields import CharacterOverrideTemplateFields as _Fields

class CharacterOverrideTemplateConfig(_StandaloneConfig):
    def __init__(self, name, model_line, body_part, values, path=None):
        # type: (str, str, str, list, str) -> None
        super(CharacterOverrideTemplateConfig, self).__init__(name, path)
        self._model_line = model_line
        self._body_part = body_part
        self._values = values

    @property
    def model_line(self):
        return self._model_line

    @property
    def body_part(self):
        return self._body_part

    @property
    def values(self):
        return self._values

class TemplateValue(object):
    def __init__(self, for_materials, attribute_name, attribute_value):
        self.for_materials = for_materials
        self.attribute_name = attribute_name
        self.attribute_value = attribute_value

    @property
    def is_template_key(self):
        return self.attribute_value.startswith('$') and self.attribute_value.endswith('$')

    @property
    def template_key(self):
        value = self.attribute_value.replace('$')
        return value.split(':')[0] if ':' in value else value

    @property
    def template_default(self):
        value = self.attribute_value.replace('$')
        return value.split(':')[1] if ':' in value else None
