from character_library.common.fields import ConfigFields as _ConfigFields

class CharacterOverrideTemplateFields(_ConfigFields):
    MODEL_LINE = 'model_line'
    BODY_PART = 'body_part'
    VALUES = 'values'

    FOR_MATERIALS = 'for_materials'
    ATTRIBUTE_NAME = 'attribute_name'
    ATTRIBUTE_VALUE = 'attribute_value'
