from character_library.common.fields import ConfigFields as _ConfigFields

class CharacterOverrideFields(_ConfigFields):
    MODEL_LINE = 'model_line'
    CHARACTER = 'character'
    BODY_PART = 'body_part'
    TEMPLATE = 'template_name'
    TEMPLATE_VALUES = 'template_values'
    OVERRIDE_VALUES = 'override_values'
    VARIANT = 'variant'

    FOR_MATERIALS = 'for_materials'
    ATTRIBUTE_NAME = 'attribute_name'
    ATTRIBUTE_VALUE = 'attribute_value'
