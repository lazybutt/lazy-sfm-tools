from character_library.common.fields import ConfigFields as _ConfigFields

class ModelLineFields(_ConfigFields):
    PARTS = "parts"
    CONSTRAINS = "constrains"
