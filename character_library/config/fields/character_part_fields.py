from character_library.common.fields import ConfigFields as _ConfigFields

class CharacterPartFields(_ConfigFields):
    BODY_PART = 'body_part'
    CHARACTER = 'character'
    MODEL_LINE = 'model_line'
    VARIANT = 'variant'
