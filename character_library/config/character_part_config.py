from character_library.common.pairbased_config import PairBasedConfig as _PairBasedConfig
from character_library.config.fields import CharacterPartFields as _Fields

class CharacterPartConfig(_PairBasedConfig):
    def __init__(self, name, body_part, character, model_line, variant, path=None):
        super(CharacterPartConfig, self).__init__(name, path)
        self._body_part = body_part
        self._character = character
        self._model_line = model_line
        self._variant = variant

    @property
    def body_part(self):
        return self._body_part

    @property
    def model_line(self):
        return self._model_line

    @property
    def character(self):
        return self._character

    @property
    def variant(self):
        return self._variant

    @property
    def dump(self):
        return {
            _Fields.NAME: self._name,
            _Fields.MODEL_LINE: self._model_line,
            _Fields.BODY_PART: self._body_part,
            _Fields.CHARACTER: self._character,
            _Fields.VARIANT: self._variant,
        }
