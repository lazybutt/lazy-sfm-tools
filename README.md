# Installation.

Unpack folders **lazy_sfm_tools** and **character_library** in **\<SFM_FOLDER\>\game\sdktools\python\global\lib\site-packages**.

# Important info.
The library is in an early stage of development and is subject to change, be prepared for the fact that the library may change slightly and you may need to maintain your scripts.
