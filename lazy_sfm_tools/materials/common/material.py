import vsUtils
from vs.movieobjects import CDmeGameModel
from vs.movieobjects import CDmeMaterial
from lazy_sfm_tools.files.file_structure import GamePath
from lazy_sfm_tools.object_wrapper import AttributeWrapper


class Material(object):
    """Material creator/wrapper."""
    def __init__(self, **kw):
        """Native material creator/wrapper.

        Keyword Args:
            gm (CDmeGameModel): parent gamemodel.
            material (CDmeMaterial): native material object.
            name (str|unicode): name of material.
            path (str|unicode|GamePath): content related path to material.
        """
        self._parent = kw.pop("gm") \
            if "gm" in kw and isinstance(kw["gm"], CDmeGameModel) \
            else None
        self._material = kw.pop("material") \
            if "material" in kw and isinstance(kw["material"], CDmeMaterial) \
            else None
        if self._material is None:
            if "name" in kw and "path" in kw:
                KeyError("Name and path is required for creating new material object")
            self._create_material(
                str(kw.pop("name")),
                kw.pop("path")
            )

    @classmethod
    def from_native(cls, native_material):
        """Custom constructor to wrap existing material.

        Args:
            native_material (CDmeMaterial): native material object.
        """
        return cls(material=native_material)

    @classmethod
    def create_new(cls, name, path, parent=None):
        """Custom constructor for creating new material

        Args:
            name (str|unicode): name of material.
            path (str|unicode|GamePath): content related path to material.
            parent (CDmeGameModel|None): parent gamemodel.
        """
        return cls(name=name, path=path, gm=parent)

    def _create_material(self, name, path):
        from lazy_sfm_tools.utils.elements import create_element
        if isinstance(path, GamePath):
            path = path.typed_content_relative_path
        elif isinstance(path, unicode):
            path = str(path)
        self._material = create_element("DmeMaterial", name, self._parent)
        self._material.AddAttributeAsString("mtlName").SetValue(path)

    @property
    def native(self):
        """Native material object.

        Returns:
            CDmeMaterial: native material object.
        """
        return self._material

    @property
    def name(self):
        """Name of material.

        Returns:
            str: name of material.
        """
        return self._material.GetName()

    @property
    def path(self):
        """Content related path of material.

        Returns:
            str: content related path of material.
        """
        return self._material.mtlName.GetValue()

    @property
    def game_path(self):
        """GamePath of material.

        Returns:
            GamePath: GamePath of material.
        """
        path = self.path if ".vmt" in self.path.lower() else self.path + ".vmt"
        return GamePath.material_or_texture(path)

    @property
    def override_names(self):
        """Attribute names that have been overridden.

        Returns:
            list[str]: list of overridden attribute names.
        """
        return vsUtils.getDynamicAttributeNames(self._material)

    @property
    def override_attrs(self):
        """Attributes that have been overridden.

        Returns:
            list[AttributeWrapper]: list of overridden attributes (wrapped by SimpleAttribute wrapper).
        """
        return [AttributeWrapper(self._material.GetAttribute(name)) for name in self.override_names]

    @property
    def override_names_to_attrs(self):
        """Dict of material names to attributes that have been overridden.

        Returns:
            dict[str, AttributeWrapper]: dict of overridden attribute names
                to attributes (wrapped by SimpleAttribute wrapper).
        """
        return {override_attr.name: override_attr for override_attr in self.override_attrs}
