import sfm
from vs.studio import studiohdr_t
from vs.movieobjects import CDmeGameModel
from lazy_sfm_tools.files.file_structure import GamePath
from filesystem.valve import KeyValueFile


class MaterialsData(object):
    """Functionality for getting information about gamemodel's materials."""
    def __init__(self, **kw):
        import vs
        if "gm" in kw and isinstance(kw["gm"], vs.movieobjects.CDmeGameModel):
            self._hdr_studio = kw.pop("gm").GetStudioHdr()
        elif "native" in kw and isinstance(kw["native"], vs.studio.studiohdr_t):
            self._hdr_studio = kw["native"]
        else:
            raise NotImplementedError()

    @classmethod
    def from_gamemodel(cls, gamemodel):
        """Additional constructor for gamemodel object.

        Args:
            gamemodel (CDmeGameModel): gamemodel whose material information needs to be read.

        Returns:
            MaterialsData: material info receiver
        """
        return cls(gm=gamemodel)

    @classmethod
    def from_studio_hdr(cls, studio_hdr):
        """Additional constructor for wrapping studio hdr in materials info context.

        Args:
            studio_hdr (studiohdr_t): studio_hdr to wrap in materials info context.

        Returns:
            MaterialsData: materials info receiver
        """
        return cls(native=studio_hdr)

    @property
    def material_names(self):
        """Material names contained in gamemodel.

        Returns:
            list[str]: material names contained in gamemodel.
        """
        materialNames = []
        for i in range(self._hdr_studio.numtextures):
            materialName = self._hdr_studio.pTexture(i).pszName().lower()
            if materialName not in materialNames:
                materialNames.append(materialName)
        return materialNames

    @property
    def material_search_paths(self):
        """Paths for finding materials.

        Returns:
            list[str]: paths for finding materials.
        """
        return [
            self._hdr_studio.pCdtexture(idx).lower()
            for idx in range(self._hdr_studio.numcdtextures)
        ]

    @property
    def mat_names_to_full_paths(self):
        """Dict of material names to their full paths.

        Returns:
            dict[str, str]: Dict of material names to their full paths.
        """
        return {
            mat_name: game_path.absolute_path
            for mat_name, game_path in self.mat_names_to_game_paths.items()
        }

    @property
    def mat_names_to_mod_related_paths(self):
        """Dict of material names to their mod related paths.

        Returns:
            dict[str, str]: Dict of material names to their full paths.
        """
        return {
            mat_name: game_path.mod_relative_path
            for mat_name, game_path in self.mat_names_to_game_paths.items()
        }

    @property
    def mat_names_to_game_paths(self):
        """Dict of material names to their GamePaths.

        Returns:
            dict[str, GamePath]: Dict of material names to their full paths.
        """
        searchPaths = self.material_search_paths
        material_names_and_paths = {}
        for material_name in self.material_names:
            if material_name in material_names_and_paths:
                continue
            for search_path in searchPaths:
                path = self._build_mat_path(search_path, material_name)
                game_path = GamePath.material_or_texture(path)
                if game_path.is_valid:
                    material_names_and_paths[material_name] = game_path
                    break
            if material_name not in material_names_and_paths:
                sfm.Msg('Material "{}" not found is search paths\n'.format(material_name))
        return material_names_and_paths

    @property
    def mat_names_to_kv_files(self):
        """Dict of material names to their KeyValue files.

        Returns:
            dict[str, KeyValueFile]: Dict of material names to their full paths.
        """
        mat_names_to_paths = self.mat_names_to_full_paths
        return {
            mat_name: KeyValueFile(path)
            for mat_name, path in mat_names_to_paths.items()
        }

    @staticmethod
    def _build_mat_path(search_path, material_name):
        import os
        path = os.path.join(search_path, material_name)
        return path if path.endswith('.vmt') else path + '.vmt'

