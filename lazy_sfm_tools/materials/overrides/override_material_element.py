from lazy_sfm_tools.commons.enums import AttributeTypes
from lazy_sfm_tools.materials.overrides import MaterialAttributesInfo


class OverrideMaterialElement(object):
    """Data transfer object for material override."""
    ANY = "*"

    def __init__(self, for_materials, attribute_name, attribute_value):
        """Data transfer object for material override.

        Args:
            for_materials (list[str|unicode]): materials for which the attribute should be applied.
            attribute_name (str|unicode): name of the attribute to be added to the materials.
            attribute_value (Any): value of the attribute to be added to the materials.
        """
        for_materials = (for_materials if isinstance(for_materials, (list, tuple)) else [for_materials])
        self._for_materials = [
            str(material_name.lower()) if "$everything$" not in str(material_name.lower()) else "*"
            for material_name in for_materials
        ]
        self._attr_name = "$" + str(attribute_name).replace("$", "")
        self._attr_type = MaterialAttributesInfo.define_type(self._attr_name).value
        self._attr_value = attribute_value

    @property
    def is_valid(self):
        """Check attribute is valid.

        Returns:
            bool: true if the attribute type was defined by its name, false if not.
        """
        return self._attr_type != AttributeTypes.UNKNOWN

    @property
    def for_materials(self):
        """Materials for which the attribute should be applied.

        Returns:
            list[str|unicode]: materials for which the attribute should be applied.
        """
        return self._for_materials

    @property
    def attr_name(self):
        """Name of the attribute to be added to the materials.

        Returns:
            str: name of the attribute to be added to the materials.
        """
        return self._attr_name

    @property
    def attr_type(self):
        """Type of the attribute to be added to the materials.

        Returns:
            str: type of the attribute to be added to the materials.
        """
        return self._attr_type

    @property
    def attr_value(self):
        """Value of the attribute to be added to the materials.

        Returns:
            str: value of the attribute to be added to the materials.
        """
        return self._attr_value

    @property
    def attribute_data_array(self):
        """Override material element as array.

        Returns:
            list: array representation of override material element.
        """
        return [self.attr_name, self.attr_type, self.attr_value]
