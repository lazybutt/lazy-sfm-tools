import sfm
from vs.movieobjects import CDmeGameModel
from lazy_sfm_tools.commons.enums.attribute_types import AttributeTypes
from lazy_sfm_tools.materials.common import Material
from lazy_sfm_tools.materials.common import MaterialsData
from lazy_sfm_tools.materials.overrides import OverrideMaterialElement
from lazy_sfm_tools.utils.type_checks import isinstance_or_raise


class OverrideMaterialsApplier(object):
    """Functionality of overriding model materials."""

    def __init__(self, gamemodel):
        """Constructor.

        Args:
            gamemodel (CDmeGameModel): target for override materials.

        Raises:
             TypeError: if received gamemodel parameter is not CDmeGameModel.
        """
        isinstance_or_raise(gamemodel, CDmeGameModel, alias="gamemodel")
        self._gamemodel = gamemodel

    @staticmethod
    def for_animset(animset):
        """Additional constructor.

        Args:
            animset: animation set (model) target for override materials.

        Raises:
             TypeError: if animation set is not a model.

        Returns:
            OverrideMaterialsApplier: constructed applier.
        """
        if not animset.HasAttribute("gameModel"):
            raise TypeError("Animation set must be model")
        return OverrideMaterialsApplier(animset.gameModel)

    @property
    def has_overrides(self):
        """Check if gamemodel has material overrides.

        Returns:
            bool: true if gamemodel has material overrides, false if not.
        """
        return self._gamemodel.HasAttribute('materials')

    def add_overrides(self, override_elements):
        """Add overrides for gamemodel's materials.

        Args:
            override_elements (list[OverrideMaterialElement]): a list of overrides
                that must be added to the gamemodel's materials.
        """
        materials = {}
        if not self.has_overrides:
            self.make_empty_overrides()
        for material in self._gamemodel.materials:
            materials[material.GetName()] = material
        for item in override_elements:  # type: OverrideMaterialElement
            if not item.is_valid:
                sfm.Msg("Attribute {} skipped for materials: {}. Attribute type UNKNOWN.\n")
            if OverrideMaterialElement.ANY in item.for_materials:
                for material in materials.values():
                    self._add_attribute_and_value(material, *item.attribute_data_array)
            else:
                for material_name in item.for_materials:
                    if material_name in materials:
                        self._add_attribute_and_value(materials[material_name], *item.attribute_data_array)
                    else:
                        sfm.Msg(
                            'Attribute "{}" not applied to "{}" material. Material not found.\n'
                                .format(item.attr_name, material_name)
                        )

    def make_empty_overrides(self):
        """Make empty override materials for gamemodel."""
        self.remove_overrides()
        materials_attr = self._gamemodel.AddAttribute('materials', AttributeTypes.ELEMENT_ARRAY.value)
        added_material_names = []
        for mat_name, mat_path in MaterialsData.from_gamemodel(self._gamemodel).mat_names_to_game_paths.items():
            if mat_name in added_material_names:
                continue
            material = Material(name=mat_name, path=mat_path.typed_content_relative_path)
            materials_attr.AddToTail(material.native)
            added_material_names.append(mat_name)

    def remove_overrides(self):
        """Remove gamemodel's material overrides if exists."""
        if self.has_overrides:
            self._gamemodel.RemoveAttribute('materials')

    @classmethod
    def _add_attribute_and_value(cls, material, attr_name, attr_type, attr_value):
        from lazy_sfm_tools.misc import AttributeSetter
        attr = None
        if material.HasAttribute(str(attr_name)):
            attr = material.GetAttribute(str(attr_name))
            if attr.GetType() != attr_type:
                material.RemoveAttributeByPtr(attr)
        if attr is None:
            attr = material.AddAttribute(str(attr_name), attr_type)
        AttributeSetter.set(attr, attr_value)
        return attr
