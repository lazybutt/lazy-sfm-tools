import os.path

from lazy_sfm_tools.commons.enums import AttributeTypes
from lazy_sfm_tools.commons import Paths
from lazy_sfm_tools.files.file_structure import GamePath

class _MaterialAttributesInfo(object):
    class _MaterialAttributeInfo(object):
        """Object holder for information about material attribute."""
        def __init__(self, name, attr_type, compatibility, documentation):
            self._name = name
            self._attr_type = attr_type
            self._compatibility = compatibility
            self._documentation = documentation

        @classmethod
        def create_from_dict(cls, obj):
            return cls(
                name=str(obj[0]).lower().replace("$", ""),
                attr_type=AttributeTypes.of(int(obj[1])),
                compatibility=obj[2] if len(obj) >= 3 else None,
                documentation=obj[3] if len(obj) >= 4 else None,
            )

        @property
        def attr_name(self):
            """Attribute name.

            Returns:
                str: attribute name.
            """
            return self._name

        @property
        def attr_type(self):
            """Attribute type.

            Returns:
                AttributeTypes: attribute type.
            """
            return self._attr_type

        @property
        def compatibility(self):
            """Attribute compatibility.

            Returns:
                str: attribute compatibility.
            """
            return self._compatibility

        @property
        def documentation(self):
            """Attribute documentation.

            Returns:
                str: attribute documentation.
            """
            return self._documentation

    def __init__(self):
        self._attr_infos = []
        self._attr_infos_by_name = {}
        self._attr_infos_by_type = {}
        self.reload()

    def reload(self):
        """Reload attributes information from file."""
        config_path = Paths.MATERIAL_ATTRS_INFO
        if not os.path.exists(config_path):
            raise OSError(
                'Loading material attributes info failed. Path "{}" not found'
                    .format(Paths.MATERIAL_ATTRS_INFO)
            )

        with open(config_path, 'r') as csv_file:
            import csv
            reader = csv.reader(csv_file, dialect='excel')
            for row in reader:
                attr_info = self._MaterialAttributeInfo.create_from_dict(row)
                self._attr_infos_by_name[attr_info.attr_name] = attr_info
                if attr_info.attr_type not in self._attr_infos_by_type:
                    self._attr_infos_by_type[attr_info.attr_type] = [attr_info]
                else:
                    self._attr_infos_by_type[attr_info.attr_type].append(attr_info)
                self._attr_infos.append(attr_info)

    def define_type(self, attr_name):
        """Define attribute type by attribute name.

        Args:
            attr_name: name of attribute.

        Returns:
            AttributeTypes: type of given attribute name.
        """
        attr_name = attr_name.lower().replace("$", "")
        if attr_name not in self._attr_infos_by_name.keys():
            from lazy_sfm_tools.commons.enums import AttributeTypes
            return AttributeTypes.UNKNOWN
        return self._attr_infos_by_name[attr_name].attr_type

    def all_by_type(self, attr_type):
        """Receive all material attributes with given type.

        Args:
            attr_type (AttributeTypes): attribute type.

        Returns:
            list[_MaterialAttributesInfo._MaterialAttributeInfo]: list of material attribute information.
        """
        if attr_type in self._attr_infos_by_type.keys():
            return [info for info in self._attr_infos_by_type[attr_type]]
        else:
            return []


MaterialAttributesInfo = _MaterialAttributesInfo()
