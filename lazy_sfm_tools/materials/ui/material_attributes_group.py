from lazy_sfm_tools.ui.line_edits import FieldsGroup
from lazy_sfm_tools.utils.value_buffer import BufferByID


class MaterialAttributesGroup(FieldsGroup):
    """Group of editable material attributes UI element."""

    DEFAULT_FILE_SELECT_DIALOG = {
        "title": 'Select Texture',
        "extensions": 'Valve Texture File (*.vtf)',
        "folder_root_source_id": BufferByID.add('usermod\\materials\\models'),
        "category": 'materials'
    }

    class Attribute(object):
        """
        Attribute object
        """
        def __init__(self, key, attr_name, attr_value):
            self._key = key
            self._attr_name = attr_name
            self._attr_value = attr_value

        @property
        def key(self):
            return self._key

        @property
        def attribute_name(self):
            return self._attr_name

        @property
        def attribute_value(self):
            return self._attr_value

    def __init__(self, labelWidth=120, fieldWidth=150, height=25, disableLabel=False, disableRestore=False,
                 file_select_dialog_settings=None, **kwargs):
        """Group of editable material attributes UI element.

        Args:
            labelWidth (int): width of attribute name label.
            fieldWidth (int): width of attribute value edit line.
            height (int): attribute widget height.
            disableLabel (bool): disable label with attribute name.
            disableRestore (bool): disable restore value button.
            file_select_dialog_settings (dict): kw with default parameters.
                for filedialog, None if you need default.
            **kwargs (dict): PyQt attributes.
        """
        super(MaterialAttributesGroup, self).__init__(labelWidth, fieldWidth, height, disableLabel, disableRestore, **kwargs)
        self._defaultFileSelectDialog = file_select_dialog_settings \
            if file_select_dialog_settings is not None \
            else self.DEFAULT_FILE_SELECT_DIALOG

    def clear(self):
        """Clear all material attribute widgets from UI group."""
        for widget in self._widgets.values():
            self._layout.removeWidget(widget)
            widget.deleteLater()
        self._widgets = {}

    def add_attribute_by_name(self, attrName, label='', value=None, mandatory=True, key=None):
        """Add material attribute to UI group by material attribute name.

        Args:
            attrName (str|unicode): the name of the material attribute
                by which the attribute type will be defined.
            label (str|unicode): attribute label.
            value (Any|None): value to fill if needed.
            mandatory (bool): true if it is required to fill the attribute field of the attribute, false if not.
            key (Any|None): unique key for field
        """
        from lazy_sfm_tools.materials.overrides import MaterialAttributesInfo
        attrType = MaterialAttributesInfo.define_type(attrName)
        self.add_attribute_by_type(attrType, label, value, mandatory, key)

    def add_attribute_by_type(self, attrType, label='', value=None, mandatory=True, key=None):
        """Add material attribute to UI group by attribute type.

        Args:
            attrType (str|unicode): attribute type.
            label (str|unicode): attribute label.
            value (Any|None): value to fill if needed.
            mandatory (bool): true if it is required to fill the attribute field of the attribute, false if not.
            key (Any|None): unique key for field
        """
        attrs = dict(label=label, value=value, mandatory=mandatory, key=key)
        from lazy_sfm_tools.commons.enums import AttributeTypes
        if attrType == AttributeTypes.INT:
            self.add_int(**attrs)
        elif attrType == AttributeTypes.FLOAT:
            self.add_float(**attrs)
        elif attrType == AttributeTypes.STRING:
            self.add_file(**dict(self._defaultFileSelectDialog, **attrs))
        elif attrType == AttributeTypes.COLOR:
            from lazy_sfm_tools.ui.line_edits import ColorField
            self.add_color(output_format=ColorField.OUTPUT_AS_HEX, **attrs)
        elif attrType == AttributeTypes.VECTOR2:
            self.add_vector(dimensions=2, **attrs)
        elif attrType == AttributeTypes.VECTOR3:
            self.add_vector(dimensions=3, **attrs)
        elif attrType == AttributeTypes.VECTOR4 or attrType == AttributeTypes.QUATERNION:
            self.add_vector(dimensions=4, **attrs)
        elif attrType == AttributeTypes.BOOL:
            raise NotImplementedError()
        else:
            raise NotImplementedError()
