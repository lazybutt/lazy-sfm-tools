def isinstance_or_raise(obj, type_or_tuple, **kw):
    """
    Check type and raise Error if type not matched

    Args:
        obj (object): object to check
        type_or_tuple (type|tuple[type]|list[type]): required type or tuple of required types
        **kw: additional arguments

    Keyword Args:
        alias (str|unicode): name of parameter for creating error message
        nullable (bool): validate by None, default False

    Returns:
        None: nothing
    """
    alias = kw["alias"] if "alias" in kw else None
    nullable = kw["nullable"] if "nullable" in kw else False

    if obj is None and not nullable:
        raise ValueError("NotNull violation")
    if not isinstance(obj, type_or_tuple) and nullable is False:
        raise TypeError('"{}" must be {}'.format(
            alias,
            " or ".join([item.__str__() for item in type_or_tuple])
            if isinstance(type_or_tuple, (list, tuple)) else type_or_tuple
        ))
