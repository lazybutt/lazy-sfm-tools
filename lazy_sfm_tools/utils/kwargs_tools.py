from lazy_sfm_tools.utils.type_checks import isinstance_or_raise

class KwargsReader(object):
    def __init__(self, dict_obj):
        """Dict wrapper for easy read values.

        Args:
            dict_obj (dict): dict object to read
        """
        self._kw = dict_obj

    def get(self, key, type_or_tuple=None, none_allowed=False):
        """Get from dict with options.

        Args:
            key (str|unicode): unique dict key.
            type_or_tuple (type|tuple[type]|list[type]): required type or tuple of required types.
            none_allowed (bool): none checking, False by default.

        Raises:
            KeyValue: if key not found in dict
            ValueError: if none_allowed is false but received by key value is None
            TypeError: if received by key value's type not matched

        Returns:
            Any: value by received key.
        """
        if key in self._kw:
            if not none_allowed:
                raise ValueError('"{}" can not be None')
            return None
        if type_or_tuple is not None:
            isinstance_or_raise(self._kw[key], type_or_tuple, alias=key)
        value = self._kw.pop(key)
        return str(value) if isinstance(value, unicode) else value

    def valid_keys(self, keys, errorMessage=None):
        """Validate that dict has all excepted keys.

        Args:
            keys: expected keys in dict.
            errorMessage: message indicated in the error, None by default.

        Raises:
            KeyError: if at least one expected key not found in dict.
        """
        if not isinstance(keys, (list, tuple)):
            keys = [keys]
        keys_not_found = []
        for key in keys:
            if key not in self._kw:
                keys_not_found.append(key)
        if keys_not_found:
            if errorMessage is None:
                errorMessage = "Missing values: " + ", ".join(keys_not_found)
            raise KeyError(errorMessage)

    def has_keys(self, keys):
        """Check that dict has all excepted keys.

        Args:
            keys: expected keys in dict.

        Returns:
            bool: check result, true if dict contain all excepted keys, false if not.
        """
        if not isinstance(keys, (list, tuple)):
            keys = [keys]
        keys_not_found = []
        for key in keys:
            if key not in self._kw:
                keys_not_found.append(key)
        if keys_not_found:
            return False
        return True

    def pop_or_default(self, key, default):
        """Pop value from dict by key, or default value.

        Args:
            key (str|unicode): unique dict key.
            default (Any): default value if value not found by key.

        Returns:
            Any: value by received key of default value.
        """
        return self._kw.pop(key) if key in self._kw else default

    def get_or_default(self, key, default):
        """Get value from dict by key, or default value.

        Args:
            key (str|unicode): unique dict key.
            default (object): default value if value not found by key.

        Returns:
            Any: value by received key of default value.
        """
        return self._kw[key] if key in self._kw else default

    def pop_or_raise(self, key, error_text):
        """Pop value from dict by key, or raise KeyError with custom message.

        Args:
            key (str|unicode): unique dict key.
            error_text (str|unicode): error text.

        Raises:
            KeyError: if value not found by key.

        Returns:
            Any: value by received key.
        """
        if key not in self._kw:
            raise KeyError(error_text)
        return self._kw.pop(key)

    def get_or_raise(self, key, error_text):
        """Get value from dict by key, or raise KeyError with custom message.

        Args:
            key (str|unicode): unique dict key.
            error_text (str|unicode): error text.

        Raises:
            KeyError: if value not found by key.

        Returns:
            Any: value by received key.
        """
        if key not in self._kw:
            raise KeyError(error_text)
        return self._kw[key]

    def __getitem__(self, key, default):
        """Get value from dict by key, or default value.

        Args:
            key (str|unicode): unique dict key.
            default (object): default value if value not found by key.

        Returns:
            Any: value by received key of default value.
        """
        return self.get_or_default(key, default)
