class _BufferByID(object):
    """
    The main reason why this class was created was for the scenario where you need
    to add an entity (which is part of the session structure like CDmeAnimationSet, CDmeClip etc)
    to the UI element, SFM crashes.

    For example when you create a QComboBox and add animset's names
    with a link to the animset object itself in data slot.

    I assume that this has something to do with what PyQt does with
    the objects added to the UI elements data slots, e.g. trying to remove
    (from sfm memory) them after use, which breaks UNDO's consistency.

    It was the fastest and easiest solution:
        Store value:
            - put value in the BufferByID and get key
            - put key in data slot of UI element
        Get stored value:
            - get key from data slot of UI element
            - get value by key from BufferByID.

    This approach should be used only in the above situations,
    creating an eternal dictionary, which will always be something to store,
    can in some cases lead to memory overflow and cause a crash.

    I DO NOT recommend using this as a way to pass data (which is not session structure data)
    between two unrelated pieces of code. Well, only if you really want to and you understand
    that the stored data has a static size most of the time and will not increase with
    repeated calls to the script.

    So use variable "one_time_usage" if you're sure that the value will
    need to be retrieved only once, otherwise use tag "batch_delete_key"
    for all stored values which will allow to release all created values by your script.
    """

    class _BufferItem(object):
        def __init__(self, value, one_time_usage, batch_delete_key=None):
            self._value = value
            self._one_time_usage = one_time_usage
            self._batch_delete_key = batch_delete_key

        @property
        def value(self):
            return self._value

        @property
        def is_one_time_usage(self):
            return self._one_time_usage

        def batch_delete_key(self):
            return self._batch_delete_key

    def __init__(self):
        self._storage = {}

    #
    def add(self, item, key=None, only_one_use=False, batch_delete_key=None):
        """Adding a value to the buffer by received or generated (if not received) key.

        Args:
            item (Any): value to add to buffer.
            key (str|unicode): key to retrieve the value (will be generated if None).
            only_one_use (bool): a flag indicating that the value must be deleted after the first request.
            batch_delete_key (Any): alternative key for deletion. you can set this field to the name
                of your script or class name and call the batch_delete_by_key function after it runs.

        Returns:
            str|unicode: received or generated key.
        """
        import uuid
        key = str(uuid.uuid4()) if key is None else key
        self._storage[key] = self._BufferItem(item, only_one_use, batch_delete_key)
        return key

    def get(self, key):
        """Receive value by unique key.

        Notes:
            If item marked as "only one use" value will be deleted from buffer.

        Args:
            key (str|unicode): unique key related to value.

        Raises:
            KeyError: when value wasn't found by key.

        Returns:
            Any: value related to key.
        """
        if key in self._storage.keys():
            buffer_item = self._storage.get(key)
            if buffer_item.is_one_time_usage:
                self._storage.pop(key)
            return buffer_item.value
        raise KeyError()

    def batch_delete_by_key(self, key):
        """Delete values by not unique key.

        Args:
            key (Any): not unique key which is common for more than one item.

        Returns:
            None: nothing
        """
        items_to_delete = filter(lambda item: item[1].batch_delete_key == key, self._storage.items())
        for key_to_delete, _ in items_to_delete:
            self._storage.pop(key_to_delete)

    def __contains__(self, key):
        """Check if the key contains.

        Args:
            key (str|unicode): key to check.

        Returns:
            bool: true if key exists, false if not.
        """
        return str(key) in self._storage.keys()

    def __getitem__(self, key):
        """Get stored value by key.

        Notes:
            Getting operation may delete stored key and value
            if stored item have parameter "one_time_usage" == true

        Args:
            key (str|unicode): key to get value.

        Returns:
            bool: true if key exists, false if not.
        """
        return self.get(key)

    def __setitem__(self, key, value):
        """Adding a value to the buffer by key.

        Args:
            key (str|unicode): key to retrieve the value (will be generated if None).
            value (Any): value to add to buffer.

        Returns:
            str|unicode: initial key.
        """
        return self.add(value, key)

    def __delitem__(self, key):
        """Adding a value to the buffer by key.

        Args:
            key (str|unicode): key to retrieve the value (will be generated if None).
        """
        self._storage.pop(key)


BufferByID = _BufferByID()
