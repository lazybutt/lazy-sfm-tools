import vs
from vs.datamodel import CDmElement

def create_element(typeAlias, elementName="unnamed", parent=None):
    """Create vs.CDmElement.

    Args:
        typeAlias (str|unicode): type of element to be created.
        elementName (str|unicode): name of element to be created.
        parent (CDmElement): parent element (can be automatically set,
            when a created element is added to any parent element via the attribute).

    Returns:
        Any: created element with a type inherited from CDmElement.
    """
    if parent is not None:
        if not hasattr(parent, "GetId") or not hasattr(parent, "GetFileId"):
            raise TypeError('"parent" must be {}'.format(vs.CDmElement))
        return vs.CreateElement(str(typeAlias), str(elementName), parent.GetFileId(), int(parent.GetId().m_Value))
    return vs.CreateElement(str(typeAlias), str(elementName), 0)
