import vs
from vs.datamodel import CDmElement
from vs.movieobjects import CDmeAnimationSet, CDmeConnectionOperator

def create_connection_operator(operator_name, animset, input_object, input_attribute, outputs):
    """

    Args:
        operator_name (str): operator name.
        animset (CDmeAnimationSet): anim set to add operator.
        input_object (CDmElement): input element.
        input_attribute (str|unicode): attribute name of input element.
        outputs (dict[CDmElement, str|unicode]): dict of outputs (pairs of input elements and attribute names)

    Returns:
        CDmeConnectionOperator:
    """
    operator = vs.CreateElement("DmeConnectionOperator", operator_name, animset.GetFileId())
    operator.SetInput(input_object, input_attribute)
    for output_object, output_attribute in outputs.items():
        operator.AddOutput(output_object, output_attribute)
    animset.AddOperator(operator)
    return operator
