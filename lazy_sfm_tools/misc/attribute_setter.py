from vs.misc import Color
from vs.mathlib import Vector2D, Vector, Vector4D
from vs.datamodel import CDmAttribute
from lazy_sfm_tools.commons.enums import AttributeTypes
from lazy_sfm_tools.misc.type_converters import ColorConverter, ColorReprType
from lazy_sfm_tools.misc.type_converters import Vector2Converter, Vector2ReprType
from lazy_sfm_tools.misc.type_converters import Vector3Converter, Vector3ReprType
from lazy_sfm_tools.misc.type_converters import Vector4Converter, Vector4ReprType
from lazy_sfm_tools.misc.type_converters import QuaternionConverter, QuaternionReprType

class AttributeSetter(object):

    @classmethod
    def set(cls, attr, value):
        """Universal method to set a value for an attribute, with converting the value
        to a suitable format considering the attribute type

        Args:
            attr (CDmAttribute): attribute for setting value
            value: value to set
        """
        if not isinstance(attr, CDmAttribute):
            raise TypeError("Type must be CDmAttribute")
        attr_type = AttributeTypes.of(attr.GetType())

        convert_fun = None
        if attr_type in (AttributeTypes.STRING, AttributeTypes.STRING_ARRAY):
            convert_fun = lambda val: str(val)
        elif attr_type in (AttributeTypes.FLOAT, AttributeTypes.FLOAT_ARRAY):
            convert_fun = lambda val: float(val)
        elif attr_type in (AttributeTypes.INT, AttributeTypes.INT_ARRAY):
            convert_fun = lambda val: int(round(val))
        elif attr_type in (AttributeTypes.COLOR, AttributeTypes.COLOR_ARRAY):
            convert_fun = lambda val: ColorConverter.convert(val)
        elif attr_type in (AttributeTypes.VECTOR2, AttributeTypes.VECTOR2_ARRAY):
            convert_fun = lambda val: Vector2Converter.convert(val)
        elif attr_type in (AttributeTypes.VECTOR3, AttributeTypes.VECTOR3_ARRAY):
            convert_fun = lambda val: Vector3Converter.convert(val)
        elif attr_type in (AttributeTypes.VECTOR4, AttributeTypes.VECTOR4_ARRAY):
            convert_fun = lambda val: Vector4Converter.convert(val)
        elif attr_type in (AttributeTypes.QUATERNION, AttributeTypes.QUATERNION_ARRAY):
            convert_fun = lambda val: QuaternionConverter.convert(val)

        cls._set_attribute(attr, value, convert_fun)

    @staticmethod
    def set_string(attr, value):
        """Set a value for an string attribute, with additional conversions if necessary.

        Args:
            attr (CDmAttribute): attribute for setting value.
            value (str|unicode): value to set.
        """
        AttributeTypes.validate_attr_type(attr, AttributeTypes.STRING)
        attr.SetValue(str(value))

    @staticmethod
    def add_string(attr, value):
        """Add a value to an string array attribute, with additional conversions if necessary.

        Args:
            attr (CDmAttribute): attribute for adding value.
            value (str|unicode): value to add.
        """
        AttributeTypes.validate_attr_type(attr, AttributeTypes.STRING_ARRAY)
        attr.AddToTail(str(value))

    @staticmethod
    def add_strings(attr, values):
        """Add all values to an string array attribute, with additional conversions if necessary.

        Args:
            attr (CDmAttribute): attribute for adding values.
            values (list[str|unicode]): values to add.
        """
        AttributeTypes.validate_attr_type(attr, AttributeTypes.STRING_ARRAY)
        for value in values:
            attr.AddToTail(str(value))

    @staticmethod
    def set_int(attr, value):
        """Set a value for an int attribute, with additional conversions if necessary.

        Args:
            attr (CDmAttribute): attribute for setting value.
            value (int): value to set.
        """
        AttributeTypes.validate_attr_type(attr, AttributeTypes.INT)
        attr.SetValue(int(round(value)))

    @staticmethod
    def add_int(attr, value):
        """Add a value to an int array attribute, with additional conversions if necessary.

        Args:
            attr (CDmAttribute): attribute for adding value.
            value (int): value to add.
        """
        AttributeTypes.validate_attr_type(attr, AttributeTypes.INT_ARRAY)
        attr.AddToTail(int(round(value)))

    @staticmethod
    def add_ints(attr, values):
        """Add all values to an int array attribute, with additional conversions if necessary.

        Args:
            attr (CDmAttribute): attribute for adding values.
            values (list[int]): values to add.
        """
        AttributeTypes.validate_attr_type(attr, AttributeTypes.INT_ARRAY)
        for value in values:
            attr.AddToTail(int(round(value)))

    @staticmethod
    def set_float(attr, value):
        """Set a value for an float attribute, with additional conversions if necessary.

        Args:
            attr (CDmAttribute): attribute for setting value.
            value (int|float): value to set.
        """
        AttributeTypes.validate_attr_type(attr, AttributeTypes.FLOAT)
        attr.SetValue(float(value))

    @staticmethod
    def add_float(attr, value):
        """Add a value to an float array attribute, with additional conversions if necessary.

        Args:
            attr (CDmAttribute): attribute for adding value.
            value (int|float): value to add.
        """
        AttributeTypes.validate_attr_type(attr, AttributeTypes.FLOAT_ARRAY)
        attr.AddToTail(float(value))

    @staticmethod
    def add_floats(attr, values):
        """Add all values to an float array attribute, with additional conversions if necessary.

        Args:
            attr (CDmAttribute): attribute for adding values.
            values (list[int|float]): values to add.
        """
        AttributeTypes.validate_attr_type(attr, AttributeTypes.FLOAT_ARRAY)
        for value in values:
            attr.AddToTail(float(value))

    @staticmethod
    def set_color(attr, value):
        """Set a value for an color attribute, with additional conversions if necessary.

        Args:
            attr (CDmAttribute): attribute for setting value.
            value(Color|list[int]|tuple[int]|str|unicode): value to set.
        """
        AttributeTypes.validate_attr_type(attr, AttributeTypes.COLOR)
        attr.SetValue(ColorConverter.convert(value, ColorReprType.COLOR_NATIVE))

    @staticmethod
    def add_color(attr, value):
        """Add a value to an color array attribute, with additional conversions if necessary.

        Args:
            attr (CDmAttribute): attribute for adding value.
            value(Color|list[int]|tuple[int]|str|unicode): value to add.
        """
        AttributeTypes.validate_attr_type(attr, AttributeTypes.COLOR_ARRAY)
        attr.AddToTail(ColorConverter.convert(value, ColorReprType.COLOR_NATIVE))

    @staticmethod
    def add_colors(attr, values):
        """Add all values to an color array attribute, with additional conversions if necessary.

        Args:
            attr (CDmAttribute): attribute for adding values.
            values(list[Color|list[int]|tuple[int]|str|unicode]): values to add.
        """
        AttributeTypes.validate_attr_type(attr, AttributeTypes.COLOR_ARRAY)
        for value in values:
            attr.AddToTail(ColorConverter.convert(value, ColorReprType.COLOR_NATIVE))

    @staticmethod
    def set_vector2(attr, value):
        """Set a value for an vector2 attribute, with additional conversions if necessary.

        Args:
            attr (CDmAttribute): attribute for setting value.
            value(Vector2D|list[float|int]|tuple[float|int]|str|unicode): value to set.
        """
        AttributeTypes.validate_attr_type(attr, AttributeTypes.VECTOR2)
        attr.SetValue(Vector2Converter.convert(value, Vector2ReprType.VECTOR_NATIVE))

    @staticmethod
    def add_vector2(attr, value):
        """Add a value to an vector2 array attribute, with additional conversions if necessary.

        Args:
            attr (CDmAttribute): attribute for adding value.
            value(Vector2D|list[float|int]|tuple[float|int]|str|unicode): value to add.
        """
        AttributeTypes.validate_attr_type(attr, AttributeTypes.VECTOR2_ARRAY)
        attr.AddToTail(Vector2Converter.convert(value, Vector2ReprType.VECTOR_NATIVE))

    @staticmethod
    def add_vector2s(attr, values):
        """Add all values to an vector2 array attribute, with additional conversions if necessary.

        Args:
            attr (CDmAttribute): attribute for adding values.
            values(list[Vector2D|list[float|int]|tuple[float|int]|str|unicode]): values to add.
        """
        AttributeTypes.validate_attr_type(attr, AttributeTypes.VECTOR2_ARRAY)
        for value in values:
            attr.AddToTail(Vector2Converter.convert(value, Vector2ReprType.VECTOR_NATIVE))

    @staticmethod
    def set_vector3(attr, value):
        """Set a value for an vector3 attribute, with additional conversions if necessary.

        Args:
            attr (CDmAttribute): attribute for setting value.
            value(Vector|list[float|int]|tuple[float|int]|str|unicode): value to set.
        """
        AttributeTypes.validate_attr_type(attr, AttributeTypes.VECTOR3)
        attr.SetValue(Vector3Converter.convert(value, Vector3ReprType.VECTOR_NATIVE))

    @staticmethod
    def add_vector3(attr, value):
        """Add a value to an vector3 array attribute, with additional conversions if necessary.

        Args:
            attr (CDmAttribute): attribute for adding value.
            value(Vector|list[float|int]|tuple[float|int]|str|unicode): value to add.
        """
        AttributeTypes.validate_attr_type(attr, AttributeTypes.VECTOR3_ARRAY)
        attr.AddToTail(Vector3Converter.convert(value, Vector3ReprType.VECTOR_NATIVE))

    @staticmethod
    def add_vector3s(attr, values):
        """Add a value to an vector3 array attribute, with additional conversions if necessary.

        Args:
            attr (CDmAttribute): attribute for adding values.
            values(list[Vector|list[float|int]|tuple[float|int]|str|unicode]): values to add.
        """
        AttributeTypes.validate_attr_type(attr, AttributeTypes.VECTOR3_ARRAY)
        for value in values:
            attr.AddToTail(Vector3Converter.convert(value, Vector3ReprType.VECTOR_NATIVE))

    @staticmethod
    def set_vector4(attr, value):
        """Set a value for an vector4 attribute, with additional conversions if necessary.

        Args:
            attr (CDmAttribute): attribute for setting value
            value(Vector4D|list[float|int]|tuple[float|int]|str|unicode): value to set
        """
        AttributeTypes.validate_attr_type(attr, AttributeTypes.VECTOR4)
        attr.SetValue(Vector4Converter.convert(value, Vector4ReprType.VECTOR_NATIVE))

    @staticmethod
    def add_vector4(attr, value):
        """Add a value to an vector4 array attribute, with additional conversions if necessary.

        Args:
            attr (CDmAttribute): attribute for adding value
            value(Vector4D|list[float|int]|tuple[float|int]|str|unicode): value to add
        """
        AttributeTypes.validate_attr_type(attr, AttributeTypes.VECTOR4_ARRAY)
        attr.AddToTail(Vector4Converter.convert(value, Vector4ReprType.VECTOR_NATIVE))

    @staticmethod
    def add_vector4s(attr, values):
        """Add all values to an vector4 array attribute, with additional conversions if necessary.

        Args:
            attr (CDmAttribute): attribute for adding values
            values(list[Vector4D|list[float|int]|tuple[float|int]|str|unicode]): values to add
        """
        AttributeTypes.validate_attr_type(attr, AttributeTypes.VECTOR4_ARRAY)
        for value in values:
            attr.AddToTail(Vector4Converter.convert(value, Vector4ReprType.VECTOR_NATIVE))

    @staticmethod
    def set_quaternion(attr, value):
        """Set a value for an quaternion attribute, with additional conversions if necessary.

        Args:
            attr (CDmAttribute): attribute for setting value
            value(Vector4D|list[float|int]|tuple[float|int]|str|unicode): value to set
        """
        AttributeTypes.validate_attr_type(attr, AttributeTypes.QUATERNION)
        attr.SetValue(QuaternionConverter.convert(value, QuaternionReprType.QUATERNION_NATIVE))

    @staticmethod
    def add_quaternion(attr, value):
        """Add a value to an quaternion array attribute, with additional conversions if necessary.

        Args:
            attr (CDmAttribute): attribute for adding value
            value(Vector4D|list[float|int]|tuple[float|int]|str|unicode): value to add
        """
        AttributeTypes.validate_attr_type(attr, AttributeTypes.QUATERNION_ARRAY)
        attr.AddToTail(QuaternionConverter.convert(value, QuaternionReprType.QUATERNION_NATIVE))

    @staticmethod
    def add_quaternions(attr, values):
        """Add all values to an quaternion array attribute, with additional conversions if necessary.

        Args:
            attr (CDmAttribute): attribute for adding values
            values(list[Vector4D|list[float|int]|tuple[float|int]|str|unicode]): values to add
        """
        AttributeTypes.validate_attr_type(attr, AttributeTypes.QUATERNION_ARRAY)
        for value in values:
            attr.AddToTail(QuaternionConverter.convert(value, QuaternionReprType.QUATERNION_NATIVE))

    @staticmethod
    def _set_attribute(attr, value, converter_func):
        if converter_func is None:
            attr.SetValue(value)
        if isinstance(value, (list, tuple)):
            attr.SetValue([converter_func(item) for item in value])
        else:
            attr.SetValue(converter_func(value))
