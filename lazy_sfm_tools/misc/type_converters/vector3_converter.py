import vs
from enum import Enum
from vs.mathlib import Vector
from lazy_sfm_tools.utils.type_checks import isinstance_or_raise

class Vector3ReprType(Enum):
    VECTOR_ARRAY = 0,
    VECTOR_STRING = 1,
    VECTOR_NATIVE = 2

class Vector3Converter(object):

    @classmethod
    def convert(cls, vector3_obj, to=Vector3ReprType.VECTOR_NATIVE):
        """Converting a given vector3 representation to a specific vector3 representation.

        Args:
            vector3_obj (str|unicode|list[int|float]|Vector): vector3 related object to convert.
            to (Vector3ReprType): output vector3 structure (type).

        Returns:
            str|list[float]|Vector: vector3 with selected structure (type).
        """
        vector = cls._parse(vector3_obj)
        if to == Vector3ReprType.VECTOR_ARRAY:
            return vector
        elif to == Vector3ReprType.VECTOR_STRING:
            return " ".join([str(num) for num in vector])
        elif to == Vector3ReprType.VECTOR_NATIVE:
            return vs.Vector(*vector)

    @classmethod
    def _parse(cls, vector_obj):
        import vs
        if isinstance(vector_obj, vs.Vector):
            vector_arr = cls._parse_native_vector(vector_obj)
        elif isinstance(vector_obj, (list, tuple)):
            vector_arr = cls._parse_vector_array(vector_obj)
        elif isinstance(vector_obj, (str, unicode)):
            vector_arr = cls._parse_vector_string(vector_obj)
        else:
            raise TypeError('Received type "{}" not supported'.format(type(vector_obj)))

        return vector_arr

    @staticmethod
    def _parse_native_vector(vector):
        isinstance_or_raise(vector, vs.Vector)
        return [vector.x, vector.y]

    @staticmethod
    def _parse_vector_array(float_array):
        if len(float_array) != 3:
            raise ValueError(
                "Supported only 3-dimensional vectors. "
                "Received {}-dimensional vector {}".format(len(float_array), float_array)
            )
        copy_of_float_array = [float(num) for num in float_array]
        return copy_of_float_array

    @classmethod
    def _parse_vector_string(cls, vector_string):
        vector_string = vector_string.replace(",", " ")

        isAllSymbolsDigitOrSpace = all([char.isdigit() or char.isspace() or char == "." for char in vector_string])
        isContainSpace = " " in vector_string

        if not (isAllSymbolsDigitOrSpace and isContainSpace):
            raise ValueError("Received string \"{}\" is not vector value".format(vector_string))
        return cls._parse_vector_array(vector_string.split())
