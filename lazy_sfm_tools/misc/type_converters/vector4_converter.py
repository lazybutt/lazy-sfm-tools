import vs
from enum import Enum
from vs.mathlib import Vector4D
from lazy_sfm_tools.utils.type_checks import isinstance_or_raise

class Vector4ReprType(Enum):
    VECTOR_ARRAY = 0,
    VECTOR_STRING = 1,
    VECTOR_NATIVE = 2

class Vector4Converter(object):

    @classmethod
    def convert(cls, vector4_obj, to=Vector4ReprType.VECTOR_NATIVE):
        """Converting a given vector4 representation to a specific vector4 representation.

        Args:
            vector4_obj (str|unicode|list[int|float]|Vector4D): vector4 related object to convert.
            to (Vector4ReprType): output vector4 structure (type).

        Returns:
            str|list[float]|Vector4D: vector4 with selected structure (type).
        """
        vector = cls._parse(vector4_obj)
        if to == Vector4ReprType.VECTOR_ARRAY:
            return vector
        elif to == Vector4ReprType.VECTOR_STRING:
            return " ".join([str(num) for num in vector])
        elif to == Vector4ReprType.VECTOR_NATIVE:
            return vs.Vector4D(*vector)

    @classmethod
    def _parse(cls, vector_obj):
        import vs
        if isinstance(vector_obj, vs.Vector4D):
            vector_arr = cls._parse_native_vector(vector_obj)
        elif isinstance(vector_obj, (list, tuple)):
            vector_arr = cls._parse_vector_array(vector_obj)
        elif isinstance(vector_obj, (str, unicode)):
            vector_arr = cls._parse_vector_string(vector_obj)
        else:
            raise TypeError('Received type "{}" not supported'.format(type(vector_obj)))

        return vector_arr

    @staticmethod
    def _parse_native_vector(vector):
        isinstance_or_raise(vector, vs.Vector4D)
        return [vector.x, vector.y]

    @staticmethod
    def _parse_vector_array(float_array):
        if len(float_array) != 4:
            raise ValueError(
                "Supported only 4-dimensional vectors. "
                "Received {}-dimensional vector {}".format(len(float_array), float_array)
            )
        copy_of_float_array = [float(num) for num in float_array]
        return copy_of_float_array

    @classmethod
    def _parse_vector_string(cls, vector_string):
        vector_string = vector_string.replace(",", " ")

        isAllSymbolsDigitOrSpace = all([char.isdigit() or char.isspace() or char == "." for char in vector_string])
        isContainSpace = " " in vector_string

        if not (isAllSymbolsDigitOrSpace and isContainSpace):
            raise ValueError("Received string \"{}\" is not vector value".format(vector_string))
        return cls._parse_vector_array(vector_string.split())
