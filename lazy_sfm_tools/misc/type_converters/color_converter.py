import vs
from enum import Enum
from vs.misc import Color

class ColorReprType(Enum):
    COLOR_ARRAY = 0,
    COLOR_STRING_RGB = 1,
    COLOR_STRING_HEX = 2,
    COLOR_NATIVE = 3

class ColorConverter(object):
    @classmethod
    def convert(cls, color_obj, to=ColorReprType.COLOR_NATIVE, dropAlpha=False):
        """Converting a given color representation to a specific color representation.

        Args:
            color_obj (str|unicode|list[int]|Color): color related object to convert.
            to (ColorReprType): output color structure (type).
            dropAlpha (bool): drop alpha value from output color object.

        Returns:
            str|list[int]|Color: color with selected structure (type).
        """
        color_array = cls._parse(color_obj, dropAlpha)
        if to == ColorReprType.COLOR_ARRAY:
            return color_array
        elif to == ColorReprType.COLOR_STRING_RGB:
            return cls._array_to_color_string(color_array)
        elif to == ColorReprType.COLOR_STRING_HEX:
            return cls._array_to_hex(color_array)
        elif to == ColorReprType.COLOR_NATIVE:
            return vs.misc.Color(*color_array)

    @classmethod
    def _parse(cls, color_obj, cutAlpha):
        import vs
        if isinstance(color_obj, vs.misc.Color):
            color = cls._parse_native_color(color_obj)
        elif isinstance(color_obj, (list, tuple)):
            color = cls._parse_color_array(color_obj)
        elif isinstance(color_obj, (str, unicode)):
            color = cls._parse_color_string(color_obj)
        else:
            raise TypeError('Received type "{}" not supported'.format(type(color_obj)))
        return color[:-1] if cutAlpha else color

    @staticmethod
    def _array_to_hex(color_array):
        return ("#" + ("{:02x}" * 4)).format(*color_array)

    @staticmethod
    def _array_to_color_string(color_array):
        return " ".join([str(num) for num in color_array])

    @staticmethod
    def _parse_native_color(color):
        return [color.r(), color.g(), color.b(), color.a()]

    @classmethod
    def _parse_color_array(cls, color_array):
        copy_of_color_array = [int(num) for num in color_array]
        copy_of_color_array = cls._add_alpha_if_not_exist(copy_of_color_array)
        return copy_of_color_array

    @classmethod
    def _parse_color_string(cls, color_string):
        color_string = color_string.replace(",", " ").lstrip("#")

        isAllSymbolsDigitOrSpace = all([char.isdigit() or char.isspace() for char in color_string])
        isContainSpace = " " in color_string
        isContainComma = "," in color_string

        isHex = len(color_string) in (6, 8) and not isContainSpace and not isContainSpace

        color_arr = []
        if isAllSymbolsDigitOrSpace and (isContainSpace or isContainComma):
            color_arr = [int(num) for num in color_string.split()]
        elif isHex:
            color_arr = tuple(
                int(color_string[i:i + 2], 16)
                for i in [item * 2 for item in range(len(color_string) / 2)]
            )
        color_arr_with_alpha = cls._add_alpha_if_not_exist(color_arr)
        return color_arr_with_alpha

    @staticmethod
    def _add_alpha_if_not_exist(color_arr):
        if len(color_arr) == 3:
            if isinstance(color_arr, list):
                return color_arr + [255]
            elif isinstance(color_arr, tuple):
                return color_arr + (255,)
            else:
                raise NotImplementedError()
        elif len(color_arr) == 4:
            return color_arr
        else:
            raise AssertionError("Length of color array must be 3 or 4. Received: {}".format(color_arr))
