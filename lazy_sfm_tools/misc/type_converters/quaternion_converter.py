import vs
from enum import Enum
from vs.mathlib import Quaternion

class QuaternionReprType(Enum):
    QUATERNION_ARRAY = 0,
    QUATERNION_STRING = 1,
    QUATERNION_NATIVE = 2,

class QuaternionConverter(object):

    @classmethod
    def convert(cls, quaternion_obj, to=QuaternionReprType.QUATERNION_NATIVE):
        """Converting a given quaternion representation to a specific quaternion representation.

        Args:
            quaternion_obj (str|unicode|list[int|float]|Quaternion): quaternion related object to convert.
            to (QuaternionReprType): output quaternion structure (type).

        Returns:
            str|list[float]|Quaternion: quaternion with selected structure (type).
        """
        quaternion = cls._parse(quaternion_obj)
        if to == QuaternionReprType.QUATERNION_ARRAY:
            return quaternion
        elif to == QuaternionReprType.QUATERNION_STRING:
            return " ".join([str(num) for num in quaternion])
        elif to == QuaternionReprType.QUATERNION_NATIVE:
            return vs.Quaternion(*quaternion)

    @classmethod
    def _parse(cls, quaternion_obj):
        import vs
        if isinstance(quaternion_obj, vs.Quaternion):
            quaternion_arr = cls._parse_native_quaternion(quaternion_obj)
        elif isinstance(quaternion_obj, (list, tuple)):
            quaternion_arr = cls._parse_quaternion_array(quaternion_obj)
        elif isinstance(quaternion_obj, (str, unicode)):
            quaternion_arr = cls._parse_quaternion_string(quaternion_obj)
        else:
            raise TypeError('Received type "{}" not supported'.format(type(quaternion_obj)))
        if len(quaternion_arr) != 4:
            raise ValueError(
                "Quaternion is 4-dimensional vector."
                "Received {}-dimensional vector {}".format(
                    len(quaternion_arr), quaternion_arr
                )
            )
        return quaternion_arr

    @staticmethod
    def _parse_native_quaternion(quaternion):
        return [quaternion.x, quaternion.y, quaternion.z, quaternion.w]

    @classmethod
    def _parse_quaternion_array(cls, color_array):
        copy_of_color_array = [float(num) for num in color_array]
        return copy_of_color_array

    @classmethod
    def _parse_quaternion_string(cls, quaternion_string):
        quaternion_string = quaternion_string.replace(",", " ")

        isAllSymbolsDigitOrSpace = all([char.isdigit() or char.isspace() or char == "." for char in quaternion_string])
        isContainSpace = " " in quaternion_string

        if isAllSymbolsDigitOrSpace and isContainSpace:
            return [float(num) for num in quaternion_string.split()]
        else:
            raise ValueError("Received string \"{}\" is not quaternion value".format(quaternion_string))
