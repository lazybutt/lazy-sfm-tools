from vs.misc import Color
from vs.tier1 import DmeTime_t
from vs.datamodel import CDmElement, CDmAttribute
from vs.mathlib import Vector2D, Vector, Vector4D, QAngle, Quaternion

from lazy_sfm_tools.commons.enums import AttributeTypes
from lazy_sfm_tools.misc.serialization.serialized_attribute import SerializedAttribute
from lazy_sfm_tools.misc.serialization.serialized_element import SerializedElement
from lazy_sfm_tools.misc.type_converters import ColorConverter, Vector2Converter, Vector3Converter, Vector4Converter, \
    QuaternionConverter
from lazy_sfm_tools.utils.elements import create_element


class DataModelDeserializer(object):

    @classmethod
    def deserialize_element(cls, element, created_elements=None, ref_attribute_need_to_set=None):
        """

        Args:
            element (SerializedElement): serialized element to deserialize.
            created_elements (dict[str, CDmElement]): created
            ref_attribute_need_to_set (dict[str, CDmAttribute]):

        Returns:
            CDmElement:
        """
        if ref_attribute_need_to_set is None:
            ref_attribute_need_to_set = {}
        if created_elements is None:
            created_elements = {}
        if element.id in created_elements.keys():
            return created_elements[element.id]
        if element.is_ref:
            return None
        created_element = create_element(element.type)  # type: CDmElement
        created_elements[element.id] = created_element
        cls._set_ref_attributes_if_possible(created_elements, ref_attribute_need_to_set)
        for serialized_attr in element.attrs:
            attr_value = cls.deserialize_attribute_value(serialized_attr, created_elements, ref_attribute_need_to_set)
            attr = created_element.GetAttribute(serialized_attr.name) \
                if created_element.HasAttribute(serialized_attr.name) \
                else created_element.AddAttribute(serialized_attr.name, serialized_attr.type)  # type: CDmAttribute
            if serialized_attr.type == AttributeTypes.ELEMENT.value and serialized_attr.value.is_ref:
                ref_attribute_need_to_set[serialized_attr.value.id] = attr
                continue
            if serialized_attr.type == AttributeTypes.ELEMENT_ARRAY.value:
                for val in serialized_attr.value:
                    if val.is_ref:
                        ref_attribute_need_to_set[val.id] = attr
            if isinstance(attr_value, (list, tuple)):
                for value in attr_value:
                    if value is not None:
                        if value not in attr:
                            attr.AddToTail(value)
            else:
                attr.SetValue(attr_value)
        return created_element

    @classmethod
    def deserialize_attribute_value(cls, attribute, created_elements=None, ref_attribute_need_to_set=None):
        """

        Args:
            attribute (SerializedAttribute):
            created_elements (dict[str, CDmElement]):
            ref_attribute_need_to_set (dict[str, CDmAttribute]):

        Returns:
            CDmElement|str|int|float|Color|Vector2D|Vector|Vector4D|QAngle|Quaternion|DmeTime_t|list[CDmElement|str|int|float|Color|Vector2D|Vector|Vector4D|QAngle|Quaternion|DmeTime_t]: deserialized value
        """
        if ref_attribute_need_to_set is None:
            ref_attribute_need_to_set = {}
        if created_elements is None:
            created_elements = {}
        attr_type = AttributeTypes.of(attribute.type)
        if attr_type in (AttributeTypes.ELEMENT, AttributeTypes.ELEMENT_ARRAY):
            proceed_func = lambda item: cls.deserialize_element(item, created_elements, ref_attribute_need_to_set)
            attr_value = cls._proceed_value(attribute.value, proceed_func)
        elif attr_type in (AttributeTypes.complex_types() + AttributeTypes.complex_array_types()):
            attr_value = cls._proceed_value(attribute.value, cls._get_deserializer(attr_type))
        elif attr_type in (AttributeTypes.elementary_types() + AttributeTypes.elementary_array_types()):
            attr_value = attribute.value
        else:
            raise TypeError()

        cls._set_ref_attributes_if_possible(created_elements, ref_attribute_need_to_set)
        return attr_value

    @staticmethod
    def _get_deserializer(attr_type):
        """

        Args:
            attr_type (AttributeTypes):

        Returns:
            function:
        """
        if attr_type in (AttributeTypes.COLOR, AttributeTypes.COLOR_ARRAY):
            return ColorConverter.convert
        if attr_type in (AttributeTypes.VECTOR2, AttributeTypes.VECTOR2_ARRAY):
            return Vector2Converter.convert
        if attr_type in (AttributeTypes.VECTOR3, AttributeTypes.VECTOR3_ARRAY):
            return Vector3Converter.convert
        if attr_type in (AttributeTypes.VECTOR4, AttributeTypes.VECTOR4_ARRAY):
            return Vector4Converter.convert
        if attr_type in (AttributeTypes.QUATERNION, AttributeTypes.QUATERNION_ARRAY):
            return QuaternionConverter.convert

    @staticmethod
    def _set_ref_attributes_if_possible(created_elements, attribute_need_to_set):
        """

        Args:
            created_elements (dict[str, CDmElement]):
            attribute_need_to_set (dict[str, CDmAttribute]):

        Returns:
            None:
        """
        attribute_need_to_set_copy = {ref_id: attribute for ref_id, attribute in attribute_need_to_set.items()}
        for ref_id, attribute in attribute_need_to_set_copy.items():
            if ref_id in created_elements.keys():
                element = created_elements[ref_id]
                if attribute.IsArray():
                    if element not in attribute:
                        attribute.AddToTail(element)
                else:
                    attribute.SetValue(element)
                attribute_need_to_set.pop(ref_id)

    @staticmethod
    def _proceed_value(value, proceed_func):
        """

        Args:
            value (Any):
            proceed_func (function):

        Returns:
            Any:
        """
        if isinstance(value, (list, tuple)):
            return [proceed_func(item) for item in value]
        return proceed_func(value)
