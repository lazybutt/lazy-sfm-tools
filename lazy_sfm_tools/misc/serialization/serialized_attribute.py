from vs.datamodel import CDmAttribute


class SerializedAttribute(dict):
    def __init__(self, attr_name, attr_type, attr_value):
        """

        Args:
            attr_name (str|unicode):
            attr_type (int):
            attr_value (Any):
        """
        super(SerializedAttribute, self).__init__()
        self["_attr_name"] = str(attr_name)
        self["_attr_type"] = int(attr_type)
        self["_attr_value"] = attr_value

    @property
    def name(self):
        """

        Returns:
            str:
        """
        return self["_attr_name"]

    @property
    def type(self):
        """

        Returns:
            int:
        """
        return self["_attr_type"]

    @property
    def value(self):
        """

        Returns:
            Any:
        """
        return self["_attr_value"]

    @classmethod
    def from_attribute(cls, attr, value):
        """

        Args:
            attr (CDmAttribute):
            value (Any):
        """
        return cls(attr.GetName(), attr.GetType(), value)

    @classmethod
    def void_attribute(cls, attr):
        """

        Args:
            attr (CDmAttribute):
        """
        value = [None] * attr.count() if attr.IsArray() else None
        return cls(attr, value)

    @classmethod
    def from_serialized_dict(cls, dict_value):
        """

        Args:
            dict_value (dict):

        Returns:
            SerializedAttribute:
        """
        expected_keys = ["_attr_name", "_attr_type", "_attr_value"]
        dict_keys = dict_value.keys()
        if not all([key in dict_keys for key in expected_keys]):
            raise ValueError("dict value is not serialized attribute")
        return cls(
            dict_value["_attr_name"],
            dict_value["_attr_type"],
            dict_value["_attr_value"]
        )
