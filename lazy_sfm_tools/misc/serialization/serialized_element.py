from vs.datamodel import CDmElement
from lazy_sfm_tools.misc.serialization.serialized_attribute import SerializedAttribute


class SerializedElement(dict):
    def __init__(self, element_id, element_type, attrs=None, is_ref=False):
        """

        Args:
            element_id (str|unicode):
            element_type (str|unicode):
            attrs (list[SerializedAttribute]):
            is_ref (bool):
        """
        super(SerializedElement, self).__init__()
        if attrs is None:
            attrs = []
        self["_id"] = str(element_id)
        self["_type"] = str(element_type)
        self["_attrs"] = attrs
        self["_ref"] = is_ref

    @classmethod
    def from_element(cls, element, attrs=None, is_ref=False):
        """

        Args:
            element (CDmElement):
            attrs (list[SerializedAttribute]):
            is_ref (bool)
        """
        if attrs is None:
            attrs = []
        return cls(str(element.GetId()), element.GetTypeString(), attrs, is_ref)

    @property
    def id(self):
        """

        Returns:
            str:
        """
        return self["_id"]

    @property
    def type(self):
        """

        Returns:
            str:
        """
        return self["_type"]

    @property
    def attrs(self):
        """

        Returns:
            list[SerializedAttribute]:
        """
        return self["_attrs"]

    @property
    def is_ref(self):
        """

        Returns:
            bool:
        """
        return self["_ref"]
