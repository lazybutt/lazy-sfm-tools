import vsUtils
from vs.datamodel import CDmElement
from vs.datamodel import CDmAttribute
from vs.mathlib import Vector2D, Vector, Vector4D, Quaternion, QAngle
from vs.tier1 import DmeTime_t
from vs.misc import Color

from lazy_sfm_tools.commons.enums import AttributeTypes
from lazy_sfm_tools.misc.serialization.serialized_attribute import SerializedAttribute
from lazy_sfm_tools.misc.serialization.serialized_element import SerializedElement


class DataModelSerializer(object):
    @classmethod
    def serialize_element(cls, element, already_read_ids=None):
        """

        Args:
            already_read_ids (list[str]):
            element (CDmElement|Any|None):

        Returns:
            SerializedElement:
        """
        if already_read_ids is None:
            already_read_ids = []
        if element is None:
            return {"_type": "unknown"}
        if not hasattr(element, "AddAttribute"):
            raise TypeError("Object is not element, received type {}".format(type(element)))
        element_id = str(element.GetId())
        if element_id in already_read_ids:
            return SerializedElement(element_id, element.GetTypeString(), is_ref=True)
        already_read_ids.append(element_id)
        attributes = []
        attrs = vsUtils.iterAttrs(element)  # type: list[CDmAttribute]
        for attr in attrs:
            attributes.append(cls.serialize_attribute(attr, already_read_ids))

        return SerializedElement.from_element(element, attributes)

    @classmethod
    def serialize_attribute(cls, attr, already_read_ids=None):
        """

        Args:
            already_read_ids (list[str]):
            attr (CDmAttribute):

        Returns:
            SerializedAttribute:
        """
        if already_read_ids is None:
            already_read_ids = []
        attr_type = AttributeTypes.of(attr.GetType())
        if attr_type in [AttributeTypes.ELEMENT, AttributeTypes.ELEMENT_ARRAY]:
            return cls._proceed_attribute(attr, lambda item: cls.serialize_element(item, already_read_ids))
        elif attr_type in AttributeTypes.elementary_types() + AttributeTypes.elementary_array_types():
            return cls._proceed_attribute(attr)
        elif attr_type in AttributeTypes.complex_types() + AttributeTypes.complex_array_types():
            return cls._proceed_attribute(attr, cls.serialize_complex)
        elif attr_type in [AttributeTypes.VOID, AttributeTypes.VOID_ARRAY, AttributeTypes.UNKNOWN]:
            return SerializedAttribute.void_attribute(attr)
        else:
            raise TypeError("Attr {}.Not Implemented for type {}({})".format(attr.GetName(), attr_type, AttributeTypes.of(attr_type).name))

    @staticmethod
    def serialize_complex(value):
        """

        Args:
            value (CDmAttribute|Any):

        Returns:

        """
        def join_array_to_string(array):
            str_array = [str(item) for item in array]
            return " ".join(str_array)

        if isinstance(value, Vector2D):
            return join_array_to_string([value.x, value.y])
        elif isinstance(value, (Vector, QAngle)):
            return join_array_to_string([value.x, value.y, value.z])
        elif isinstance(value, (Vector4D, Quaternion)):
            return join_array_to_string([value.x, value.y, value.z, value.w])
        elif isinstance(value, DmeTime_t):
            return join_array_to_string([value.GetTenthsOfMS()])
        elif isinstance(value, Color):
            return join_array_to_string([value.r(), value.g(), value.b(), value.a()])
        else:
            raise TypeError("Not Implemented for type {}".format(type(value)))

    @staticmethod
    def _proceed_attribute(attr, serialize_func=lambda item_value: item_value):
        """

        Args:
            attr (CDmAttribute):
            serialize_func (function):

        Returns:
            SerializedAttribute:
        """
        if attr.IsArray():
            value = [serialize_func(item) for item in attr.GetValue()]
        else:
            value = serialize_func(attr.GetValue())
        return SerializedAttribute.from_attribute(attr, value)
