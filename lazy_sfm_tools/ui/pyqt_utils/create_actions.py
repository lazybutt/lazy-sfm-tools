from PySide import QtGui

class CreateWidgetActions(object):
    @classmethod
    def h_layout(cls, *widgets):
        return cls._create_basic_layout(QtGui.QHBoxLayout, widgets)

    @classmethod
    def v_layout(cls, *widgets):
        return cls._create_basic_layout(QtGui.QVBoxLayout, widgets)

    @staticmethod
    def _create_basic_layout(layout_class, widgets):
        layout = layout_class()
        for widget in widgets:
            layout.addWidget(widget)
        return layout
