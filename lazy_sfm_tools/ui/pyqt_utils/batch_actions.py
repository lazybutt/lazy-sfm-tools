class WidgetBatchActions(object):
    @staticmethod
    def show(*widgets):
        for widget in widgets:
            widget.show()

    @staticmethod
    def hide(*widgets):
        for widget in widgets:
            widget.hide()

    @staticmethod
    def set_font(font, *widgets):
        for widget in widgets:
            widget.setFont(font)

    @staticmethod
    def set_alignment(alignment, *widgets):
        for widget in widgets:
            widget.setAlignment(alignment)

    @staticmethod
    def set_enabled(enabled, *widgets):
        for widget in widgets:
            widget.setEnabled(enabled)

    @staticmethod
    def set_disabled(disabled, *widgets):
        for widget in widgets:
            widget.setDisabled(disabled)

    @staticmethod
    def set_stylesheet(stylesheet, *widgets):
        for widget in widgets:
            widget.setStyleSheet(stylesheet)

    @staticmethod
    def set_fixed_height(height, *widgets):
        for widget in widgets:
            widget.setFixedHeight(height)

    @staticmethod
    def set_fixed_width(height, *widgets):
        for widget in widgets:
            widget.setFixedWidth(height)

    @staticmethod
    def set_height(height, *widgets):
        for widget in widgets:
            widget.setHeight(height)

    @staticmethod
    def set_width(width, *widgets):
        for widget in widgets:
            widget.setWidth(width)
