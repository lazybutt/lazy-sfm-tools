from PySide import QtGui


class ComboBoxAutoCompletion(QtGui.QComboBox):
    """Custom ComboBox widget with autocompletion feature."""

    def __init__(self, adding=False, parent=None):
        """Constructor custom ComboBox widget with autocompletion feature.

        Args:
            adding (bool): enable adding the entered value if autocomplete is not selected
                and the entered value is not on the list.
            parent (QWidget): parent widget.
        """
        super(ComboBoxAutoCompletion, self).__init__(parent)
        from PySide import QtCore

        self.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.setEditable(True)
        self._completer = QtGui.QCompleter(self)
        self._completer.setCompletionMode(QtGui.QCompleter.UnfilteredPopupCompletion)
        self._pFilterModel = QtGui.QSortFilterProxyModel(self)
        self._pFilterModel.setFilterCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self._completer.setPopup(self._completer.popup())
        self.setCompleter(self._completer)
        self._adding = adding
        self.lineEdit().textEdited[unicode].connect(self._pFilterModel.setFilterFixedString)
        self._completer.activated.connect(self._setTextIfCompleterIsClicked)
        self._currentIndex = self.currentIndex()

    def alphabet_sort(self):
        """Sort values by alphabet."""
        proxy = QtGui.QSortFilterProxyModel()
        proxy.setSourceModel(self.model())
        self.model().setParent(proxy)
        self.setModel(proxy)
        self.model().sort(0)

    def focusOutEvent(self, e):
        """Overridden focus out event."""
        QtGui.QComboBox.focusOutEvent(self, e)
        if not self._adding:
            array = [str(self.itemText(i)) for i in range(self.count())]
            if self.currentText() in array:
                self._currentIndex = self.currentIndex()
            else:
                self.setCurrentIndex(self._currentIndex)

    def indexChanged(self):
        """Overridden index changed event."""
        array = [str(self.itemText(i)) for i in range(self.count())]
        if self.currentText() in array:
            self._currentIndex = self.currentIndex()

    def setModel(self, model):
        """Overridden set model."""
        super(ComboBoxAutoCompletion, self).setModel(model)
        self._pFilterModel.setSourceModel(model)
        self._completer.setModel(self._pFilterModel)

    def setModelColumn(self, column):
        """Overridden set model column."""
        self._completer.setCompletionColumn(column)
        self._pFilterModel.setFilterKeyColumn(column)
        super(ComboBoxAutoCompletion, self).setModelColumn(column)

    def addItems(self, array):
        """Overridden add items method."""
        model = QtGui.QStandardItemModel()
        for i, word in enumerate(array):
            item = QtGui.QStandardItem(word)
            model.setItem(i, 0, item)
        self.setModel(model)
        self._currentIndex = self.currentIndex()

    def findAndSet(self, text):
        idx = self.findText(text)
        if idx != -1:
            self.setCurrentIndex(idx)
            return True
        return False

    def _setTextIfCompleterIsClicked(self, text):
        if text:
            index = self.findText(text)
            self.setCurrentIndex(index)
            self._currentIndex = self.currentIndex()
