from enum import Enum
from vs.movieobjects import CDmeAnimationSet
from lazy_sfm_tools.ui.icons.base_icons import BaseIcons
from PySide import QtGui


class AnimsetIcons(Enum):
    LIGHT = QtGui.QIcon(r'platform\tools\images\sfm\icon_light_item.png')
    CAMERA = QtGui.QIcon(r'platform\tools\images\sfm\icon_camera_item.png')
    GAMEMODEL = QtGui.QIcon(r'platform\tools\images\sfm\icon_model_item.png')
    PARTICLE = QtGui.QIcon(r'platform\tools\images\sfm\icon_particle_item.png')
    UNKNOWN = BaseIcons.UNKNOWN

    @classmethod
    def of(cls, element):
        """Define icon by animation set.

        Args:
            element (CDmeAnimationSet): animation set.

        Returns:
            AnimsetIcons: defined icon.
        """
        if element.HasAttribute("gameModel"):
            return cls.GAMEMODEL
        elif element.HasAttribute("light"):
            return cls.LIGHT
        elif element.HasAttribute("camera"):
            return cls.CAMERA
        elif element.HasAttribute("particle"):
            return cls.PARTICLE
        else:
            return cls.UNKNOWN
