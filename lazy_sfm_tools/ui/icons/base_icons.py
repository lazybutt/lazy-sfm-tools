from PySide import QtGui
from enum import Enum


class BaseIcons(Enum):
    UNKNOWN = QtGui.QIcon(r'question.png')

    def of(self, element):
        """Define icon by element.

        Args:
            element (Any): element.

        Returns:
            BaseIcons: defined icon.
        """
        return self.UNKNOWN
