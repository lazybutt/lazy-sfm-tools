from PySide import QtGui
from PySide import QtCore

class CheckBoxWithLabel(QtGui.QWidget):
    def __init__(self, text, *args, **kwargs):
        super(CheckBoxWithLabel, self).__init__(*args, **kwargs)
        self._label = QtGui.QLabel(text)
        self._check_box = QtGui.QCheckBox()
        layout = QtGui.QHBoxLayout()
        layout.addWidget(self._check_box)
        layout.addWidget(self._label)
        layout.setAlignment(QtCore.Qt.AlignLeft)
        self.setLayout(layout)

    @property
    def label(self):
        return self._label

    @property
    def check_box(self):
        return self._check_box
