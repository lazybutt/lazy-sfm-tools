from PySide import QtGui

class VerticalLabel(QtGui.QLabel):
    """Custom vertical oriented label."""
    def __init__(self, text, *args, **kwargs):
        """Constructor for VerticalLabel.

        Args:
            text: label text.
            *args: args for QLabel.
            **kwargs: kwargs for QLabel.
        """
        super(VerticalLabel, self).__init__(*args, **kwargs)
        self._text = text

    def paintEvent(self, event):
        """Overridden paint event."""
        from PySide import QtCore

        painter = QtGui.QPainter(self)
        painter.setPen(QtCore.Qt.black)
        painter.translate(20, 100)
        painter.rotate(-90)
        painter.drawText(0, 0, self._text)
        painter.end()
