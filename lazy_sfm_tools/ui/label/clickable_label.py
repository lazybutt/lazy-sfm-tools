from PySide import QtGui
from PySide import QtCore

class ClickableLabel(QtGui.QLabel):
    """Custom clickable label."""
    clicked = QtCore.Signal()

    def __init__(self, **kwargs):
        """Constructor for ClickableLabel.

        Keyword Args:
            text (str|unicode): label text.
            func (function): function to call after click on label.
        """
        text = kwargs["text"] if "text" in kwargs else None
        if "custom_func" in kwargs:
            self._custom_func = kwargs["custom_func"]
            if not callable(self._custom_func):
                raise ValueError("custom_func must be callable")
        else:
            self._custom_func = None
        super(ClickableLabel, self).__init__(**kwargs)
        if text:
            self.setText(text)

    def mousePressEvent(self, event):
        """Overridden mouse press event."""
        self._custom_func() if self._custom_func else self.clicked.emit()
