import uuid

from PySide import QtGui
from PySide import QtCore

from lazy_sfm_tools.selectors import ShotSelector
from lazy_sfm_tools.ui.icons import AnimsetIcons
from lazy_sfm_tools.ui.utlis import is_same_element
from lazy_sfm_tools.utils.value_buffer import BufferByID


class AnimationSetTreeWidget(QtGui.QTreeWidget):
    cache_clear_key = uuid.uuid4()

    def __init__(self, *args, **kwargs):
        super(AnimationSetTreeWidget, self).__init__(*args, **kwargs)

    def build_from_shot(self, shot, without_root_group=True):
        self._get_items_from_group(self, shot.scene, shot.animationSets, without_root_group)
        self.expandAll()

    def build_from_dag(self, dag, without_root_group=True):
        animation_sets = self._get_all_anim_sets()
        self._get_items_from_group(self, dag, animation_sets, without_root_group)
        self.expandAll()

    def get_items(self):
        for item_idx in range(0, self.topLevelItemCount()):
            pass

    @staticmethod
    def _get_all_anim_sets():
        anim_sets = []
        for shot in ShotSelector.shots:
            anim_sets.extend(shot.animationSets)
        return anim_sets

    @classmethod
    def _is_group(cls, dag_group):
        if not dag_group.HasAttribute("children"):
            return False
        if cls._is_animset_related_dag(dag_group):
            return False
        if len(dag_group.children) == 1 and cls._is_animset_related_dag(dag_group.children[0]):
            return False
        return True

    @staticmethod
    def _is_animset_related_dag(dag):
        return dag.GetTypeString() in ["DmeProjectedLight", "DmeGameModel", "DmeCamera", "DmeRig"]

    def _get_dag_as_item(self, dag, anim_sets, root):
        dag = dag if self._is_animset_related_dag(dag) else dag.children[0]
        found_anim_set = None
        for anim_set in anim_sets:
            if dag.GetTypeString() == "DmeCamera" \
                    and anim_set.HasAttribute("camera") \
                    and is_same_element(anim_set.camera, dag):
                found_anim_set = anim_set
                break
            elif dag.GetTypeString() == "DmeGameModel" \
                    and anim_set.HasAttribute("gameModel") \
                    and is_same_element(anim_set.gameModel, dag):
                found_anim_set = anim_set
                break
            elif dag.GetTypeString() == "DmeProjectedLight" \
                    and anim_set.HasAttribute("light") \
                    and is_same_element(anim_set.light, dag):
                found_anim_set = anim_set
                break
        if not found_anim_set:
            return None
        item = QtGui.QTreeWidgetItem(root, [found_anim_set.GetName()])
        item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)
        item.setCheckState(0, QtCore.Qt.Unchecked)
        item.setIcon(0, AnimsetIcons.of(found_anim_set).value)
        item.setData(0, QtCore.Qt.UserRole, BufferByID.add(item))
        return item

    def _is_empty(self, group):
        for child in group.children:
            if self._is_group(child):
                return self._is_empty(child)
            else:
                return False
        return True

    def _get_items_from_group(self, root, dag_group, animation_sets, without_root_group=False):
        if self._is_group(dag_group):
            if not self._is_empty(dag_group):
                if not without_root_group:
                    current_group = QtGui.QTreeWidgetItem(root, [dag_group.GetName()])
                    current_group.setFlags(current_group.flags() | QtCore.Qt.ItemIsTristate | QtCore.Qt.ItemIsUserCheckable)
                    current_group.setCheckState(0, QtCore.Qt.Unchecked)
                else:
                    current_group = self
                for child in dag_group.children:
                    if child is None:
                        continue
                    if self._is_group(child):
                        self._get_items_from_group(current_group, child, animation_sets)
                    else:
                        self._get_dag_as_item(child, animation_sets, current_group)
                return current_group
        else:
            return self._get_dag_as_item(dag_group, animation_sets, root)
