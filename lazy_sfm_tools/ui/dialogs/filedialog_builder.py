import os.path
from lazy_sfm_tools.files.file_structure import GamePath

class FileDialogBuilder(object):
    """File dialog builder."""

    @classmethod
    def save_file(cls):
        """Save file dialog.

        Returns:
            _SaveFileSelectBuilder: save file dialog builder.
        """
        return _SaveFileSelectBuilder()

    @classmethod
    def open_file(cls):
        """Open file dialog.

        Returns:
            _OpenFileSelectBuilder: select existed file dialog builder.
        """
        return _OpenFileSelectBuilder()

    @classmethod
    def existing_folder(cls):
        """Select folder dialog.

        Returns:
            function: select existed folder dialog builder.
        """
        return _FolderSelectBuilder()

class _FolderSelectBuilder(object):
    def __init__(self):
        """Constructor for _FolderSelectBuilder"""
        self._parent = None
        self._caption = None
        self._start_dir_path = None
        self._options = []

    def with_parent(self, parent):
        """Set parent widget.

        Args:
            parent: parent widget.

        Returns:
            _FolderSelectBuilder: self.
        """
        self._parent = parent
        return self

    def with_caption(self, caption):
        """Set caption.

        Args:
            caption: caption to set.

        Returns:
            _FolderSelectBuilder: self.
        """
        self._caption = str(caption)
        return self

    def with_start_dir(self, start_dir_path):
        """Set start directory.

        Args:
            start_dir_path: start directory that will be opened in the dialog box.

        Returns:
            _FolderSelectBuilder: self.
        """
        if isinstance(start_dir_path, GamePath):
            full_path = start_dir_path.absolute_path
            self._start_dir_path = full_path \
                if start_dir_path.is_folder \
                else os.path.dirname(full_path)
        elif isinstance(start_dir_path, (str, unicode)):
            self._start_dir_path = str(start_dir_path)
        else:
            raise TypeError()  # todo
        return self

    def with_options(self, *options):
        """Set options.

        Args:
            options: options for dialog.

        Returns:
            _FolderSelectBuilder: self.
        """
        self._options = options

    def add_option(self, option):
        """Set single option.

        Args:
            option: option to add for dialog.

        Returns:
            _FolderSelectBuilder: self.
        """
        if self._options is None:
            self._options = []
        self._options.append(option)

    @property
    def _params_as_dict(self):
        params = {"parent": self._parent}
        if self._caption is not None:
            params["caption"] = self._caption
        if self._start_dir_path is not None:
            params["dir"] = self._start_dir_path
        if self._options is not None and self._options != []:
            params["options"] = self._options

        return params

    @property
    def _file_dialog_func(self):
        from PySide import QtGui
        return QtGui.QFileDialog.getExistingDirectory

    def build(self):
        params = self._params_as_dict
        return lambda: self._file_dialog_func(**params)


class _SaveFileSelectBuilder(_FolderSelectBuilder):
    def __init__(self):
        super(_SaveFileSelectBuilder, self).__init__()
        self._filter = None

    def with_extension_filter(self, extensions, caption="Files"):
        """Set extension filter.

        Args:
            extensions (str|unicode|list[str|unicode]): list of allowed extensions.
            caption (str|unicode): caption for extensions in dialog window (optional).

        Returns:
            _SaveFileSelectBuilder: self.
        """
        if not isinstance(extensions, (list, tuple)):
            extensions = [extensions]
        extensions_str = " ".join(["*." + extension.lstrip(".") for extension in extensions])
        filter_str = "{} ({})".format(caption, extensions_str)
        self._filter = filter_str
        return self

    def with_file_filter(self, filename, caption="File"):
        """Set filename filter.

        Args:
            filename: filename to select.
            caption: caption for filename.

        Returns:
            _SaveFileSelectBuilder: self.
        """
        filter_str = "{} ({})".format(caption, filename)
        self._filter = filter_str
        return self

    def _params_as_dict(self):
        params = super(_SaveFileSelectBuilder, self)._params_as_dict
        if self._filter is not None:
            params["selectedFilter"] = self._filter
        return params

    def _file_dialog_func(self):
        from PySide import QtGui
        return QtGui.QFileDialog.getSaveFileName

class _OpenFileSelectBuilder(_SaveFileSelectBuilder):
    def __init__(self):
        super(_OpenFileSelectBuilder, self).__init__()
        self._allow_multi_files = False

    def enable_multi_select(self):
        """Enable multi file select in dialog.

        Returns:
            _OpenFileSelectBuilder: self.
        """
        self._allow_multi_files = True
        return self

    def _file_dialog_func(self):
        from PySide import QtGui
        if self._allow_multi_files:
            return QtGui.QFileDialog.getOpenFileNames
        else:
            return QtGui.QFileDialog.getOpenFileName
