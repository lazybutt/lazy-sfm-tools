from lazy_sfm_tools.ui.line_edits.common import AbstractMonoField


class ColorField(AbstractMonoField):
    OUTPUT_AS_HEX = "HEX"
    OUTPUT_AS_RGB_ARRAY = "RGB_ARRAY"
    OUTPUT_AS_RGB_STRING = "RGB_STRING"

    def __init__(self, **kwargs):
        super(ColorField, self).__init__(**kwargs)
        from PySide import QtGui
        from PySide import QtCore
        
        self._current_color = QtGui.QLabel()
        self._current_color.setStyleSheet('border: 2px; border-color: white')
        palette = self._current_color.palette()
        palette.setColor(self._current_color.foregroundRole(), QtCore.Qt.red)
        self._current_color.setPalette(palette)
        self._current_color.setAlignment(QtCore.Qt.AlignCenter)
        if not self._disable_label:
            self._layout.addWidget(self._label)
        self._layout.addWidget(self._line_edit)
        self._layout.addWidget(self._current_color)
        if not self._disable_restore:
            self._layout.addWidget(self._restore_button)
        self._output_format = kwargs.pop('output_format') if 'output_format' in kwargs else self.OUTPUT_AS_HEX
        self._line_edit.textEdited.connect(lambda x: self._value_processing(self._line_edit.text()))
        self.setLayout(self._layout)
        self._setup_init_sizes(kwargs)
        if 'value' in kwargs:
            self.value = kwargs.pop('value')

    def set_field_size(self, width, height):
        line_edit_width = width - height - self._layout.spacing() * 1.5
        if not self._disable_restore:
            line_edit_width += self._layout.spacing() / 2
        self._line_edit.setFixedHeight(height)
        self._line_edit.setFixedWidth(line_edit_width)
        self._current_color.setFixedSize(height, height)
        self._restore_button.setFixedSize(height, height)

    def _get_rgb(self):
        if self._current_mode == 'RGB':
            return self._line_edit.text().replace(' ', '').split(',') if ',' in self._value else self._value.split(
                ' ')
        elif self._current_mode == 'HEX':
            return self._hex_to_rgb(self._line_edit.text())
        else:
            return None

    def _get_hex(self):
        if self._current_mode == 'RGB':
            return self._rgb_to_hex(self._line_edit.text())
        elif self._current_mode == 'HEX':
            return self._line_edit.text()
        else:
            return None

    @property
    def value(self):
        if self._output_format == self.OUTPUT_AS_HEX:
            return self._get_hex()
        elif self._output_format == self.OUTPUT_AS_RGB_STRING:
            return self._get_rgb()
        elif self._output_format == self.OUTPUT_AS_RGB_ARRAY:
            value = self._get_rgb()
            return ' '.join(value) if value is not None else None
        else:
            raise NotImplementedError()

    @value.setter
    def value(self, value):
        if value is not None:
            self._value_processing(value)
            self._value_to_restore = self._line_edit.text()
            self._restore_button.setEnabled(True)

    @staticmethod
    def _hex_to_rgb(hex_value):
        hex_value = hex_value.lstrip('#')
        if len(hex_value) != 6:
            return None
        lv = len(hex_value)
        return list(int(hex_value[i:i + lv // 3], 16) for i in range(0, lv, lv // 3))

    @staticmethod
    def _rgb_to_hex(rgb):
        array = rgb.replace(' ', '').split(',') if ',' in rgb else rgb.split(' ')
        newarray = []
        for item in array:
            if item.isdigit():
                newarray.append(int(item))
            else:
                return None
        if len(newarray) == 3:
            return '#%02x%02x%02x' % tuple(newarray)
        else:
            return None

    @property
    def is_valid(self):
        if not self.is_filled:
            if self.is_mandatory:
                return False
        if self._current_color == 'Err':
            return False
        return True

    @property
    def is_filled(self):
        return self._line_edit.text().replace(' ', '') != ''

    def restore_value(self):
        if self._value_to_restore is not None:
            self._line_edit.setText(self._value_to_restore)
            self._value_processing(self._value_to_restore)

    def _value_processing(self, value=None):
        if value is not None and not str(value).isspace():
            self._line_edit.setText(' '.join(value) if type(value) == list else value)
            text_to_parse = ''
            available_characters = [' ', ',', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e',
                                   'f', 'A', 'B', 'C', 'D', 'E', 'F']
            for char in value:
                if char in available_characters:
                    text_to_parse += char
                elif char == '#' and '#' not in text_to_parse:
                    text_to_parse += char
            if len(text_to_parse) >= 4 and ',' not in text_to_parse and ' ' not in text_to_parse and '#' not in text_to_parse:
                text_to_parse = '#' + text_to_parse
            if len(text_to_parse) >= 8 and text_to_parse[0] == '#':
                text_to_parse = text_to_parse[0: 7]
            if value != text_to_parse:
                self._value = text_to_parse
                self._line_edit.setText(self._value)
            if '#' in text_to_parse:
                if self._hex_to_rgb(text_to_parse) is not None:
                    sheet = 'QLabel { background-color: ' + text_to_parse + '; border-radius: 5px }'
                    self._current_color.setStyleSheet(sheet)
                    self._current_color.setText('')
                    self._current_mode = 'HEX'
                    return None
            color = self._rgb_to_hex(text_to_parse)
            if color is not None:
                sheet = 'QLabel { background-color: ' + color + '; border-radius: 5px }'
                self._current_color.setStyleSheet(sheet)
                self._current_color.setText('')
                self._current_mode = 'RGB'
                return None
            self._current_color.setStyleSheet('QLabel { background-color: none; border-radius: 5px}')
            self._current_color.setText('Err')
            self._current_mode = None
