from lazy_sfm_tools.files.file_structure import GamePath
from lazy_sfm_tools.ui.line_edits.common import AbstractMonoField


class FileField(AbstractMonoField):
    def __init__(self, title, extensions, folder_root_source_id, category, **kwargs):
        super(FileField, self).__init__(**kwargs)
        from PySide import QtGui

        self._title = title
        self._extensions = extensions
        self._folder_root_source_id = folder_root_source_id
        self._value_path = None
        self._category = category
        self._browse_button = QtGui.QPushButton('...')
        if not self._disable_label:
            self._layout.addWidget(self._label)
        self._layout.addWidget(self._line_edit)
        self._layout.addWidget(self._browse_button)
        if not self._disable_restore:
            self._layout.addWidget(self._restore_button)
        self._line_edit.setEnabled(False)
        self._browse_button.setFixedWidth(20)
        self._line_edit.installEventFilter(self)
        self.setLayout(self._layout)
        self._setup_init_sizes(kwargs)
        self._browse_button.clicked.connect(self._select_file)
        if 'value' in kwargs:
            self.value = kwargs['value']

    def eventFilter(self, watched, event):
        from PySide import QtCore
        from PySide import QtGui

        if watched == self._line_edit and event.type() == QtCore.QEvent.Type.MouseButtonDblClick:
            self.open_folder()
        return QtGui.QWidget.eventFilter(self, watched, event)

    def open_folder(self):
        if self.is_path_valid:
            import os
            game_path = self.game_path
            path_to_open = game_path.absolute_path \
                if game_path.is_folder \
                else os.path.dirname(game_path.absolute_path)
            os.startfile(path_to_open)

    @property
    def game_path(self):
        if self.is_path_valid:
            return GamePath(self._value_path, self._category)
        return None

    def restore_value(self):
        if self._value_to_restore is not None:
            import os
            self._value_path = self._value_to_restore
            filename = os.path.basename(self._value_to_restore)
            self._line_edit.setText(filename)

    @property
    def value(self):
        return self._value_path

    @value.setter
    def value(self, value):
        if value is not None:
            import os
            self._value_path = value
            filename = os.path.basename(self._value_to_restore)
            self._line_edit.setText(filename)
            self._set_valid_visual()
            self._line_edit.setToolTip(value)
            self._value_to_restore = self._value_path
            self._restore_button.setEnabled(True)

    @property
    def is_valid(self):
        if self._value_path is None:
            return not self.is_mandatory
        return self.is_path_valid

    @property
    def is_path_valid(self):
        return GamePath(self._value_path, self._category).is_valid

    @property
    def is_filled(self):
        return self._line_edit.text().replace(' ', '') != ''

    def _set_valid_visual(self):
        self._line_edit.setStyleSheet('border-color: #232323' if self.is_path_valid else 'border-color: #990000')

    def set_field_size(self, width, height):
        line_edit_width = width - height - self._layout.spacing() * 1.5
        if not self._disable_restore:
            line_edit_width += self._layout.spacing() / 2
        self._line_edit.setFixedHeight(height)
        self._line_edit.setFixedWidth(line_edit_width)
        self._browse_button.setFixedSize(height, height)
        self._restore_button.setFixedSize(height, height)

    @property
    def _file_dialog(self):
        from lazy_sfm_tools.utils.value_buffer import BufferByID
        from lazy_sfm_tools.ui.dialogs.filedialog_builder import FileDialogBuilder

        return FileDialogBuilder\
            .open_file()\
            .with_parent(self.parent())\
            .with_caption(self._title)\
            .with_start_dir(BufferByID[self._folder_root_source_id])\
            .with_extension_filter(self._extensions)\
            .build()

    def _select_file(self):
        from lazy_sfm_tools.utils.value_buffer import BufferByID

        file_name = self._file_dialog()
        if file_name:
            import os
            game_path = GamePath(file_name, self._category)
            BufferByID[self._folder_root_source_id] = game_path.absolute_path \
                if game_path.is_folder \
                else os.path.dirname(game_path.absolute_path)
            self._value_path = game_path.mod_relative_path
            self._line_edit.setStyleSheet('border-color: #990000')
            self._set_valid_visual()
