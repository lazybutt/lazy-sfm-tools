from lazy_sfm_tools.ui.line_edits.common import AbstractMonoField


class IntField(AbstractMonoField):
    def __init__(self, **kwargs):
        super(IntField, self).__init__(**kwargs)
        if not self._disable_label:
            self._layout.addWidget(self._label)
        self._layout.addWidget(self._line_edit)
        if not self._disable_restore:
            self._layout.addWidget(self._restore_button)
        self._line_edit.textEdited.connect(lambda x: self._value_processing(self._line_edit.text()))
        self.setLayout(self._layout)
        self._setup_init_sizes(kwargs)
        if 'value' in kwargs:
            self.value = kwargs['value']

    @property
    def value(self):
        text = self._line_edit.text()
        return int(text) if text == '' else None

    @value.setter
    def value(self, value):
        if value is not None:
            self._value_processing(value)
            self._value_to_restore = self._line_edit.text()
            self._restore_button.setEnabled(True)

    @property
    def is_valid(self):
        if not self.is_filled:
            if self.is_mandatory:
                return False
        return True

    @property
    def is_filled(self):
        return self._line_edit.text().replace(' ', '') != ''

    def restore_value(self):
        if self._value_to_restore is not None:
            self._line_edit.setText(self._value_to_restore)

    def _value_processing(self, value=None):
        if value is not None and not str(value).isspace():
            newText = ''
            for char in str(value):
                if char.isdigit():
                    newText += char
            if self._line_edit.text() != newText:
                self._line_edit.setText(newText)
