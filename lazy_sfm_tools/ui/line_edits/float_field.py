from lazy_sfm_tools.ui.line_edits import IntField


class FloatField(IntField):
    def __init__(self, **kwargs):
        super(FloatField, self).__init__(**kwargs)

    def _value_processing(self, value=None):
        if value is not None and not str(value).isspace():
            newText = ''
            hasSign = False
            for char in str(value):
                if char.isdigit():
                    newText += char
                elif char == '.' or char == ',':
                    if '.' not in newText:
                        newText += '.'
                elif char == '-':
                    if not hasSign:
                        newText += '-'
                        hasSign = True
            if len(newText) == 2:
                if newText[0] == '0' and newText[1].isdigit():
                    newText = '0.' + newText[1]
            self._value = newText
            if self._line_edit.text() != newText:
                self._line_edit.setText(newText)
