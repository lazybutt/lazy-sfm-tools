from lazy_sfm_tools.ui.line_edits.common import AbstractField


class VectorSplitField(AbstractField):
    def __init__(self, label_width=120, field_width=150, height=25, disable_label=False, disable_restore=False, dimensions=3, **kwargs):
        super(VectorSplitField, self).__init__(**kwargs)
        from PySide import QtGui
        from PySide import QtCore
        
        self._layout = QtGui.QHBoxLayout()
        self._layout.setSpacing(5)
        self._args = {
            'label_width': label_width,
            'field_width': field_width,
            'height': height,
            'disable_label': disable_label,
            'disable_restore': disable_restore
        }
        self._vector_items = []
        if not disable_label:
            self._label = QtGui.QLabel(kwargs['label'] if 'label' in kwargs else '')
            self._label.setFixedWidth(label_width)
            self._label.setFixedHeight(height)
            self._layout.addWidget(self._label)
        oneFieldWidth = round((field_width - (5.0 * (dimensions - 1))) / dimensions)
        for item in range(dimensions):
            field = QtGui.QLineEdit()
            field.setFixedWidth(oneFieldWidth)
            field.setFixedHeight(height)
            field.textEdited.connect(self._value_processing)
            self._layout.addWidget(field)
            self._vector_items.append(field)
            self._layout.setAlignment(field, QtCore.Qt.AlignLeft)
        if not disable_restore:
            self._restore_button = QtGui.QPushButton('R')
            self._restore_button.setFixedSize(height, height)
            self._restore_button.setEnabled(False)
            self._restore_button.clicked.connect(self.restore_value)
            self._layout.addWidget(self._restore_button)
        self._layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self._layout)
        self._layout.setAlignment(QtCore.Qt.AlignLeft)
        if 'value' in kwargs:
            self.value = kwargs['value']

    @property
    def value(self):
        return [field.value for field in self._vector_items]

    @value.setter
    def value(self, value):
        if value is not None:
            if isinstance(value, (list, tuple)):
                for idx, field in enumerate(self._vector_items):
                    field.setText(str(value[idx]))
                    field.textEdited.emit(field.text())
            elif isinstance(value, dict):
                matching_field = {'x': 0, 'y': 1, 'z': 2, 'w': 3}
                for key, field_value in value.items():
                    if key in matching_field.keys():
                        field_idx = matching_field[key]
                        if len(self._vector_items) > field_idx:
                            field = self._vector_items[field_idx]
                            field.setText(str(field_value))
                            field.textEdited.emit(field.text())
            else:
                return
            self._value_to_restore = value
            self._restore_button.setEnabled(True)

    @property
    def is_valid(self):
        return all(field.is_valid for field in self._vector_items)

    @property
    def is_filled(self):
        return all(field.is_filled for field in self._vector_items)

    def restore_value(self):
        if self._value_to_restore is not None:
            self.value = self._value_to_restore

    def set_label_width(self, width):
        pass

    def set_field_size(self, width, height):
        pass

    def _value_processing(self, value):
        target = self.sender()
        if value is not None and not str(value).isspace():
            if len(value) > 0:
                if value[-1] == ' ':
                    target.setText(value[:-1])
                    idx = self._vector_items.index(target)
                    if len(self._vector_items) - 1 > idx >= 0:
                        self._vector_items[idx+1].setFocus()
            has_sign = False
            new_text = ''
            for char in str(value):
                if char.isdigit():
                    new_text += char
                elif char == '.' or char == ',':
                    if '.' not in new_text:
                        new_text += '.'
                elif char == '-':
                    if not has_sign:
                        new_text += '-'
                        has_sign = True
            if len(new_text) == 2:
                if new_text[0] == '0' and new_text[1].isdigit():
                    new_text = '0.' + new_text[1]
            if target.text() != new_text:
                target.setText(new_text)
