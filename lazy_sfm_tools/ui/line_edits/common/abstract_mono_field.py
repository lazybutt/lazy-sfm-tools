import abc
from lazy_sfm_tools.ui.line_edits.common.abstract_field import AbstractField


class AbstractMonoField(AbstractField):
    """Basic abstract class for mono input fields"""
    __metadata__ = abc.ABCMeta

    def __init__(self, **kwargs):
        """

        Args:
            **kwargs:
        """
        super(AbstractMonoField, self).__init__(**kwargs)
        from lazy_sfm_tools.utils.kwargs_tools import KwargsReader
        from PySide import QtCore
        from PySide import QtGui

        label_text = KwargsReader(kwargs).pop_or_default('label', '')
        self._layout = QtGui.QHBoxLayout()
        self._label = QtGui.QLabel(label_text)
        self._line_edit = QtGui.QLineEdit()
        self._restore_button = QtGui.QPushButton('R')
        self._restore_button.setEnabled(False)
        self._restore_button.clicked.connect(self.restore_value)
        self._layout.setAlignment(QtCore.Qt.AlignLeft)
        self._layout.setContentsMargins(0, 0, 0, 0)

    def _setup_init_sizes(self, kwargs):
        self.adjustSize()
        self.set_label_width(kwargs.pop('labelWidth') if 'labelWidth' in kwargs else 120)
        self.set_field_size(kwargs.pop('fieldWidth') if 'fieldWidth' in kwargs else 150,
                            kwargs.pop('height') if 'height' in kwargs else 25)

    def set_label_width(self, width):
        self._label.setFixedWidth(width)

    def set_field_size(self, width, height):
        self._line_edit.setFixedWidth(width)
        self._line_edit.setFixedHeight(height)
        self._restore_button.setFixedWidth(height)
        self._restore_button.setFixedHeight(height)