import abc
from abc import ABCMeta
from PySide import QtGui


class AbstractField(QtGui.QWidget):
    """Basic abstract class for input fields"""
    __metadata__ = ABCMeta

    def __init__(self, **kw):
        """Constructor for AbstractField.

        Keyword Args:
            mandatory (bool): fill field is required (False by default).
            disableRestore (bool): disable restore button (False by default).
            disableLabel (bool): disable label text (False by default).
            qt_args (list): args for QWidget.
            qt_kwargs (dict): kwargs for QWidget.
        """
        qt_args = kw['qt_args'] if 'qt_args' in kw else []
        qt_kwargs = kw['qt_kwargs'] if 'qt_kwargs' in kw else {}
        super(AbstractField, self).__init__(*qt_args, **qt_kwargs)
        self._mandatory = kw['mandatory'] if 'mandatory' in kw else False
        self._disable_restore = kw['disableRestore'] if 'disableRestore' in kw else False
        self._disable_label = kw['disableLabel'] if 'disableLabel' in kw else False
        self._value_to_restore = None

    @property
    @abc.abstractmethod
    def value(self):
        """Get field value.

        Returns:
            Any: stored value.
        """
        pass

    @value.setter
    @abc.abstractmethod
    def value(self, value):
        """Set field value.

        Args:
            value (Any): value to set.
        """
        pass

    @property
    @abc.abstractmethod
    def is_valid(self):
        """Check if value in field is valid.

        Returns:
            bool: true if value is valid, false if not.
        """
        pass

    @property
    @abc.abstractmethod
    def is_filled(self):
        """Check if value in field is filled.

        Returns:
            bool: true if value is filled, false if not.
        """
        pass

    @property
    def is_mandatory(self):
        """Receive mandatory of field.

        Returns:
            bool: field mandatory.
        """
        return self._mandatory

    @abc.abstractmethod
    def restore_value(self):
        """Restore value to init value."""
        pass

    @abc.abstractmethod
    def set_label_width(self, width):
        """Set label width.

        Args:
            width: label width to set.
        """
        pass

    @abc.abstractmethod
    def set_field_size(self, width, height):
        """Set label width.

        Args:
            width: field width to set.
            height: field height to set.
        """
        pass
