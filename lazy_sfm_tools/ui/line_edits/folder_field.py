from lazy_sfm_tools.ui.line_edits import FileField


class FolderField(FileField):
    def __init__(self, title, folder_root_source_id, category, **kwargs):
        super(FolderField, self).__init__(title, None, folder_root_source_id, category, **kwargs)

    @property
    def _file_dialog(self):
        from lazy_sfm_tools.utils.value_buffer import BufferByID
        from lazy_sfm_tools.ui.dialogs.filedialog_builder import FileDialogBuilder

        return FileDialogBuilder\
            .existing_folder()\
            .with_parent(self.parent())\
            .with_caption(self._title)\
            .with_start_dir(BufferByID[self._folder_root_source_id])\
            .build()
