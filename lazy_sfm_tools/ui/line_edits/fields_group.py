from lazy_sfm_tools.ui.line_edits.common import AbstractField


class FieldsGroup(AbstractField):
    def __init__(self, labelWidth=120, fieldWidth=150, height=25, disableLabel=False, disableRestore=False,
                 **kwargs):
        super(FieldsGroup, self).__init__(**kwargs)
        from PySide import QtGui

        self._layout = QtGui.QVBoxLayout()
        self._args = {'label_width': labelWidth,
                      'field_width': fieldWidth,
                      'height': height,
                      'disable_label': disableLabel,
                      'disable_restore': disableRestore}
        self._widgets = {}
        self._layout.setSpacing(5)
        self.setLayout(self._layout)

    def _add_field(self, field, key):
        import uuid
        self._widgets[key if key is not None else str(uuid.uuid4())] = field
        self._layout.addWidget(field)

    def add_string(self, label='', value=None, mandatory=True, key=None):
        from lazy_sfm_tools.ui.line_edits import StringField
        self._add_field(StringField(label=label, value=value, mandatory=mandatory, parent=self, **self._args), key)

    def add_int(self, label='', value=None, mandatory=True, key=None):
        from lazy_sfm_tools.ui.line_edits.int_field import IntField
        self._add_field(IntField(label=label, value=value, mandatory=mandatory, parent=self, **self._args), key)

    def add_float(self, label='', value=None, mandatory=True, key=None):
        from lazy_sfm_tools.ui.line_edits.float_field import FloatField
        self._add_field(FloatField(label=label, value=value, mandatory=mandatory, parent=self, **self._args), key)

    def add_file(self, title, extensions, folder_root_source_id, category, label='', value=None, mandatory=True, key=None):
        from lazy_sfm_tools.ui.line_edits import FileField
        self._add_field(
            FileField(
                title, extensions, folder_root_source_id, category, label=label,
                value=value, mandatory=mandatory, parent=self, **self._args
            ),
            key
        )

    def add_folder(self, title, folder_root_source_id, category, label='', value=None, mandatory=True, key=None):
        from lazy_sfm_tools.ui.line_edits import FolderField
        self._add_field(
            FolderField(
                title, folder_root_source_id, category, label=label,
                value=value, mandatory=mandatory, parent=self, **self._args
            ),
            key
        )

    def add_color(self, label='', value=None, output_format="HEX", mandatory=True, key=None):
        from lazy_sfm_tools.ui.line_edits.color_field import ColorField
        self._add_field(
            ColorField(
                label=label, value=value, output_format=output_format, mandatory=mandatory, parent=self, **self._args
            ), 
            key
        )

    def add_vector(self, label='', value=None, dimensions=3, mandatory=True, key=None):
        from lazy_sfm_tools.ui.line_edits import VectorField
        self._add_field(
            VectorField(
                label=label, value=value, dimensions=dimensions, mandatory=mandatory, parent=self, **self._args
            ),
            key
        )

    def add_vector_separated(self, label='', value=None, dimensions=3, mandatory=True, key=None):
        from lazy_sfm_tools.ui.line_edits.vector_split_field import VectorSplitField
        self._add_field(
            VectorSplitField(
                label=label, value=value, dimensions=dimensions, mandatory=mandatory, parent=self, **self._args
            ),
            key
        )

    def restore_value(self):
        for widget in self._widgets.values():  # type: AbstractField
            widget.restore_value()

    @property
    def is_filled(self):
        return all([widget.is_filled for widget in self._widgets.values()])

    @property
    def is_valid(self):
        return all([widget.is_valid for widget in self._widgets.values()])

    @property
    def value(self):
        # type: () -> dict
        valueDict = {}
        for key, widget in self._widgets.items():  # type: str, AbstractField
            valueDict[key] = widget.value if widget.is_valid else None
        return valueDict

    @value.setter
    def value(self, value):
        if type(value) == list or type(value) == tuple:
            for widget in self._widgets.values():  # type: AbstractField
                widget.value = value
        elif type(value) == dict:
            for key in value.keys():
                if key in self._widgets:
                    self._widgets[key].value = value[key]
        else:
            self.value = [value] * len(self._widgets)

    def set_label_width(self, width):
        for widget in self._widgets.values():  # type: AbstractField
            widget.set_label_width(width)

    def set_field_size(self, width, height):
        for widget in self._widgets.values():  # type: AbstractField
            widget.set_field_size(width, height)
