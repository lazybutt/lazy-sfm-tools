from lazy_sfm_tools.ui.line_edits.common import AbstractMonoField


class StringField(AbstractMonoField):
    def __init__(self, **kwargs):
        super(StringField, self).__init__(**kwargs)
        if not self._disableLabel:
            self._layout.addWidget(self._label)
        self._layout.addWidget(self._line_edit)
        if not self._disable_restore:
            self._layout.addWidget(self._restore_button)
        self.setLayout(self._layout)
        self._setup_init_sizes(kwargs)
        if 'value' in kwargs:
            self.Value = kwargs['value']

    @property
    def value(self):
        return self._line_edit.text()

    @value.setter
    def value(self, value):
        if value is not None:
            self._line_edit.setText(value)
            self._restore_button.setEnabled(True)

    @property
    def is_valid(self):
        if not self.is_filled:
            if self.is_mandatory:
                return False
        return True

    @property
    def is_filled(self):
        return self._line_edit.text().replace(' ', '') != ''

    def restore_value(self):
        if self._value_to_restore is not None:
            self._line_edit.setText(self._value_to_restore)
