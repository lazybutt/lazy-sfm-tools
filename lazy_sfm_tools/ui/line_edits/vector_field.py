from lazy_sfm_tools.ui.line_edits.common import AbstractMonoField


class VectorField(AbstractMonoField):
    def __init__(self, **kwargs):
        super(VectorField, self).__init__(**kwargs)
        self._dimensions = kwargs['dimensions'] if 'dimensions' in kwargs else 3
        if not self._disable_label:
            self._layout.addWidget(self._label)
        self._layout.addWidget(self._line_edit)
        if not self._disableRestore:
            self._layout.addWidget(self._restore_button)
        self._line_edit.textEdited.connect(lambda x: self._value_processing(self._line_edit.text()))
        self.setLayout(self._layout)
        self._setup_init_sizes(kwargs)
        if 'value' in kwargs:
            self.value = kwargs['value']

    @property
    def value(self):
        if not self.is_filled:
            return None
        split_value = self._line_edit.text().split(' ')
        split_value = [float(value) for value in split_value]
        return split_value + [0.0] * (self._dimensions - len(split_value))

    @value.setter
    def value(self, value):
        if value is not None:
            self._value_processing(value)
            self._value_to_restore = self._line_edit.text()
            self._restore_button.setEnabled(True)

    @property
    def is_valid(self):
        if not self.is_filled:
            if self.is_mandatory:
                return False
        return True

    @property
    def is_filled(self):
        return self._line_edit.text().replace(' ', '') != ''

    def restore_value(self):
        if self._value_to_restore is not None:
            self._line_edit.setText(self._value_to_restore)
            self._value_processing(self._value_to_restore)

    def _value_processing(self, value=None):
        if value is not None and not str(value).isspace():
            text_to_parse = ''
            available_characters = [' ', '-', ',', '.', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
            last_char_is_space = True
            minus_counter = 0
            dot_counter = 0
            space_counter = 1
            for char in value:
                if char in available_characters:
                    if char == ' ':
                        if not last_char_is_space and space_counter < self._dimensions:
                            text_to_parse += char
                            last_char_is_space = True
                            space_counter += 1
                    elif char == ',' or char == '.':
                        if dot_counter < space_counter:
                            text_to_parse += '.'
                            dot_counter += 1
                            last_char_is_space = False
                    elif char == '-':
                        if minus_counter < space_counter:
                            if last_char_is_space:
                                text_to_parse += '-'
                                minus_counter += 1
                    else:
                        text_to_parse += char
                        last_char_is_space = False
            self._value = text_to_parse
            if value != text_to_parse:
                self._line_edit.setText(self._value)
