from PySide import QtCore
from PySide import QtGui

from lazy_sfm_tools.ui.list import BufferedListWidget
from lazy_sfm_tools.utils.value_buffer import BufferByID

class BufferedListWidgetWithCheckBox(BufferedListWidget):
    def __init__(self, check_state_changed_callback=None, *args, **kwargs):
        super(BufferedListWidgetWithCheckBox, self).__init__(*args, **kwargs)
        self._state_changed = check_state_changed_callback

    def add_item(self, text, icon=None, is_checked=False):
        """Add item in list widget.

        Args:
            text (str|unicode): item text.
            icon (QtGui.QIcon|None): optional icon.
            is_checked (bool): is check box checked.

        Returns:
            QtGui.QListWidgetItem: added list widget item.
        """
        item = QtGui.QListWidgetItem()
        check_box = QtGui.QCheckBox(text)
        check_box.setChecked(is_checked)
        if self._state_changed is not None:
            check_box.stateChanged.connect(self._state_changed)
        if icon is not None:
            item.setIcon(icon)
        self.addItem(item)
        self.setItemWidget(item, check_box)
        return item
