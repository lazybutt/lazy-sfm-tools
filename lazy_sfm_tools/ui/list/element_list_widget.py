from lazy_sfm_tools.ui.icons import AnimsetIcons
from lazy_sfm_tools.ui.list import BaseListWidget
from lazy_sfm_tools.utils.value_buffer import BufferByID
from PySide import QtCore


class ElementListWidget(BaseListWidget):
    def __init__(self, *args, **kwargs):
        super(ElementListWidget, self).__init__(*args, **kwargs)

    def add_anim_set(self, animset, override_name=None, is_checked=False, buffer_key=None):
        animset_name = animset.GetName() if override_name is None else override_name
        icon = AnimsetIcons.of(animset)
        item = self._add_item(animset_name, icon, is_checked)
        buffer_key = BufferByID.add(animset, buffer_key)
        item.setData(QtCore.Qt.UserRole, buffer_key)
        return item
