from PySide import QtCore
from PySide import QtGui


class BaseListWidget(QtGui.QListWidget):
    def __init__(self, *args, **kwargs):
        self._with_check_box = kwargs.pop("with_check_box") if "with_check_box" in kwargs else False
        self._check_box_callback = kwargs.pop("check_box_callback") if "check_box_callback" in kwargs else None
        self._default_icon = kwargs.pop("default_icon") if "default_icon" in kwargs else None
        super(BaseListWidget, self).__init__(*args, **kwargs)

    def get_current_data(self, slot=QtCore.Qt.UserRole):
        """Receive current data.

        Args:
            slot (QtCore.Qt.UserRole): data slot.

        Returns:
            Any: stored data.
        """
        return self.currentItem().data(slot)

    def _add_item(self, text, icon=None, is_checked=False):
        """Add item in list widget.

        Args:
            text (str|unicode): item text.
            icon (QtGui.QIcon|None): optional icon.
            is_checked (bool): is checked if item with checkbox.

        Returns:
            QtGui.QListWidgetItem: added list widget item.
        """
        if icon is None and self._default_icon is not None:
            icon = self._default_icon
        if self._with_check_box:
            item = QtGui.QListWidgetItem()
            check_box = QtGui.QCheckBox(text)
            check_box.setChecked(is_checked)
            if self._check_box_callback is not None:
                check_box.stateChanged.connect(self._check_box_callback)
            self.setItemWidget(item, check_box)
        else:
            item = QtGui.QListWidgetItem(text)
        if icon is not None:
            item.setIcon(icon)
        self.addItem(item)
        return item
