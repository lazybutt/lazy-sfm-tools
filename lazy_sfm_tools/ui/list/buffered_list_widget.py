from PySide import QtCore

from lazy_sfm_tools.ui.list import BaseListWidget
from lazy_sfm_tools.utils.value_buffer import BufferByID

class BufferedListWidget(BaseListWidget):
    def __init__(self, *args, **kwargs):
        super(BufferedListWidget, self).__init__(*args, **kwargs)

    def get_current_buffered_data(self, slot=QtCore.Qt.UserRole):
        """Receive current data from buffer.

        Args:
            slot (QtCore.Qt.UserRole): data slot.

        Returns:
            Any: stored data.
        """
        buffer_key = self.get_current_data(slot)
        return BufferByID[buffer_key]

    def add_item(self, text, icon=None, is_checked=False):
        """Add item in list widget.

        Args:
            text (str|unicode): item text.
            icon (QtGui.QIcon|None): optional icon.
            is_checked (bool): is checked if item with checkbox.

        Returns:
            QtGui.QListWidgetItem: added list widget item.
        """
        self._add_item(text, icon, is_checked)

    def add_data_item(self, text, data, slot=QtCore.Qt.UserRole, icon=None, is_checked=False):
        """Add item in list widget with data.

        Args:
            text (str|unicode): item text.
            data (Any): data to set.
            slot (QtCore.Qt.UserRole): data slot.
            icon (QtGui.QIcon|None): optional icon.
            is_checked (bool): is checked if item with checkbox.

        Returns:
            QtGui.QListWidgetItem: added list widget item.
        """
        item = self.add_item(text, icon, is_checked)
        item.setData(slot, data)

    def add_buffered_data_item(self, text, data, buffer_key=None, slot=QtCore.Qt.UserRole, icon=None, is_checked=False):
        """Add item in list widget with data.

        Args:
            text (str|unicode): item text.
            data (Any): data to set.
            buffer_key (Any|None): optional custom buffer key.
            slot (QtCore.Qt.UserRole): data slot.
            icon (QtGui.QIcon|None): optional icon.
            is_checked (bool): is checked if item with checkbox.

        Returns:
            QtGui.QListWidgetItem: added list widget item.
        """
        item = self._add_item(text, icon, is_checked)
        buffer_key = BufferByID.add(data, buffer_key)
        item.setData(slot, buffer_key)
        return item
