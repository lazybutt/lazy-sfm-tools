import sfm
from vs.movieobjects import CDmeClip, CDmeDag, CDmeAnimationSet
from lazy_sfm_tools.factories import DagFactory
from lazy_sfm_tools.files.file_structure import GamePath
from lazy_sfm_tools.utils.elements import create_element


class AnimationSetFactory(object):
    """Factory of animation sets."""

    @classmethod
    def create_camera_animset(cls, name, shot=None, parent_dag=None):
        """Create animation set of camera.

        Args:
            name (str|unicode): camera name.
            shot (CDmeClip|None): shot where you need to create an animation set.
            parent_dag (CDmeDag|None): parent dag (animation sets group).

        Returns:
            CDmeAnimationSet: created camera animation sets.
        """
        camera = DagFactory.create_camera(str(name))
        return cls.create_anim_set(str(name), camera, shot, parent_dag)

    @classmethod
    def create_light_animset(cls, name, shot=None, parent_dag=None):
        """Create animation set of light.

        Args:
            name (str|unicode): camera name.
            shot (CDmeClip|None): shot where you need to create an animation set.
            parent_dag (CDmeDag|None): parent dag (animation sets group).

        Returns:
            CDmeAnimationSet: created light animation sets.
        """
        light = DagFactory.create_light(name)
        return cls.create_anim_set(str(name), light, shot, parent_dag)

    @classmethod
    def create_model_animset(cls, name, mdl_path, shot=None, parent_dag=None):
        """Create animation set of model.

        Args:
            name (str|unicode): camera name.
            mdl_path (str|unicode|GamePath): path to mdl model file.
            shot (CDmeClip|None): shot where you need to create an animation set.
            parent_dag (CDmeDag|None): parent dag (animation sets group).

        Returns:
            CDmeAnimationSet: created model animation sets.
        """
        game_path = cls._path_prepare(mdl_path, "models", ".mdl")
        gamemodel = sfm.CreateModel(game_path.mod_relative_path)
        return cls.create_anim_set(str(name), gamemodel, shot, parent_dag)

    @classmethod
    def particle_animset(cls, name, shot=None, parent_dag=None):
        """Not implemented yet."""
        raise NotImplementedError("Particles not implemented yet")

    @staticmethod
    def create_anim_set(name, target, shot=None, parent_dag=None):
        if shot is None:
            shot = sfm.GetCurrentShot()
        if parent_dag is None:
            parent_dag = shot.scene
        animset = sfm.CreateAnimationSet(str(name), target=target)
        parent_dag.AddChild(target)
        return animset

    @staticmethod
    def _path_prepare(path, category, extension):
        if not isinstance(path, GamePath):
            path = GamePath(path, category)
        if not path.is_valid or not path.is_file or path.extension != extension:
            raise OSError('Path "{}" not found OR path is not file OR extension is not ".{}"'
                          .format(path.absolute_path, extension))
        return path
