from vs.datamodel import CDmElement, CDmAttribute
from lazy_sfm_tools.misc import AttributeSetter
from lazy_sfm_tools.commons.enums import AttributeTypes


class AttributeFactory(object):
    """Factory of attribute objects."""

    @classmethod
    def create_attribute(cls, name, attr_type, parent_element, value=None):
        """Create attribute object.

        Args:
            name (str|unicode): attribute name.
            attr_type (AttributeTypes): attribute type.
            parent_element (CDmElement): parent element to add attribute.
            value (Any|None): value to set.

        Returns:
            CDmAttribute: created attribute object.
        """
        attr = parent_element.AddAttribute(str(name), attr_type.value)
        if value is not None:
            AttributeSetter.set(attr, value)
        return attr
