from vs.movieobjects import CDmeClip, CDmeTrack, CDmeTrackGroup
from lazy_sfm_tools.utils.elements import create_element

class MovieObjectsFactory(object):
    """Factory of objects that are elements of scene separation."""

    @classmethod
    def shot(cls, name):
        """Create shot (clip) object.

        Args:
            name (str|unicode): shot (clip) name.

        Returns:
            CDmeClip: shot (clip) object.
        """
        return create_element("DmeFilmClip", name)

    @classmethod
    def track(cls, name):
        """Create track object.

        Args:
            name (str|unicode): track name.

        Returns:
            CDmeTrack: track object.
        """
        return create_element("DmeTrack", name)

    @classmethod
    def track_group(cls, name):
        """Create track group object.

        Args:
            name (str|unicode): track group name.

        Returns:
            CDmeTrackGroup: track group object.
        """
        return create_element("DmeTrackGroup", name)
