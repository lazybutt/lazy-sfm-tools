from vs.datamodel import CDmElement
from vs.movieobjects import CDmeDag
from vs.movieobjects import CDmeCamera
from vs.movieobjects import CDmeGameModel
from lazy_sfm_tools.files.file_structure import GamePath
from lazy_sfm_tools.utils.elements import create_element


class DagFactory(object):
    """Factory of dag based objects."""

    @classmethod
    def create_camera(cls, name, parent_dag=None):
        """Create camera object.

        Args:
            name: camera name
            parent_dag (CDmeDag|None): parent dag.

        Returns:
            CDmeCamera: camera object.
        """
        camera = create_element("DmeCamera", name)
        cls._add_to_parent(camera, parent_dag)
        return camera

    @classmethod
    def create_light(cls, name, parent_dag=None):
        """Create light object.

        Args:
            name (str|unicode): light name
            parent_dag (CDmeDag|None): parent dag.

        Returns:
            CDmElement: light object.
        """
        light = create_element("DmeProjectedLight", str(name))
        cls._add_to_parent(light, parent_dag)
        return light

    @classmethod
    def create_model(cls, name, mdl_path, parent_dag=None):
        """Create game model object.

        Args:
            name (str|unicode): name of game model.
            mdl_path (str|unicode|GamePath): path to mdl model file.
            parent_dag (CDmeDag|None): parent dag.

        Returns:
            CDmeGameModel: game model object.
        """
        game_path = cls._path_prepare(mdl_path, "models", "mdl")
        model = sfm.CreateModel(game_path.mod_relative_path)
        model.SetName(name)
        cls._add_to_parent(model, parent_dag)
        return model

    @classmethod
    def create_dag(cls, name, parent_dag=None):
        """Create dag object.

        Args:
            name (str|unicode): dag name.
            parent_dag (CDmeDag|None): parent dag.

        Returns:
            CDmeDag: dag object.
        """
        dag = create_element("DmeDag", name)
        cls._add_to_parent(dag, parent_dag)
        return dag

    @classmethod
    def create_particle(cls, name, shot=None):
        """Create particle object. Not implemented yet."""
        raise NotImplementedError("Particles not implemented yet")

    @staticmethod
    def _add_to_parent(child_dag, parent_dag):
        """Add child dag to parent dag if parent is not None.

        Args:
            child_dag (CDmeDag): child dag
            parent_dag (CDmeDag|None): parent dag
        """
        if parent_dag is not None:
            parent_dag.AddToTail(child_dag)

    @staticmethod
    def _path_prepare(path, category, extension):
        if not isinstance(path, GamePath):
            path = GamePath(path, category)
        if not path.is_valid or not path.is_file or path.extension != extension:
            raise OSError('Path "{}" not found OR path is not file OR extension is not ".{}"'
                          .format(path.absolute_path, extension))
        return path
