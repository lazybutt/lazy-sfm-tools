"""
For development purposes!
DO NOT USE in released scripts!
It can critically slow down other scripts!

Just import it in first line of ur script if you want
to reimport lazy_sfm_tools package without restart sfm.
"""
def clear_cache():
    import sys
    is_lazy_sfm_tool_module = lambda module_item: 'lazy_sfm_tools' in module_item and \
                                                  'lazy_sfm_tools.maintain' not in module_item
    [sys.modules.pop(key) for key in sys.modules.keys() if is_lazy_sfm_tool_module(key)]
    import sfm
    sfm.Msg("lazy_sfm_tools package cache cleared, lazy_sfm_tools can be re-imported\n")
