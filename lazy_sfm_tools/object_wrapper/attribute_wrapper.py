from vs.datamodel import CDmAttribute
from lazy_sfm_tools.misc import AttributeSetter
from lazy_sfm_tools.commons.enums import AttributeTypes
from lazy_sfm_tools.utils.type_checks import isinstance_or_raise


class AttributeWrapper(object):
    """Attribute wrapper for simplify manipulations with get and set operations."""

    def __init__(self, attribute):
        """Constructor for AttributeWrapper."""
        isinstance_or_raise(attribute, CDmAttribute)
        self._attr = attribute

    @property
    def name(self):
        """Receive attribute name.

        Returns:
            str: attribute name.
        """
        return self._attr.GetName()

    @name.setter
    def name(self, attr_name):
        """Set attribute name.

        Args:
            attr_name (str|unicode): new attribute name.
        """
        self._attr.SetName(str(attr_name))

    @property
    def type(self):
        """Receive attribute type.

        Returns:
            AttributeTypes: attribute type.
        """
        attr_type_code = self._attr.GetType()
        return AttributeTypes.of(attr_type_code)
    
    @type.setter
    def type(self, attr_type):
        """Set attribute type.

        Args:
            attr_type (AttributeTypes): new attribute type.
        """
        self._attr.ChangeType_UNSAFE(attr_type.value)

    @property
    def value(self):
        """Receive attribute value.

        Returns:
            Any: attribute value.
        """
        return self._attr.GetValue()

    @value.setter
    def value(self, attr_value):
        """Set attribute name.

        Args:
            attr_value (Any): new attribute value.
        """
        AttributeSetter.set(self.native, attr_value)

    @property
    def native(self):
        """Receive native attribute object.

        Returns:
            CDmAttribute: native attribute object.
        """
        return self._attr
