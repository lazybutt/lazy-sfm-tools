from vs.misc import Color
from lazy_sfm_tools.utils.type_checks import isinstance_or_raise
from lazy_sfm_tools.object_wrapper.element_wrapper import ElementWrapper


class LightWrapper(ElementWrapper):
    """Light wrapper for simplify manipulations with light attributes."""

    def __init__(self, light):
        """Constructor for LightWrapper."""
        super(LightWrapper, self).__init__(light)
        import vs
        isinstance_or_raise(light, vs.CDmeProjectedLight)

    @property
    def shadow_enabled(self):
        """Get shadows enabled.

        Returns:
            bool: shadows enabled.
        """
        return self._get("castsShadows")

    @shadow_enabled.setter
    def shadow_enabled(self, value):
        """Set shadows enabled.

        Args:
            value (bool): enable shadows.
        """
        self._set("castsShadows", value)

    @property
    def volumetric(self):
        """Get shadows enabled.

        Returns:
            bool: shadows enabled.
        """
        return self._get("volumetric")

    @volumetric.setter
    def volumetric(self, value):
        """Set volumetric enabled.

        Args:
            value (bool): enable volumetric.
        """
        self._set("volumetric", value)

    @property
    def color(self):
        """Get shadows enabled.

        Returns:
            Color: shadows enabled.
        """
        return self._get("color")

    @color.setter
    def color(self, value):
        """Set light color.

        Args:
            value (str|unicode|list[int]|Color): object convertible to color.
        """
        from lazy_sfm_tools.misc.type_converters import ColorConverter, ColorReprType
        native_color = ColorConverter.convert(value, ColorReprType.COLOR_NATIVE)
        self._set("color", native_color)
