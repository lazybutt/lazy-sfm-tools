from lazy_sfm_tools.misc import AttributeSetter


class ElementWrapper(object):
    """Element wrapper for simplify manipulations with attributes."""

    def __init__(self, element):
        """Constructor for ElementWrapper.

        Args:
            element: element inherited from CDmElement to wrap.
        """
        import vs.datamodel
        if hasattr(element, "GetName"):
            raise TypeError("Element must be children of {}".format(vs.datamodel.CDmElement))
        self._element = element

    @property
    def name(self):
        """Receive element name.

        Returns:
            str: element name.
        """
        return self._get("name")

    @name.setter
    def name(self, value):
        """Set element name.

        Args:
            value (str|unicode): new element name
        """
        self._set("name", str(value))

    def _get(self, attr):
        return self._element.__getattribute__(attr).GetValue()

    def _set(self, attr, value):
        attr = self._element.__getattribute__(attr)
        AttributeSetter.set(attr, value)


