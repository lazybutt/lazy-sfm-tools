import sfm
from enum import Enum
from vs.movieobjects import CDmeClip, CDmeAnimationSet
from lazy_sfm_tools.commons.enums.animset_type import AnimationSetType
from lazy_sfm_tools.selectors import ShotSelector


class AnimSetSelectContext(Enum):
    IN_CURRENT_SHOT = 1
    IN_SHOT_BY_TIMEHEAD = 2

class _AnimationSetsSelector(object):
    """
    Class for receive animation sets by parameters.
    """

    @property
    def current_animation_set(self):
        """Current animation set receiver.

        Notes:
            Works only in code called from rig menu.

        Returns:
            vs.CDmeAnimationSet|None: current selected animation set or None if not selected.
        """
        return sfm.GetCurrentAnimationSet()

    @classmethod
    def animation_sets_in_shot(cls, shot_or_context, *type_filter):
        """Find all animation sets in shot with filtration.

        Args:
            shot_or_context (CDmeClip|AnimSetSelectContext): clip or context from which you need to get animation sets.
            *type_filter (AnimationSetType): listing the required types of animation sets, can be empty.

        Returns:
            list[CDmeAnimationSet]: list of found animation sets.
        """
        shot = cls._extract_shot(shot_or_context)
        if not type_filter:
            return [animset for animset in shot.animationSets.GetValue()]
        else:
            return [
                animset for animset in shot.animationSets.GetValue()
                if len(type_filter) == 0 or AnimationSetType.of(animset) in type_filter
            ]

    @classmethod
    def find_by_name_in_shot(cls, animset_name, shot_or_context):
        """Find animation sets in shot by name.

        Args:
            animset_name: name of the animation set you are looking for.
            shot_or_context (CDmeClip|AnimSetSelectContext): clip or context from which you need to get animation sets.

        Returns:
            list[CDmeAnimationSet]: list of found animation sets.
        """
        shot = cls._extract_shot(shot_or_context)
        for animSet in shot.animationSets:
            if animSet.GetValue() == str(animset_name):
                return animSet
        raise KeyError('Animation set "{}" not found'.format(animset_name))

    @staticmethod
    def _extract_shot(shot_or_context):
        if type(shot_or_context) == AnimSetSelectContext:
            if shot_or_context == AnimSetSelectContext.IN_CURRENT_SHOT:
                return ShotSelector.current_shot
            elif shot_or_context == AnimSetSelectContext.IN_SHOT_BY_TIMEHEAD:
                return ShotSelector.shot_on_playhead
            else:
                raise ValueError("Incorrect animation set select context: {}".format(shot_or_context))
        else:
            from lazy_sfm_tools.utils.type_checks import isinstance_or_raise
            isinstance_or_raise(shot_or_context, CDmeClip)
            return shot_or_context

    def __getattr__(self, animset_name, shot_or_context=ShotSelector.current_shot):
        """Find animation sets in shot by name.

        Args:
            animset_name: name of the animation set you are looking for.
            shot_or_context (CDmeClip|AnimSetSelectContext): clip or context from which you need to get animation sets.

        Returns:
            list[CDmeAnimationSet]: list of found animation sets.
        """
        return self.find_by_name_in_shot(str(animset_name), shot_or_context)


AnimationSetsSelector = _AnimationSetsSelector()
