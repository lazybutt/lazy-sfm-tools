import sfm
import sfmApp
from vs.movieobjects import CDmeClip

class _ShotSelector(object):
    """
    Class for receive shots by parameters.
    """

    @property
    def current_shot(self):
        """Current selected shot receiver.

        Notes:
            Works only in code called from rig menu.

        Returns:
            CDmeClip: current selected shot.
        """
        return sfm.GetCurrentShot()

    @property
    def shot_on_playhead(self):
        """Current shot by playhead receiver.


        Returns:
            CDmeClip: current shot by playhead.
        """
        playhead_time = sfmApp.GetHeadTimeInSeconds()
        return sfmApp.GetShotAtCurrentTime(playhead_time)

    @property
    def shots(self):
        """All shots receiver.

        Returns:
            list[CDmeClip]: list of shots.
        """
        return {shot.GetName(): shot for shot in sfmApp.GetShots()}

    @staticmethod
    def shot_at_time(at_time):
        """Shot at time receiver.

        Returns:
            CDmeClip|None: found shot or None if all clips out of received time.
        """
        return sfmApp.GetShotAtCurrentTime(at_time)

    def __getitem__(self, query):
        """Shot receiver by index or name.

        Args:
            query (int|str|unicode): index or name of shot to get.

        Raises:
            KeyError: if shot not found by name.
            TypeError: if incorrect query type.

        Returns:
            CDmeClip: found clip.
        """
        if isinstance(query, int):
            return self.shots[query]
        elif isinstance(query, (str, unicode)):
            for shot in self.shots:
                if shot.GetName() == str(query):
                    return shot
            raise KeyError('Shot "{}" not found'.format(query))
        else:
            raise TypeError('ShotSelector indices must be integers or str, not "{}"'.format(type(query)))

    def __contains__(self, name):
        """Checking for shot by name.

        Args:
            name (str|unicode): shot name query

        Returns:
            bool: True if contains, False if not.
        """
        if isinstance(name, (str, unicode)):
            for shot in self.shots:
                if shot.GetName() == str(name):
                    return True
        return False


ShotSelector = _ShotSelector()
