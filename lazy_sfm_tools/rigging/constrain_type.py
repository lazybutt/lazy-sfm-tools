from enum import Enum
import sfm

class ConstraintType(Enum):
    AIM = sfm.AimConstraint
    IK = sfm.IKConstraint
    ORIENT = sfm.OrientConstraint
    POINT = sfm.PointConstraint
    PARENT = sfm.ParentConstraint
