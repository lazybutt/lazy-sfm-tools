from vs.movieobjects import CDmeAnimationSet, CDmeTransformControl, CDmeConnectionOperator, CDmeGlobalFlexControllerOperator

from lazy_sfm_tools.misc.attribute_connection import create_connection_operator
from lazy_sfm_tools.rigging.constrain_type import ConstraintType


class AnimsetConstrainTool(object):
    @classmethod
    def constrain_animsets(cls, master, slave):
        """Make parent constrain for bones with repeating names in master and slave animations sets

        Args:
            master: master animation set
            slave: slave animation set
        """
        cls.constrain_animsets_bones(master, slave)

        cls.constrain_animsets_flexes(master, slave)

    @staticmethod
    def constrain_animsets_bones(master, slave):
        """Make parent constrain for bones with repeating names in master and slave animations sets

        Args:
            master: master animation set
            slave: slave animation set
        """
        import sfm

        master_name = str(master.GetName())
        slave_name = str(slave.GetName())

        master_bone_names = []
        slave_bone_names = []

        for control in master.controls:
            if isinstance(control, CDmeTransformControl):
                master_bone_names.append(control.GetName())

        for control in slave.controls:
            if isinstance(control, CDmeTransformControl):
                slave_bone_names.append(control.GetName())

        for bone_name in master_bone_names:
            if bone_name in slave_bone_names:
                parentName = master_name + ":" + bone_name
                childName = slave_name + ":" + bone_name
                sfm.ParentConstraint(parentName, childName)

    @classmethod
    def constrain_animsets_flexes(cls, master, slave):
        """Make parent constrain for bones with repeating names in master and slave animations sets

        Args:
            master (CDmeAnimationSet): master animation set
            slave (CDmeAnimationSet): slave animation set
        """
        if not master.HasAttribute("gameModel") or not slave.HasAttribute("gameModel"):
            return
        slave_flexes = {flex.GetName(): flex for flex in slave.gameModel.globalFlexControllers}
        master_flexes = {flex.GetName(): flex for flex in master.gameModel.globalFlexControllers if flex.GetName() in slave_flexes}
        for flex_name, master_flex in master_flexes.items():
            cls.connect_flexes(master_flex, slave_flexes[flex_name], master)

    @staticmethod
    def connect_flexes(master_flex, slave_flex, master_animset):
        """

        Args:
            master_flex (CDmeGlobalFlexControllerOperator): master flex controller.
            slave_flex (CDmeGlobalFlexControllerOperator): slave flex controller.
            master_animset (CDmeAnimationSet): master_animset

        Returns:
            CDmeConnectionOperator: created connection operator.
        """
        operator_name = "flexDriver_" + master_flex.GetName().replace("-", "_")
        return create_connection_operator(
            operator_name, master_animset, master_flex, "flexWeight", {slave_flex: "flexWeight"}
        )

    @staticmethod
    def constrain_bones(master_animset, slave_animset, master_bone, slave_bone, constraint_type, **kw):
        """Create parent constrain for master and slave bone.

        Args:
            master_animset(CDmeAnimationSet|str|unicode): master animation set or name.
            slave_animset(CDmeAnimationSet|str|unicode): slave animation set or name.
            master_bone (CDmeTransformControl|str|unicode): master bone or name.
            slave_bone (CDmeTransformControl|str|unicode): slave bone or name.
            constraint_type (ConstraintType): constrain type.

        Keyword Args:
            name (str|unicode): name of created constraint.
            with_current_offset (bool): if false then the slave bone will "jump" to the master bone.
                That is, the location and/or orientation and/or size (depends on constrain type)
                will be copied from master bone to slave bone.
                If true then the location, rotation, and size of the slave bone will remain the same as before/
            create_control (bool): the need to create a controller to manually change the affect weight.
            control_group_name (str|unicode): creates control group in animation set with given name.
        """
        constrain_kw = {}
        if "name" in kw:
            constrain_kw["name"] = str(kw["name"])
        if "with_current_offset" in kw:
            constrain_kw["mo"] = kw["with_current_offset"]
        if "create_control" in kw:
            constrain_kw["create_control"] = kw["create_control"]
        if "control_group_name" in kw:
            constrain_kw["group"] = str(kw["control_group_name"])

        if isinstance(master_animset, CDmeAnimationSet):
            master_animset = master_animset.GetName()
        if isinstance(slave_animset, CDmeAnimationSet):
            slave_animset = slave_animset.GetName()
        if isinstance(master_bone, CDmeTransformControl):
            master_bone = master_bone.GetName()
        if isinstance(slave_bone, CDmeTransformControl):
            slave_bone = slave_bone.GetName()
        func = constraint_type.value
        return func(
            str(master_animset) + ":" + str(master_bone),
            str(slave_animset) + ":" + str(slave_bone),
            **constrain_kw
        )
