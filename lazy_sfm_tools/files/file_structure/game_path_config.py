import os

from lazy_sfm_tools.files.file_structure.enums import ContentFolder
from filesystem.valve import GameInfoFile


class _GamePathConfig:
    def __init__(self):
        self._game_info_file = GameInfoFile()

    def get_search_path(self, **kwargs):
        raw_paths = self._game_info_file.getSearchMods()
        paths = []
        for raw_path in raw_paths:
            path_to_add = raw_path
            path_to_add = self._fix_slashes(path_to_add) \
                if "/" in path_to_add \
                else path_to_add
            path_to_add = self._add_content_folder(path_to_add, kwargs["content_folder"]) \
                if "content_folder" in kwargs and kwargs["content_folder"] is not None \
                else path_to_add
            path_to_add = self._force_abs(path_to_add) \
                if "force_abs" in kwargs and kwargs["force_abs"] is True \
                else path_to_add
            paths.append(path_to_add)
        return paths

    @property
    def material_search_path(self):
        return self.get_search_path(content_folder=ContentFolder.MATERIALS)

    @property
    def material_search_abs_path(self):
        return self.get_search_path(content_folder=ContentFolder.MATERIALS, force_abs=True)

    @property
    def models_search_path(self):
        return self.get_search_path(content_folder=ContentFolder.MODELS)

    @property
    def models_search_abs_path(self):
        return self.get_search_path(content_folder=ContentFolder.MODELS, force_abs=True)

    @property
    def cfg_search_path(self):
        return self.get_search_path(content_folder=ContentFolder.CONFIGS)

    @property
    def cfg_search_abs_path(self):
        return self.get_search_path(content_folder=ContentFolder.CONFIGS, force_abs=True)

    @staticmethod
    def _force_abs(path):
        if os.path.isabs(path):
            return path
        return os.path.join(os.getcwd(), path)

    @staticmethod
    def _fix_slashes(path):
        return path.replace("/", "\\")

    @staticmethod
    def _add_content_folder(path, content_folder):
        import os
        os.path.join(path, content_folder)


GamePathConfig = _GamePathConfig()
