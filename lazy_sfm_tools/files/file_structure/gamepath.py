import os as _os
import os.path

import pathlib
from enums import ContentFolder
from enums import PathRelativity


class GamePath(object):
    """
    Object for working with the SFM file system

    Consider the SFM file structure using the following path:
    game/usermod/models/esther/misc/blackout_001.mdl
        - game: is the root path because that's where the sfm.exe executable file is located,
            and that's the reference point that is used to resolve relative paths
        - usermod: this part of the path is called "mod" in SFM, needed to divide content into logical groups
            (usermod user content, workshop content from workshop steam, tf2 content from team fortress 2, etc.)
            These folders contain models, materials, textures, scripts, sounds, etc.
        - models: the folder which is in the mod that contains the models, in the sfm paths to the models
            are specified after this folder, the same applies to the folder materials which is also in the mod.
            The same applies to other folders contained in the mod.
            This part of the path in my case I named "typed content".
        - esther/misc/blackout_001.mdl: the final part of the path
            inside the "typed content" (models in that case).
    """

    @classmethod
    def material_or_texture(cls, path):
        """Additional constructor for materials and textures.

        Args:
            path (str|unicode): path to wrap.

        Returns:
            GamePath: wrapped path.
        """
        return GamePath(path, ContentFolder.MATERIALS)

    @classmethod
    def model(cls, path):
        """Additional constructor for models.

        Args:
            path (str|unicode): path to wrap.

        Returns:
            GamePath: wrapped path.
        """
        return GamePath(path, ContentFolder.MODELS)

    @classmethod
    def config(cls, path):
        """Additional constructor for configs.

        Args:
            path (str|unicode): path to wrap.

        Returns:
            GamePath: wrapped path.
        """
        return GamePath(path, ContentFolder.CONFIGS)

    @classmethod
    def script(cls, path):
        """Additional constructor for script.

        Args:
            path (str|unicode): path to wrap.

        Returns:
            GamePath: wrapped path.
        """
        return GamePath(path, ContentFolder.SCRIPT)

    def __init__(self, path, content_folder):
        """Main constructor for GamePath.

        Args:
            path (str|unicode): path to wrap.
            content_folder (str|unicode|ContentFolder): content

        Returns:
            GamePath: wrapped path.
        """
        from filesystem.valve import GameInfoFile
        self._gameFolders = [self._cleanup_path(folder) for folder in GameInfoFile().getSearchMods()]
        self._path = self._cleanup_path(path).replace(self._cleanup_path(_os.getcwd()) + '\\', '')
        if isinstance(content_folder, (str, unicode)):
            self._category = self._cleanup_path(content_folder)
        elif isinstance(content_folder, ContentFolder):
            self._category = content_folder.value
        else:
            raise TypeError("content_folder must be str, unicode or {}".format(ContentFolder.__name__))
        self._gameFolder = ''
        for folder in self._gameFolders:
            self._gameFolder = folder
            if self._path.startswith(folder + '\\') and _os.path.exists(self._path):
                self._path = self._path[len(_os.path.join(folder, self._category)) + 1:]
                break
            elif self._path.startswith(_os.path.join(folder, self._category)):
                self._path = self._path[len(_os.path.join(folder, self._category)) + 1:]
                break
            elif not _os.path.isabs(self._path):
                if _os.path.exists(_os.path.join(folder, self._path)):
                    self._path = self._path[len(self._category) + 1:]
                    break
                elif _os.path.exists(_os.path.join(folder, self._category, self._path)):
                    break
            self._gameFolder = ''

    @property
    def is_material(self):
        """Check if path is file file is material.

        Returns:
            bool: true if path is file and extension is "vmt", false if not.
        """
        return self.is_file and self.extension == "vmt"

    @property
    def is_texture(self):
        """Check if path is file file is texture.

        Returns:
            bool: true if path is file and extension is "vtf", false if not.
        """
        return self.is_file and self.extension == "vtf"

    @property
    def is_model(self):
        """Check if path is file and file is model.

        Returns:
            bool: true if path is file and extension is "mdl", false if not.
        """
        return self.is_file and self.extension == "mdl"

    @property
    def is_file(self):
        """Check if path is file.

        Returns:
            bool: true if path is file, false if not.
        """
        return _os.path.isfile(self.absolute_path)

    @property
    def is_folder(self):
        """Check if path is directory.

        Returns:
            bool: true if path is directory, false if not.
        """
        return _os.path.isdir(self.absolute_path)

    def open_in_explorer(self):
        """Opens folder in windows explorer. If path is file, file will be selected.

        Raises:
            OSError: if path not exists.
        """
        import subprocess
        if not self.is_valid:
            raise OSError('Path "{}" is not exists.'.format(self.absolute_path))
        subprocess.Popen(r'explorer /select,"{}"'.format(self.absolute_path)) \
            if self.is_file else \
            _os.startfile(self.absolute_path)

    def open_file(self):
        """Opens file with default windows program.

        Raises:
            OSError: if path is not file or path not exists.
        """
        if not self.is_valid:
            raise OSError('Path "{}" is not exists.'.format(self.absolute_path))
        if not self.is_file:
            raise OSError('Path "{}" is not file.'.format(self.absolute_path))
        _os.startfile(self.absolute_path)

    @property
    def is_valid(self):
        """Check if path is valid (exists).

        Notes:
            When designing this object it was decided not to raise an exception
            in cases where a non-existent path was passed when creating the object
            (the path was not found anywhere in the sfm file system),
            as in many other libraries.
            So here there is a method to check if the path exists.
        """
        return _os.path.exists(self.game_relative_path)

    @property
    def mod_folder(self):
        """Receive mod folder path

        Notes:
            if mod is inside root game folder, path will be relative to root game folder.
            if mod is outside root game folder, path will be full (from disk literal).

        Examples:
            for path "C:\\...\\SourceFilmmaker\\game\\usermod\\models\\esther\\misc" mod folder is "usermod".
            for path "C:\\...\\some_custom_mod\\models\\esther\\misc" mod folder is "C:\\...\\some_custom_mod".

        Returns:
            str: mod folder path.
        """
        return self._gameFolder.strip('\\')

    @property
    def typed_content_folder(self):
        """Receive typed content path.

        Examples:
            for path "C:\\...\\SourceFilmmaker\\game\\usermod\\models\\esther\\misc"
                typed content folder is "models".

        Returns:
            str: typed content folder path (always relative to mod folder).
        """
        return self._category.strip('\\')

    @property
    def typed_content_relative_path(self):
        """Receive path related to typed contend folder (not inclusive).

        Examples:
            for path "C:\\...\\SourceFilmmaker\\game\\usermod\\models\\esther\\misc\\blackout_001.mdl"
                typed content relative path is "esther\\misc\\blackout_001.mdl".

        Returns:
            str: path related to typed contend folder (inclusive).
        """
        return self._path.strip('\\')

    @property
    def mod_relative_path(self):
        """Receive path related to mod folder (not inclusive).

        Examples:
            for path "C:\\...\\SourceFilmmaker\\game\\usermod\\models\\esther\\misc\\blackout_001.mdl"
                mod relative path is "models\\esther\\misc\\blackout_001.mdl".

        Returns:
            str: path related to mod folder (inclusive).
        """
        return _os.path.join(self._category, self.typed_content_relative_path)

    @property
    def game_relative_path(self):
        """Receive path related to game folder (not inclusive) or full path if mod is outside root game folder.

        Notes:
            if mod is inside root game folder, path will be relative to root game folder.
            if mod is outside root game folder, path will be absolute (from disk literal).

        Examples:
            for path "C:\\...\\SourceFilmmaker\\game\\usermod\\models\\esther\\misc\\blackout_001.mdl"
                game relative path is is "usermod\\models\\esther\\misc\\blackout_001.mdl".
            for path "C:\\...\\some_custom_mod\\models\\esther\\misc\\blackout_001.mdl"
                game relative path is "C:\\...\\some_custom_mod\\models\\esther\\misc\\blackout_001.mdl".

        Returns:
            str: path related to game folder (inclusive) or full path if mod is outside root game folder.
        """
        return _os.path.join(self._gameFolder, self.mod_relative_path)

    @property
    def absolute_path(self):
        """Receive absolute path.

        Returns:
            str: absolute path.
        """
        path = self.game_relative_path
        if pathlib.Path(path).is_absolute():
            return path
        else:
            root_folder = _os.getcwd().lower().replace('/', '\\')
            return _os.path.join(root_folder, self.game_relative_path)

    def as_path_string(self, path_type):
        """Receive a path with relativity by enum.

        Returns:
            str: path with the specified relativity.
        """
        if path_type == PathRelativity.ABSOLUTE:
            return self.absolute_path
        elif path_type == PathRelativity.GAME:
            return self.game_relative_path
        elif path_type == PathRelativity.MOD:
            return self.mod_relative_path
        elif path_type == PathRelativity.TYPED_CONTENT:
            return self.typed_content_relative_path
        else:
            raise ValueError("Wrong path type")

    @property
    def parts(self):
        """Splitting path to parts.

        Returns:
            list[str]: path's parts.
        """
        return self.game_relative_path.split('\\')

    @property
    def extension(self):
        """Receive file extension.

        Returns:
            str: extension if path is file, empty str if path is dir.
        """
        return os.path.splitext(self.typed_content_relative_path)[1].lower() \
            if self.is_file \
            else ""

    @staticmethod
    def _cleanup_path(path, add_slash_to_end=False):
        return str(path).replace('/', '\\').lower().strip("\\") + ("\\" if add_slash_to_end else "")

def MaterialOrTextureGamePath(path):
    return GamePath.material_or_texture(path)

def ModelGamePath(path):
    return GamePath.model(path)

def ConfigGamePath(path):
    return GamePath.config(path)

