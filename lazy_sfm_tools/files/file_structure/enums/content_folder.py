from enum import Enum


class ContentFolder(Enum):
    CONFIGS = "cfg"
    MATERIALS = "materials"
    MODELS = "models"
    PARTICLES = "particles"
    SCRIPT = "scripts"
