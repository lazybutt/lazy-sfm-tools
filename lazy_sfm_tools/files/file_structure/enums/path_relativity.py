from enum import Enum


class PathRelativity(Enum):
    ABSOLUTE = 1
    GAME = 2
    MOD = 3
    TYPED_CONTENT = 4