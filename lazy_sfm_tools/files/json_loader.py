import json


class JsonLoader(object):

    @classmethod
    def load(cls, file_handle):
        """Load json from stream.

        Args:
            file_handle: stream of json file.

        Returns:
            dict|list: json object.
        """
        return cls._byteify(
            json.load(file_handle, object_hook=cls._byteify),
            ignore_dicts=True
        )

    @classmethod
    def loads(cls, json_string):
        """Load json from string.

        Args:
            json_string: json string.

        Returns:
            dict|list: json object.
        """
        return cls._byteify(
            json.loads(json_string, object_hook=cls._byteify),
            ignore_dicts=True
        )

    @classmethod
    def _byteify(cls, data, ignore_dicts=False):
        if isinstance(data, unicode):
            return data.encode('utf-8')
        if isinstance(data, list):
            return [cls._byteify(item, ignore_dicts=True) for item in data]
        if isinstance(data, dict) and not ignore_dicts:
            return {
                cls._byteify(key, ignore_dicts=True): cls._byteify(value, ignore_dicts=True)
                for key, value in data.iteritems()
            }
        return data