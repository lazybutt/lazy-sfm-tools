from lazy_sfm_tools.files.file_structure import GamePath


class ModelPathsHolder(object):
    """Holder for model paths."""
    def __init__(self, model_path, vtx_path, vvd_path, phy_path):
        """Constructor for ModelPathsHolder.

        Args:
            model_path (GamePath): path to main model file (.mdl)
            vtx_path (GamePath): path to vtx file (.vtx)
            vvd_path (GamePath): path to vvd file (.vvd)
            phy_path (GamePath): path to physics file (.phy)
        """
        if model_path is None:
            raise ValueError("model path can not be None")
        self._model_path = model_path
        self._vtx_path = vtx_path
        self._vvd_path = vvd_path
        self._phy_path = phy_path

    @property
    def model_path(self):
        """Receive model game path (.mdl).

        Returns:
            GamePath: model game path (.mdl).
        """
        return self._model_path

    @property
    def vtx_path(self):
        """Receive vtx game path (.vtx).

        Returns:
            GamePath: vtx game path (.vtx).
        """
        return self._vtx_path

    @property
    def vvd_path(self):
        """Receive vvd game path (.vvd).

        Returns:
            GamePath: vvd game path (.vvd).
        """
        return self._vvd_path

    @property
    def phy_path(self):
        """Receive phy game path (.phy).

        Returns:
            GamePath: phy game path (.phy).
        """
        return self._phy_path

    @property
    def paths_as_list(self):
        """Collect all paths to single list.

        Returns:
            list[GamePath]: list of all model paths.
        """
        paths = [self._model_path, self._vtx_path, self._vvd_path, self._phy_path]
        return [path for path in paths if path is not None]
