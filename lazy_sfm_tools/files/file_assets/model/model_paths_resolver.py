import vs
import os
from vs.movieobjects import CDmeGameModel
from lazy_sfm_tools.files.file_structure import GamePath
from lazy_sfm_tools.utils.type_checks import isinstance_or_raise
from lazy_sfm_tools.files.path_utils import add_extension_if_not_exist
from lazy_sfm_tools.files.file_assets.model import ModelPathsHolder


class ModelPathsResolver(object):
    @classmethod
    def resolve(cls, gamemodel_or_mdl_path):
        """Resolve all paths from model object or model file path.

        Args:
            gamemodel_or_mdl_path (CDmeGameModel|str|unicode|GamePath): model object or model path.

        Returns:
            MaterialPathsHolder: holder with model paths.
        """
        if isinstance(gamemodel_or_mdl_path, vs.CDmeGameModel):
            model_path = gamemodel_or_mdl_path.modelName.GetValue()
            model_path = add_extension_if_not_exist(model_path, "mdl")
            model_path = GamePath.model(model_path)
        elif isinstance(gamemodel_or_mdl_path, (str, unicode)):
            model_path = GamePath.model(gamemodel_or_mdl_path)
        elif isinstance(gamemodel_or_mdl_path, GamePath):
            model_path = gamemodel_or_mdl_path
        else:
            raise NotImplementedError()

        if not model_path.is_valid:
            raise IOError('Path "{}" not found'.format(model_path.absolute_path))
        if not model_path.is_model:
            raise ValueError('Path "{}" is not model'.format(model_path.absolute_path))

        cls._get_other_paths(model_path)
        vtx_path, vvd_path, phy_path = cls._get_other_paths(model_path)

        from lazy_sfm_tools.files.file_assets.model import ModelPathsHolder
        return ModelPathsHolder(
            model_path,
            vtx_path,
            vvd_path,
            phy_path
        )

    @classmethod
    def resolve_gamemodel(cls, gamemodel):
        """Resolve all paths from model object.

        Args:
            gamemodel (CDmeGameModel): model object or model path.

        Returns:
            MaterialPathsHolder: holder with model paths.
        """
        isinstance_or_raise(gamemodel, vs.CDmeGameModel)
        model_path = gamemodel.modelName.GetValue()
        model_path = add_extension_if_not_exist(model_path, "mdl")
        game_path = GamePath.model(model_path)
        return cls._resolve_model_game_path(game_path)

    @classmethod
    def resolve_model_path(cls, mdl_path):
        """Resolve all paths from model object or model file path.

        Args:
            mdl_path (str|unicode|GamePath): model path.

        Returns:
            MaterialPathsHolder: holder with model paths.
        """
        if isinstance(mdl_path, (str, unicode)):
            model_path = GamePath.model(mdl_path)
        elif isinstance(mdl_path, GamePath):
            model_path = mdl_path
        else:
            raise NotImplementedError()
        cls._resolve_model_game_path(model_path)

    @classmethod
    def _resolve_model_game_path(cls, model_path):
        if not model_path.is_valid:
            raise IOError('Path "{}" not found'.format(model_path.absolute_path))
        if not model_path.is_model:
            raise ValueError('Path "{}" is not model'.format(model_path.absolute_path))
        cls._get_other_paths(model_path)
        vtx_path, vvd_path, phy_path = cls._get_other_paths(model_path)

        return ModelPathsHolder(
            model_path,
            vtx_path,
            vvd_path,
            phy_path
        )

    @staticmethod
    def _get_other_paths(model_path):
        full_path = model_path.absolute_path.replace(".mdl", "")
        if os.path.exists(full_path + ".dx90.vtx"):
            vtx = GamePath.model(full_path + ".dx90.vtx")
        elif os.path.exists(full_path + ".dx80.vtx"):
            vtx = GamePath.model(full_path + ".dx80.vtx")
        else:
            vtx = None
        vvd = GamePath.model(full_path + ".vvd") if os.path.exists(full_path + ".vvd") else None
        phy = GamePath.model(full_path + ".phy") if os.path.exists(full_path + ".phy") else None
        return vtx, vvd, phy
