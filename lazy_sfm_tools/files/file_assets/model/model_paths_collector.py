from vs.movieobjects import CDmeGameModel

from lazy_sfm_tools.files.file_assets.model import ModelPathsResolver
from lazy_sfm_tools.files.file_structure import GamePath


class ModelPathCollector(object):
    """Object for collect model paths."""
    @staticmethod
    def collect(gamemodels_or_mdl_paths):
        """Collect all model paths from gamemodel objects or paths.

        Args:
            gamemodels_or_mdl_paths (str|unicode|GamePath|CDmeGameModel|list[str|unicode|GamePath|CDmeGameModel]):
                game model, the model files of which need to be collect.

        Returns:
            list[ModelPathsResolver]: holder with model paths.
        """
        model_paths_holders = []
        if isinstance(gamemodels_or_mdl_paths, (str, unicode, GamePath, CDmeGameModel)):
            model_paths_holders.append(ModelPathsResolver.resolve(gamemodels_or_mdl_paths))
        elif isinstance(gamemodels_or_mdl_paths, (list, tuple)):
            for gamemodel_or_mdl_path in gamemodels_or_mdl_paths:
                model_paths_holders.append(ModelPathsResolver.resolve(gamemodel_or_mdl_path))
        else:
            NotImplementedError()
        return model_paths_holders

    @staticmethod
    def collect_from_gamemodel(gamemodel):
        """Collect all model paths from gamemodel.

        Args:
            gamemodel (CDmeGameModel): game model, the model files of which need to be collect.

        Returns:
            ModelPathsResolver: holder with model paths.
        """
        model_paths_holders = []
        if isinstance(gamemodel, CDmeGameModel):
            return ModelPathsResolver.resolve_gamemodel(gamemodel)
        else:
            NotImplementedError()
        return model_paths_holders

    @staticmethod
    def collect_from_mdl_path(mdl_path):
        """Collect all model paths from mdl path.

        Args:
            mdl_path (str|unicode|GamePath): game model path, the model files of which need to be collect.

        Returns:
            ModelPathsResolver: holder with model paths.
        """
        if isinstance(mdl_path, (str, unicode, GamePath)):
            return ModelPathsResolver.resolve_model_path(mdl_path)
        else:
            NotImplementedError()
