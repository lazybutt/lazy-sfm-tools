from lazy_sfm_tools.files.file_assets.model import ModelPathsHolder
from lazy_sfm_tools.files.file_assets.material import MaterialPathsHolder


class AssetPathsHolder(object):
    """Holder for model, material and texture paths."""
    def __init__(self, model_paths_holder, material_paths_holders):
        """Constructor for AssetPathsHolder

        Args:
            model_paths_holder (ModelPathsHolder): holder for model files.
            material_paths_holders (list[MaterialPathsHolder]): holder for material and texture files.
        """
        self._model_paths_holder = model_paths_holder
        self._material_paths_holders = material_paths_holders
        
    @property
    def model(self):
        """Receive model paths holder (".mdl", ".vvd", ".phy", ".dx80.vtx", ".dx90.vtx").

        Returns:
            ModelPathsHolder: holder with model files (".mdl", ".vvd", ".phy", ".dx80.vtx", ".dx90.vtx").
        """
        return self._model_paths_holder
        
    @property
    def materials(self):
        """Receive material and texture paths holder (".vmt", ".vtf").

        Returns:
            list[MaterialPathsHolder]: holder with material and texture files (".vmt", ".vtf").
        """
        return self._material_paths_holders
    
    @property
    def paths_as_list(self):
        """Collect all paths to single list.

        Returns:
            list[GamePath]: list of all asset paths.
        """
        paths = []
        paths.extend(self.model.paths_as_list)
        for material_paths_holder in self.materials:
            paths.extend(material_paths_holder.paths_as_list)
        return paths
