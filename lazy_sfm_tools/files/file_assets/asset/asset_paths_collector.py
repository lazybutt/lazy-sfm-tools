import vs
from vs.movieobjects import CDmeGameModel
from lazy_sfm_tools.files.file_assets.model import ModelPathsResolver
from lazy_sfm_tools.utils.type_checks import isinstance_or_raise
from lazy_sfm_tools.files.file_assets.material import MaterialPathsCollector
from lazy_sfm_tools.files.file_assets.asset.assets_paths_holder import AssetPathsHolder

class AssetPathsCollector(object):
    """Object for collect model, material and texture paths."""
    @classmethod
    def collect_from_gamemodel(cls, gamemodel):
        """Collect all asset paths (model, materials, textures).

        Args:
            gamemodel (CDmeGameModel): game model, the files of which need to be collect.

        Returns:
            AssetPathsHolder: holder with separated model and material paths.
        """
        isinstance_or_raise(gamemodel, vs.CDmeGameModel)

        return AssetPathsHolder(
            ModelPathsResolver.resolve_gamemodel(gamemodel),
            MaterialPathsCollector.collect_from_gamemodel(gamemodel)
        )
