import vs
import vsUtils
from vs.movieobjects import CDmeMaterial

from files.file_structure.gamepath import MaterialOrTextureGamePath
from lazy_sfm_tools.materials.common import Material
from lazy_sfm_tools.files.file_structure import GamePath
from lazy_sfm_tools.files.file_assets.material import MaterialPathsHolder


class MaterialPathsResolver(object):
    """Object to resolve all paths from material"""
    @classmethod
    def resolve(cls, material_or_path):
        """Resolve all paths from material object or material file path.

        Args:
            material_or_path (Material|CDmeMaterial|str|unicode|GamePath): material object or material path

        Returns:
            MaterialPathsHolder: holder with material and texture paths.
        """
        material_name = cls._get_material_name(material_or_path)
        material_path = cls._get_material_path(material_or_path)
        texture_paths, missed_texture_paths = cls._extract_textures_paths(material_path)
        return MaterialPathsHolder(
            material_name,
            material_path,
            texture_paths,
            missed_texture_paths
        )

    @classmethod
    def resolve_material_path(cls, material_path):
        """Resolve all paths from material file path.

        Args:
            material_path (str|unicode|GamePath): material path.

        Returns:
            MaterialPathsHolder: holder with material and texture paths.
        """
        material_name = cls._get_material_name(material_path)

        if isinstance(material_path, (str, unicode)):
            material_path = cls._str_to_game_path(material_path)
        elif isinstance(material_path, GamePath):
            material_path = material_path
        else:
            raise NotImplementedError()

        texture_paths, missed_texture_paths = cls._extract_textures_paths(material_path)
        from lazy_sfm_tools.files.file_assets.material import MaterialPathsHolder
        return MaterialPathsHolder(
            material_name,
            material_path,
            texture_paths,
            missed_texture_paths
        )

    @classmethod
    def resolve_material(cls, material):
        """Resolve all paths from material file path.

        Args:
            material (Material|CDmeMaterial): material object.

        Returns:
            MaterialPathsHolder: holder with material and texture paths.
        """
        material_name = cls._get_material_name(material)

        if isinstance(material, vs.CDmeMaterial):
            material_path = MaterialOrTextureGamePath(vs.mtlName.GetValue())
        elif isinstance(material, Material):
            material_path = material.game_path
        else:
            raise NotImplementedError()

        texture_paths, missed_texture_paths = cls._extract_textures_paths(material_path)
        from lazy_sfm_tools.files.file_assets.material import MaterialPathsHolder
        return MaterialPathsHolder(
            material_name,
            material_path,
            texture_paths,
            missed_texture_paths
        )

    @classmethod
    def _get_material_path(cls, material_or_path):
        if isinstance(material_or_path, (str, unicode)):
            material_path = MaterialOrTextureGamePath(material_or_path)
        elif isinstance(material_or_path, GamePath):
            material_path = material_or_path
        elif isinstance(material_or_path, vs.CDmeMaterial):
            material_path = MaterialOrTextureGamePath(vs.mtlName.GetValue())
        elif isinstance(material_or_path, Material):
            material_path = material_or_path.game_path
        else:
            raise TypeError("Value can be str, unicode, {}, {}, not {}".format(GamePath, vs.CDmeMaterial, material_or_path))

        cls._assert_material_path(material_path)

        return material_path

    @classmethod
    def _extract_textures_paths(cls, material_path):
        cls._assert_material_path(material_path)

        texture_paths = []
        missed_texture_paths = []
        for texture_path in vsUtils.getMaterialTextures(material_path.typed_content_relative_path).values():
            texture_path = texture_path if ".vtf" in texture_path.lower() else texture_path + ".vtf"
            game_path = GamePath.material_or_texture(texture_path)
            if game_path.is_valid:
                texture_paths.append(game_path)
            else:
                missed_texture_paths.append(texture_path)
        return texture_paths, missed_texture_paths

    @staticmethod
    def _str_to_game_path(path):
        material_or_path = path if ".vmt" in path else path + ".vmt"
        return GamePath.material_or_texture(material_or_path)

    @staticmethod
    def _get_material_name(material_path):
        import os
        return os.path.basename(material_path).replace(".vmt", "")

    @staticmethod
    def _assert_material_path(material_path):
        assert material_path.is_material, 'Path "{}" is not material'.format(material_path.absolute_path)
        assert material_path.is_valid, 'Path "{}" to VMT file is not valid'.format(material_path.absolute_path)
