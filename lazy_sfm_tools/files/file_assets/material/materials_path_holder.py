from lazy_sfm_tools.files.file_structure import GamePath


class MaterialPathsHolder(object):
    """Holder for material and texture paths."""
    def __init__(self, material_name, material_path, texture_paths, missed_texture_paths):
        """Constructor for MaterialPathsHolder.

        Args:
            material_name (str|unicode): name of material.
            material_path (GamePath): game path to material.
            texture_paths (list[GamePath]): game paths to textures from material file.
            missed_texture_paths(list[str]): missed texture paths (typed content folder related).
        """
        self._material_name = str(material_name)
        self._material_path = material_path
        self._texture_paths = texture_paths
        self._missed_texture_paths = missed_texture_paths

    @property
    def material_name(self):
        """Receive material name.

        Returns:
            str: material name.
        """
        return self._material_name

    @property
    def material_path(self):
        """Receive material game path.

        Returns:
            GamePath: material game path.
        """
        return self._material_path

    @property
    def texture_paths(self):
        """Receive texture game paths from material file.

        Returns:
            list[GamePath]: texture game paths from material file.
        """
        return [item for item in self._texture_paths]

    @property
    def missed_texture_paths(self):
        """Receive missed texture paths (typed content folder related).

        Returns:
            list[str]: missed texture paths (typed content folder related).
        """
        return [item for item in self._missed_texture_paths]

    @property
    def paths_as_list(self):
        """Collect all paths to single list.

        Returns:
            list[GamePath]: list of all material and texture paths.
        """
        paths = [self._material_path]
        paths.extend(self._texture_paths)
        return [path for path in paths if path is not None]

    def __str__(self):
        lines = ['Material "{}" in "{}"'.format(self._material_name, self._material_path), 'included textures:']
        lines.extend(
            [' - {}'.format(texture_path.absolute_path) for texture_path in self._texture_paths]
        )
        return "\n".join(lines)
