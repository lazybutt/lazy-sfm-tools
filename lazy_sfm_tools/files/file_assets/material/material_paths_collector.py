import vs
from materials.common import Material
from vs.movieobjects import CDmeMaterial
from lazy_sfm_tools.files.file_structure import GamePath
from lazy_sfm_tools.files.file_assets.material import MaterialPathsResolver
from lazy_sfm_tools.materials.common import MaterialsData
from lazy_sfm_tools.files.file_assets.material import MaterialPathsHolder


class MaterialPathsCollector(object):
    """Object for collect material and texture paths."""
    @classmethod
    def collect_from_gamemodel(cls, gamemodel):
        """Collect all material and texture paths from gamemodel.

        Args:
            gamemodel (CDmeGameModel): game model, the material and texture
                files of which need to be collect.

        Returns:
            list[MaterialPathsHolder]: holder with material and texture paths.
        """
        material_paths_holders = []
        if isinstance(gamemodel, vs.CDmeGameModel):
            material_paths = MaterialsData.from_gamemodel(gamemodel)\
                .mat_names_to_game_paths.values()
        else:
            raise NotImplementedError()

        for material_path in material_paths:
            holder = MaterialPathsResolver.resolve(material_path)
            material_paths_holders.append(holder)
        return material_paths_holders

    @classmethod
    def collect_from_materials(cls, materials):
        """Collect all material and texture paths from material objects or material paths.

        Args:
            materials (list[Material|CDmeMaterial|str|unicode|GamePath]): material objects or paths,
                files of which need to be collect.

        Returns:
            list[MaterialPathsHolder]: holders with material and texture paths.
        """
        material_paths_holders = []
        if isinstance(materials, (list, tuple)):
            material_paths = materials
        else:
            raise NotImplementedError()

        for material_path in material_paths:
            holder = MaterialPathsResolver.resolve(material_path)
            material_paths_holders.append(holder)
        return material_paths_holders

    @classmethod
    def collect_from_material(cls, material):
        """Collect material and texture paths from material object or material path.

        Args:
            material (Material|CDmeMaterial|str|unicode|GamePath): material object or path,
                files of which need to be collect.

        Returns:
            MaterialPathsHolder: holder with material and texture paths.
        """
        if isinstance(material, (Material, CDmeMaterial, str, unicode, GamePath)):
            return MaterialPathsResolver.resolve(material)
        else:
            raise NotImplementedError()
