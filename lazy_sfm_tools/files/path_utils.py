def add_extension_if_not_exist(path, extension):
    """Add extension to path if extension is not exists.

    Args:
        path (str|unicode): path with(out) extension.
        extension(str|unicode): extension to add if not found.

    Returns:
        str|unicode: path with extension.
    """
    if path.endswith("." + extension):
        return str(path)
    return str(path) + "." + str(extension)

def remove_extension_if_exists(path, extension=None):
    """Remove extension from path if extension if exists.

    Args:
        path (str|unicode): path with(out) extension.
        extension (str|unicode|None): optional extension to remove,
            if None any extension will removed.

    Returns:
         str|unicode: path without extension.
    """
    if extension is not None and path.endswith(extension):
        return path[:len(extension)]
    split_path = path.split(".")
    if len(split_path) == 1:
        return path
    return ".".join(path.split(".")[:-1])
