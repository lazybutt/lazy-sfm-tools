from enum import Enum
from vs.movieobjects import CDmeAnimationSet


class AnimationSetType(Enum):
    """Enum with animation set types."""
    MODEL = "gameModel"
    CAMERA = "camera"
    LIGHT = "light"
    PARTICLE = "particle"

    @classmethod
    def of(cls, animset):
        """Define type of animation set.

        Args:
            animset (CDmeAnimationSet): animation set to define type.

        Returns:
            AnimationSetType: animation set type.
        """
        animset_types = [cls.MODEL, cls.CAMERA, cls.LIGHT, cls.PARTICLE]
        for animset_type in animset_types:
            if animset.HasAttribute(animset_type.value):
                return animset_type

        raise RuntimeError("Unknown animation set type")
