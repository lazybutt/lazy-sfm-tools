from enum import Enum


class AttributeTypes(Enum):
    BOOL = 4
    BOOL_ARRAY = 18
    COLOR = 8
    COLOR_ARRAY = 22
    ELEMENT = 1
    ELEMENT_ARRAY = 15
    FIRST_ARRAY_TYPE = 15
    FIRST_VALUE_TYPE = 1
    FLOAT = 3
    FLOAT_ARRAY = 17
    INT = 2
    INT_ARRAY = 16
    QANGLE = 12
    QANGLE_ARRAY = 26
    QUATERNION = 13
    QUATERNION_ARRAY = 27
    STRING = 5
    STRING_ARRAY = 19
    TIME = 7
    TIME_ARRAY = 21
    TYPE_COUNT = 29
    TYPE_INVALID = 30
    UNKNOWN = 0
    VECTOR2 = 9
    VECTOR2_ARRAY = 23
    VECTOR3 = 10
    VECTOR3_ARRAY = 24
    VECTOR4 = 11
    VECTOR4_ARRAY = 25
    VMATRIX = 14
    VMATRIX_ARRAY = 28
    VOID = 6
    VOID_ARRAY = 20
    VECTOR_TYPES = (VECTOR2, VECTOR3, VECTOR4)
    VECTOR_ARRAY_TYPES = (VECTOR2_ARRAY, VECTOR3_ARRAY, VECTOR4_ARRAY)

    @classmethod
    def elementary_types(cls):
        return [
            cls.INT,
            cls.FLOAT,
            cls.BOOL,
            cls.STRING
        ]

    @classmethod
    def elementary_array_types(cls):
        return [
            cls.INT_ARRAY,
            cls.FLOAT_ARRAY,
            cls.BOOL_ARRAY,
            cls.STRING_ARRAY
        ]

    @classmethod
    def complex_types(cls):
        return [
            cls.QANGLE,
            cls.VECTOR2,
            cls.VECTOR3,
            cls.VECTOR4,
            cls.QUATERNION,
            cls.VMATRIX,
            cls.TIME,
            cls.COLOR,
        ]

    @classmethod
    def complex_array_types(cls):
        return [
            cls.QANGLE_ARRAY,
            cls.VECTOR2_ARRAY,
            cls.VECTOR3_ARRAY,
            cls.VECTOR4_ARRAY,
            cls.QUATERNION_ARRAY,
            cls.VMATRIX_ARRAY,
            cls.TIME_ARRAY,
            cls.COLOR_ARRAY,
        ]

    @classmethod
    def vector_types(cls):
        return [
            cls.VECTOR2,
            cls.VECTOR3,
            cls.VECTOR4
        ]

    @classmethod
    def vector_array_types(cls):
        return [
            cls.VECTOR2_ARRAY,
            cls.VECTOR3_ARRAY,
            cls.VECTOR4_ARRAY
        ]

    @classmethod
    def elementary_type_codes(cls):
        return [attr_type.value for attr_type in cls.elementary_types()]

    @classmethod
    def elementary_array_type_codes(cls):
        return [attr_type.value for attr_type in cls.elementary_array_types()]

    @classmethod
    def complex_type_codes(cls):
        return [attr_type.value for attr_type in cls.complex_types()]

    @classmethod
    def complex_array_type_codes(cls):
        return [attr_type.value for attr_type in cls.complex_array_types()]

    @classmethod
    def vector_type_codes(cls):
        return [attr_type.value for attr_type in cls.vector_types()]

    @classmethod
    def vector_array_type_codes(cls):
        return [attr_type.value for attr_type in cls.vector_array_types()]

    @classmethod
    def of(cls, code):
        for attr_type in cls.__dict__.values():
            if isinstance(attr_type, AttributeTypes) and attr_type.value == code:
                return attr_type
        return cls.UNKNOWN

    @classmethod
    def alias_by_type(cls, type_code):

        types_to_aliases = cls.types_to_aliases()
        if type_code not in types_to_aliases.keys():
            raise KeyError('Invalid type code: "{}"'.format(type_code))
        return types_to_aliases[type_code]

    @classmethod
    def type_by_alias(cls, alias):
        aliases_to_types = cls.aliases_to_types()
        if alias not in aliases_to_types.keys():
            raise KeyError('Invalid alias: "{}"'.format(alias))
        return aliases_to_types[alias]

    @classmethod
    def aliases_to_types(cls):
        return {value: key for key, value in cls.types_to_aliases().items()}

    @classmethod
    def types_to_aliases(cls):
        items = {}
        for key, value in cls.__dict__.items():
            if isinstance(value, AttributeTypes):
                items[value] = \
                    key.replace("_", " ").lower()
        return items

    @staticmethod
    def validate_attr_type(attr, excepted_type):
        attr_type = attr.GetType()
        if attr_type != excepted_type:
            raise TypeError("Excepted {} type, received {} type".format(excepted_type, attr_type))
