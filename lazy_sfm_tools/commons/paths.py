import os.path as _p
import lazy_sfm_tools

class Paths(object):
    """Object with all paths used in library."""

    LIBRARY = _p.dirname(lazy_sfm_tools.__file__)
    RESOURCES = _p.join(LIBRARY, "resources")

    MATERIAL_ATTRS_INFO = _p.join(RESOURCES, "mat_attrs.csv")
